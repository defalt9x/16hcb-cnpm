﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ReportDAO
    {
        public DataTable SearchReport(string strKeyWordYear, string strKeyWordSemeter, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWordYear", strKeyWordYear,
                "@KeyWordSemeter", strKeyWordSemeter
            };

            DataTable dtbReport = new DataConnect().callUSP("uspGetReport", objKeyWords);
            if (dtbReport != null && dtbReport.Rows.Count != 0)
            {
                return dtbReport;
            }

            return null;
        }

        public DataTable SearchReport2(string strKeyWordYear, string strKeyWordSemeter, string strKeyWordSubject, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWordYear", strKeyWordYear,
                "@KeyWordSemeter", strKeyWordSemeter,
                "@KeyWordSubject", strKeyWordSubject
            };

            DataTable dtbReport = new DataConnect().callUSP("uspGetReport2", objKeyWords);
            if (dtbReport != null && dtbReport.Rows.Count != 0)
            {
                return dtbReport;
            }

            return null;
        }


        public DataTable SearchReportFull(string strKeyWordYear, string strKeyWordSemeter)
        {
            object[] objKeyWords = new object[] {
                "@KeyWordYear", strKeyWordYear,
                "@KeyWordSemeter", strKeyWordSemeter
            };

            DataTable dtbReport = new DataConnect().callUSP("uspGetReportFull", objKeyWords);
            if (dtbReport != null && dtbReport.Rows.Count != 0)
            {
                return dtbReport;
            }

            return null;
        }

        public DataTable GetYearList()
        {
            DataTable dtbReport = new DataConnect().callUSP("uspGetYearList", null);
            if (dtbReport != null && dtbReport.Rows.Count != 0)
            {
                return dtbReport;
            }

            return null;
        }

    }
}
