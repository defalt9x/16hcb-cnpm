﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class EmloyeeRolesDAO
    {
        public DataTable SearchEmloyeeRoles(string strKeyWord, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWord", strKeyWord
            };

            DataTable dtbEmloyeeRoles = new DataConnect().callUSP("uspGetEmloyeeRoles_SRH", objKeyWords);
            if (dtbEmloyeeRoles != null && dtbEmloyeeRoles.Rows.Count != 0)
            {
                return dtbEmloyeeRoles;
            }

            return null;
        }

        public DataTable SearchPermission(string strKeyWord, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWord", strKeyWord
            };

            DataTable dtbPermission = new DataConnect().callUSP("uspPermissionKeyList_SRH", objKeyWords);
            if (dtbPermission != null && dtbPermission.Rows.Count != 0)
            {
                return dtbPermission;
            }

            return null;
        }

        public bool InsertEmloyeeRoles(string strRoleName, List<PermissionDTO> LstPermission)
        {
            List<string> Lstusp_name = new List<string>() { "uspEmloyeeRoles_ADD", "uspRole_Keys_ADD" };
            List<object[]> LstobjKeyWords = new List<object[]>();

            //Tạo list keywords
            object[] objKeyWords1 = new object[] {
                "@RoleName", strRoleName
            };
            LstobjKeyWords.Add(objKeyWords1);

            StringBuilder strPermissionID = new StringBuilder();
            if (LstPermission != null && LstPermission.Count > 0)
            {
                for (int i = 0; i < LstPermission.Count; i++)
                {
                    strPermissionID.Append(LstPermission[i].IDKey);

                    if (i != LstPermission.Count - 1)
                        strPermissionID.Append(",");
                }
            }
            object[] objKeyWords2 = new object[] {
                "@RoleID", "INFINITY",
                "@LstPermissionID", Convert.ToString(strPermissionID)
            };
            LstobjKeyWords.Add(objKeyWords2);

            return new DataConnect().exeUSP(Lstusp_name, LstobjKeyWords, true);
        }

        public EmloyeeRolesDTO GetEmloyeeRolesByID(string strRoleID)
        {
            EmloyeeRolesDTO objResult = new EmloyeeRolesDTO();

            //Lấy Nhóm người dùng
            object[] objKeyWords = new object[] {
                "@RoleID", strRoleID
            };

            DataTable dtbEmloyeeRoles = new DataConnect().callUSP("uspEmloyeeRoles_SEL", objKeyWords);

            if (dtbEmloyeeRoles != null && dtbEmloyeeRoles.Rows.Count > 0)
            {
                objResult.RoleID = Convert.ToString(dtbEmloyeeRoles.Rows[0]["RoleID"]).Trim();
                objResult.RoleName = Convert.ToString(dtbEmloyeeRoles.Rows[0]["RoleName"]).Trim();

                //Lấy Quyền theo nhóm người dùng
                objResult.lstPermission = GetPermissionByRoles(objResult.RoleID);
            }

            return objResult;
        }

        public List<PermissionDTO> GetPermissionByRoles(string strRoleID)
        {
            List<PermissionDTO> lstResult = new List<PermissionDTO>();

            object[] objKeyWords = new object[] {
                "@RoleID", strRoleID
            };

            DataTable dtbPermission = new DataConnect().callUSP("uspPermission_ByRole_SEL", objKeyWords);

            if (dtbPermission != null && dtbPermission.Rows.Count > 0)
            {
                int intCountRow = dtbPermission.Rows.Count;

                for (int i = 0; i < intCountRow; i++)
                {
                    PermissionDTO objPermission = new PermissionDTO();

                    objPermission.IDKey = Convert.ToString(dtbPermission.Rows[i]["IDKey"]).Trim();
                    objPermission.KeyDescription = Convert.ToString(dtbPermission.Rows[i]["KeyDescription"]).Trim();

                    lstResult.Add(objPermission);
                }
            }

            return lstResult;
        }

        public bool UpdateEmloyeeRoles(string strRoleID, string strRoleName, List<PermissionDTO> LstPermission)
        {
            List<string> Lstusp_name = new List<string>() { "uspEmloyeeRoles_UPD", "uspRole_Keys_DEL", "uspRole_Keys_ADD" };
            List<object[]> LstobjKeyWords = new List<object[]>();

            //Tạo list keywords
            object[] objKeyWords1 = new object[] {
                "@RoleID", strRoleID,
                "@RoleName", strRoleName
            };
            LstobjKeyWords.Add(objKeyWords1);

            object[] objKeyWords2 = new object[] {
                "@RoleID", strRoleID
            };
            LstobjKeyWords.Add(objKeyWords2);

            StringBuilder strPermissionID = new StringBuilder();
            if (LstPermission != null && LstPermission.Count > 0)
            {
                for (int i = 0; i < LstPermission.Count; i++)
                {
                    strPermissionID.Append(LstPermission[i].IDKey);

                    if (i != LstPermission.Count - 1)
                        strPermissionID.Append(",");
                }
            }
            object[] objKeyWords3 = new object[] {
                    "@RoleID", strRoleID,
                    "@LstPermissionID", Convert.ToString(strPermissionID)
                };
            LstobjKeyWords.Add(objKeyWords3);

            return new DataConnect().exeUSP(Lstusp_name, LstobjKeyWords, false);
        }

        public bool DeleteEmloyeeRoles(string strRoleID, ref string strMessage)
        {
            //Kiểm tra nhóm người dùng có đang tồn tại người dùng nào không
            DataTable dtbEmloyeesByRole = GetEmloyeesByRole(strRoleID);
            if (dtbEmloyeesByRole != null && dtbEmloyeesByRole.Rows.Count > 0)
            {
                strMessage = "Tồn tại nhóm người dùng thuộc nhóm! Vui lòng xóa người dùng thuộc nhóm.";
            }
            else
            {
                List<string> Lstusp_name = new List<string>() { "uspRole_Keys_DEL", "uspEmloyeeRoles_DEL" };
                List<object[]> LstobjKeyWords = new List<object[]>();

                //Tạo list keywords
                object[] objKeyWords1 = new object[] {
                    "@RoleID", strRoleID
                 };
                LstobjKeyWords.Add(objKeyWords1);

                object[] objKeyWords2 = new object[] {
                    "@RoleID", strRoleID
                };
                LstobjKeyWords.Add(objKeyWords2);

                return new DataConnect().exeUSP(Lstusp_name, LstobjKeyWords, false);
            }

            return false;
        }

        public DataTable GetEmloyeesByRole(string strRoleID)
        {
            //Lấy Nhóm người dùng
            object[] objKeyWords = new object[] {
                "@RoleID", strRoleID
            };

            DataTable dtbEmloyees = new DataConnect().callUSP("uspEmloyees_ByRole_SRH", objKeyWords);
            if (dtbEmloyees != null && dtbEmloyees.Rows.Count != 0)
            {
                return dtbEmloyees;
            }

            return null;
        }
    }
}
