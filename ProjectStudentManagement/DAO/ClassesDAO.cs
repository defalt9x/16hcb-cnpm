﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ClassesDAO
    {
        public DataTable GetClassList()
        {
            DataTable dtb = new DataConnect().callUSP("uspClasses_GetAllClasses", null);
            if (dtb != null && dtb.Rows.Count != 0)
            {
                return dtb;
            }
            return null;
        }



        public DataTable SearchClasses(string strKeyWord, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWord", strKeyWord
            };

            DataTable dtbClasses = new DataConnect().callUSP("uspGetClasses_SRH", objKeyWords);
            if (dtbClasses != null && dtbClasses.Rows.Count != 0)
            {
                return dtbClasses;
            }

            return null;
        }

        public bool EditClassesName(string strClassesID, string strClassesName)
        {
            object[] objKeyWords = new object[] {
                "@ClassesID", strClassesID,
                "@ClassesName", strClassesName
            };

            DataTable dtbClasses = new DataConnect().callUSP("uspChangeClassesName", objKeyWords);
            if (dtbClasses != null && dtbClasses.Rows.Count != 0)
            {
                return true;
            }

            return false;
        }

        public bool DeleteClasses(string strClassesID)
        {
            object[] objKeyWords = new object[] {
                "@ClassesID", strClassesID
            };

            DataTable dtbClasses = new DataConnect().callUSP("uspDeleteClasses", objKeyWords);
            if (dtbClasses != null && dtbClasses.Rows.Count != 0)
            {
                return false;
            }

            return true;
        }

        public bool InsertClasses(string strClassName, string strClassGrade)
        {
            object[] objKeyWords = new object[] {
                "@ClassesName", strClassName,
                "@ClassesGrade", strClassGrade
            };

            DataTable dtbClasses = new DataConnect().callUSP("uspInsertClasses", objKeyWords);
            if (dtbClasses != null && dtbClasses.Rows.Count != 0)
            {
                return true;
            }

            return false;
        }
    }
}
