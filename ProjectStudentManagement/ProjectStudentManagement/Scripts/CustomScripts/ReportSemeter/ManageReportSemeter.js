﻿var IsLoadServer = false;

$(document).ready(function () {
    //Nạp năm vào selectdropdown
    $.ajax({
        url: "/Home/GetYearList",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            if (data.iserror == false) {
                var obj = JSON.parse(data.yearlistcontent);
                var objsize = Object.keys(obj).length;
                for (var i = 0; i < objsize; i++) {
                    $("#sltKeyWordYear").append("<option value='" + obj[i].Year + "'>Niên khóa: " + obj[i].Year + " </option>");
                }
            }
            else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });


    //Nạp môn học vào selectdropdown
    $.ajax({
        url: "/Score/GetSubjectList",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            if (data.iserror == false) {
                var obj = JSON.parse(data.subjectlistcontent);
                var objsize = Object.keys(obj).length;
                for (var i = 0; i < objsize; i++) {
                    $("#sltKeyWordSubject").append("<option value='" + obj[i].SubjectID + "'>Môn học: " + obj[i].SubjectName + " </option>");
                }
            }
            else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });

    //Xuất excel
    $("#btnExcel").click(function () {
        //var tableclone = $("#tblReport").clone();
        //tableclone.find("thead").prepend('<tr><td colspan="7" style="text-align: center; height: 50px; vertical-align: middle;font-size: 200%;" ><b>Báo cáo tổng kết học kỳ</b></td></tr> <tr></tr> <tr></tr> <tr></tr> <tr></tr>');
        //tableclone.table2excel({
        //    exclude: ".noExl",
        //    name: "Excel Document Name",
        //    filename: "Bao-cao-tong-ket-hoc-ky",
        //    fileext: ".xls",
        //    exclude_img: true,
        //    exclude_links: true,
        //    exclude_inputs: true
        //});
        var param = {};
        param.strKeyWordYear = $('#sltKeyWordYear').val();
        param.strKeyWordSemeter = $('#sltKeyWordSemeter').val();
        if (param.strKeyWordYear == null) {
            $.notify("Vui lòng chọn niên khóa", "info");
            $("#sltKeyWordYear").focus();
            return false;
        }

        if (param.strKeyWordSemeter == null) {
            $.notify("Vui lòng chọn học kỳ", "info");
            $("#sltKeyWordSemeter").focus();
            return false;
        }
        window.location.href = "/Home/ExportExcel?strKeyWordYear=" + param.strKeyWordYear + "&strKeyWordSemeter=" + param.strKeyWordSemeter;
    });

    //In
    $("#btnPrint").click(function () {
        var param = {};
        param.strKeyWordYear = $('#sltKeyWordYear').val();
        param.strKeyWordSemeter = $('#sltKeyWordSemeter').val();
        if (param.strKeyWordYear == null) {
            $.notify("Vui lòng chọn niên khóa", "info");
            $("#sltKeyWordYear").focus();
            return false;
        }

        if (param.strKeyWordSemeter == null) {
            $.notify("Vui lòng chọn học kỳ", "info");
            $("#sltKeyWordSemeter").focus();
            return false;
        }
        //window.location.href = "/Home/PrintReport?strKeyWordYear=" + param.strKeyWordYear + "&strKeyWordSemeter=" + param.strKeyWordSemeter;
        window.open("/Home/PrintReport?strKeyWordYear=" + param.strKeyWordYear + "&strKeyWordSemeter=" + param.strKeyWordSemeter)
    });


    $("#menu-toggle").click(function () {
        $("#sidebar-hidden-div").toggle();
        if ($("#sidebar-hidden-div").css('display') == "none")
            $("#page-wrapper").prop('id', 'page-wrapper-disable');
        else
            $("#page-wrapper-disable").prop('id', 'page-wrapper');
    });
    //$("#menu-toggle2").click(function (e) {
    //    $("#sidebar-hidden-div").show();
    //    $("#page-wrapper-disable").prop('id', 'page-wrapper');
    //});
});

function SearchReport() {
    //Nạp tham số
    var param = {};
    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').val();
    if (param.strKeyWordYear == null) {
        $.notify("Vui lòng chọn niên khóa", "info");
        $("#sltKeyWordYear").focus();
        return false;
    }

    if (param.strKeyWordSemeter == null) {
        $.notify("Vui lòng chọn học kỳ", "info");
        $("#sltKeyWordSemeter").focus();
        return false;
    }


    $.ajax({
        url: "/Home/ReportSearch",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
            $("#btnSearch").button('loading');
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentReport').html(data.content);
                Paging(data.totalpages, data.totalrows, 1);
                $('#sltKeyWordYear').attr('data-value', $('#sltKeyWordYear').val());
                $('#sltKeyWordSemeter').attr('data-value', $('#sltKeyWordSemeter').val());
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

            $("#btnSearch").button('reset');
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            $("#btnSearch").button('reset');
        }
    });
}

function Paging(totalPages, totalRows, startpage) {
    if (totalRows > parseInt(visiblepages)) {
        $('#paging').remove();
        $('#divPagingIndex').html("<ul id='paging' class='pagination'></ul>");
        $('#paging').twbsPagination({
            startPage: startpage,
            totalPages: totalPages,
            visiblePages: visiblepages,
            onPageClick: function (event, page) {
                //Ghi nhận chỉ số trang hiện tại
                $('#hdCurrentPage').val(page - 1); //Vì store bắt đầu từ 0 nên -1

                //Load data ứng với chỉ số page từ server
                if (IsLoadServer == true)
                    SearchAjaxPaging();

                //Thể hiện chỉ số record đang hiển thị trên giao diện
                var visiblePages = parseInt(visiblepages);
                var ToIndextemp = page * visiblePages;
                var FromIndex = (page - 1) * visiblePages + 1;
                //Lấy chỉ số đến trang (Nếu không là trang cuối)
                if (ToIndextemp < totalRows)
                    ToIndex = ToIndextemp;
                else
                    ToIndex = totalRows;

                $('#page-from-index').text(FromIndex);
                $('#page-to-index').text(ToIndex);
                $('#total-record').text(totalRows);

                IsLoadServer = true;
            }
        });
        $('#divPaging').css('display', 'block');
    } else {
        $('#divPaging').css('display', 'none');
    }
}

function SearchAjaxPaging() {
    //Nạp tham số
    var param = {};
    param.strKeyWordYear = $('#sltKeyWordYear').attr('data-value');
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').attr('data-value');
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/Home/ReportSearch",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentReport').html(data.content);
                //Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()));
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}



function SearchReport2() {
    //Nạp tham số
    var param = {};
    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').val();
    param.strKeyWordSubject = $('#sltKeyWordSubject').val();
    if (param.strKeyWordYear == null) {
        $.notify("Vui lòng chọn niên khóa", "info");
        $("#sltKeyWordYear").focus();
        return false;
    }

    if (param.strKeyWordSemeter == null) {
        $.notify("Vui lòng chọn học kỳ", "info");
        $("#sltKeyWordSemeter").focus();
        return false;
    }

    if (param.strKeyWordSubject == null) {
        $.notify("Vui lòng chọn môn học", "info");
        $("#strKeyWordSubject").focus();
        return false;
    }

    $.ajax({
        url: "/Home/ReportSearch2",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
            $("#btnSearch").button('loading');
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentReport').html(data.content);
                Paging2(data.totalpages, data.totalrows, 1);
                $('#sltKeyWordYear').attr('data-value', $('#sltKeyWordYear').val());
                $('#sltKeyWordSemeter').attr('data-value', $('#sltKeyWordSemeter').val());
                $('#sltKeyWordSubject').attr('data-value', $('#sltKeyWordSubject').val());
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

            $("#btnSearch").button('reset');
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            $("#btnSearch").button('reset');
        }
    });
}

function Paging2(totalPages, totalRows, startpage) {
    if (totalRows > parseInt(visiblepages)) {
        $('#paging').remove();
        $('#divPagingIndex').html("<ul id='paging' class='pagination'></ul>");
        $('#paging').twbsPagination({
            startPage: startpage,
            totalPages: totalPages,
            visiblePages: visiblepages,
            onPageClick: function (event, page) {
                //Ghi nhận chỉ số trang hiện tại
                $('#hdCurrentPage').val(page - 1); //Vì store bắt đầu từ 0 nên -1

                //Load data ứng với chỉ số page từ server
                if (IsLoadServer == true)
                    SearchAjaxPaging2();

                //Thể hiện chỉ số record đang hiển thị trên giao diện
                var visiblePages = parseInt(visiblepages);
                var ToIndextemp = page * visiblePages;
                var FromIndex = (page - 1) * visiblePages + 1;
                //Lấy chỉ số đến trang (Nếu không là trang cuối)
                if (ToIndextemp < totalRows)
                    ToIndex = ToIndextemp;
                else
                    ToIndex = totalRows;

                $('#page-from-index').text(FromIndex);
                $('#page-to-index').text(ToIndex);
                $('#total-record').text(totalRows);

                IsLoadServer = true;
            }
        });
        $('#divPaging').css('display', 'block');
    } else {
        $('#divPaging').css('display', 'none');
    }
}

function SearchAjaxPaging2() {
    //Nạp tham số
    var param = {};
    param.strKeyWordYear = $('#sltKeyWordYear').attr('data-value');
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').attr('data-value');
    param.strKeyWordSubject = $('#sltKeyWordSubject').attr('data-value'); 
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/Home/ReportSearch2",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentReport').html(data.content);
                //Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()));
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}