﻿var IsLoadServer = false;


$(document).ready(function () {
    DefaultLoadPage();
    OnchangeYear();
    $("#btnSearch").click(function () {
        IsLoadServer = false;
        GetScore();
    });
});






function DefaultLoadPage() {
    //Nạp năm vào selectdropdown
    $.ajax({
        url: "/Home/GetYearList",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            if (data.iserror == false) {
                var obj = JSON.parse(data.yearlistcontent);
                var objsize = Object.keys(obj).length;
                for (var i = 0; i < objsize; i++) {
                    $("#sltKeyWordYear").append("<option value='" + obj[i].Year + "'>Niên khóa: " + obj[i].Year + " </option>");
                }
            }
            else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });


    //Nạp môn học vào selectdropdown
    $.ajax({
        url: "/Score/GetSubjectList",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            if (data.iserror == false) {
                var obj = JSON.parse(data.subjectlistcontent);
                var objsize = Object.keys(obj).length;
                for (var i = 0; i < objsize; i++) {
                    $("#sltKeyWordSubject").append("<option value='" + obj[i].SubjectID + "'>Môn học: " + obj[i].SubjectName + " </option>");
                }
            }
            else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

function OnchangeYear() {
    //Nạp lớp trong năm đã chọn
    $("#sltKeyWordYear").change(function () {
        var strHTML = "";
        strHTML += '<select class="form-control" id="sltKeyWordClass" placeholder="Lớp" data-value="">';
        strHTML += "<option disabled='disabled' selected='selected'>--Xin lựa chọn lớp học--</option>";
        strHTML += '</select>';
        $('#containersltYear').append(strHTML);
        var param = {};
        param.strKeyWord = $('#sltKeyWordYear').val();
        $.ajax({
            url: "/Score/GetClasslist",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(param),
            dataType: 'json',
            success: function (data) {
                if (data.iserror == false) {
                    var obj = JSON.parse(data.Classlistcontent);
                    var objsize = Object.keys(obj).length;
                    for (var i = 0; i < objsize; i++) {
                        $("#sltKeyWordClass").append("<option value='" + obj[i].ClassID + "'>Tên lớp: " + obj[i].ClassName + " </option>");
                    }
                }
                else {
                    $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
                }

            },
            error: function (xhr, status, error) {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        });

    });
}

//Series Function Get
function GetScore() {
    //Nạp tham số
    var param = {};
    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').val();
    param.strKeyWordStudent = $('#txtKeyWord').val();
    param.strKeyWordClass = $('#sltKeyWordClass').val();
    param.strKeyWordSubject = $('#sltKeyWordSubject').val();
    if (param.strKeyWordYear == null) {
        $.notify("Vui lòng chọn niên khóa", "info");
        $("#sltKeyWordYear").focus();
        return false;
    }

    if (param.strKeyWordSemeter == null) {
        $.notify("Vui lòng chọn học kỳ", "info");
        $("#sltKeyWordSemeter").focus();
        return false;
    }

    if (param.strKeyWordClass == null) {
        $.notify("Vui lòng chọn lớp", "info");
        $("#strKeyWordClass").focus();
        return false;
    }

    if (param.strKeyWordSubject == null) {
        $.notify("Vui lòng chọn môn", "info");
        $("#strKeyWordSubject").focus();
        return false;
    }



    $.ajax({
        url: "/Score/GetScore",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
            $("#btnSearch").button('loading');
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentScore').html(data.content);
                if (data.editpcolumn == false) {
                    $("#thProcessColumn").hide();
                }
                else {
                    $("#thProcessColumn").show();
                }
                Paging(data.totalpages, data.totalrows, 1);
                InitProcessInTable();
                $('#sltKeyWordYear').attr('data-value', $('#sltKeyWordYear').val());
                $('#sltKeyWordSemeter').attr('data-value', $('#sltKeyWordSemeter').val());
                $('#sltKeyWordClass').attr('data-value', $('#sltKeyWordClass').val());
                $('#sltKeyWordSubject').attr('data-value', $('#sltKeyWordSubject').val());
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

            $("#btnSearch").button('reset');
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            $("#btnSearch").button('reset');
        }
    });
}

function Paging(totalPages, totalRows, startpage) {
    if (totalRows > parseInt(visiblepages)) {
        $('#paging').remove();
        $('#divPagingIndex').html("<ul id='paging' class='pagination'></ul>");
        $('#paging').twbsPagination({
            startPage: startpage,
            totalPages: totalPages,
            visiblePages: visiblepages,
            onPageClick: function (event, page) {
                //Ghi nhận chỉ số trang hiện tại
                $('#hdCurrentPage').val(page - 1); //Vì store bắt đầu từ 0 nên -1

                //Load data ứng với chỉ số page từ server
                if (IsLoadServer == true)
                    SearchAjaxPaging();

                //Thể hiện chỉ số record đang hiển thị trên giao diện
                var visiblePages = parseInt(visiblepages);
                var ToIndextemp = page * visiblePages;
                var FromIndex = (page - 1) * visiblePages + 1;
                //Lấy chỉ số đến trang (Nếu không là trang cuối)
                if (ToIndextemp < totalRows)
                    ToIndex = ToIndextemp;
                else
                    ToIndex = totalRows;

                $('#page-from-index').text(FromIndex);
                $('#page-to-index').text(ToIndex);
                $('#total-record').text(totalRows);

                IsLoadServer = true;
            }
        });
        $('#divPaging').css('display', 'block');
    } else {
        $('#divPaging').css('display', 'none');
    }
}

function SearchAjaxPaging() {
    //Nạp tham số
    var param = {};
    param.strKeyWordYear = $('#sltKeyWordYear').attr('data-value');
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').attr('data-value');
    param.strKeyWordStudent = $('#txtKeyWord').attr('data-value');
    param.strKeyWordClass = $('#sltKeyWordClass').attr('data-value');
    param.strKeyWordSubject = $('#sltKeyWordSubject').attr('data-value');
  
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/Score/GetScore",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentScore').html(data.content);
                if (data.editpcolumn == false) {
                    $("#thProcessColumn").hide();
                }
                else {
                    $("#thProcessColumn").show();
                }
                //Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()));
                InitProcessInTable();
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

//Process Click
function InitProcessInTable() {
    $('#ContentScore tr').each(function () {
        $(this).find('td span[data-name="Detail"]').click(function () {
            $('#txtStudentIDModal').val($(this).closest('tr').find('.StudentIDCell').html());
            var testvar = $(this).closest('tr').find('.FullNameCell').html();
            $('#txtStudentNameModal').val($(this).closest('tr').find('.FullNameCell').html());
            $('#txtYearModal').val($('#sltKeyWordYear').attr('data-value'));
            $('#txtSemeterModal').val($('#sltKeyWordSemeter').attr('data-value'));
            var sSubject = $('#sltKeyWordSubject option:selected').html();
            sSubject = sSubject.replace("Môn học: ", "");
            $('#txtSubjectModal').val(sSubject);
            $('#txtScore1Cell').val($(this).closest('tr').find('.Score1Cell').html());
            $('#txtScore2Cell').val($(this).closest('tr').find('.Score2Cell').html());
            $('#txtScore3Cell').val($(this).closest('tr').find('.Score3Cell').html());
            OpenScoreEdit();
        });
    });
}


//Modal Function
function OpenScoreEdit() {
    $('#ModalScore').modal('toggle');
}
function CloseScoreEdit() {
    $('#ModalScore').modal('toggle');
}

//Edit process
function ScoreEditProcess()
{
    if (!$.isNumeric($('#txtScore1Cell').val()))
    {
        $.notify("Điểm miệng phải là số", "info");
        $("#txtScore1Cell").focus();
        return false;
    }

    if ($('#txtScore1Cell').val() < 0 | $('#txtScore1Cell').val() > 10) {
        $.notify("Điểm miệng phải có điểm từ 0 đến 10", "info");
        $("#txtScore1Cell").focus();
        return false;
    }

    if (!$.isNumeric($('#txtScore2Cell').val())) {
        $.notify("Điểm 15 phút phải là số", "info");
        $("#txtScore2Cell").focus();
        return false;
    }

    if ($('#txtScore2Cell').val() < 0 | $('#txtScore2Cell').val() > 10) {
        $.notify("Điểm 15 phút phải có điểm từ 0 đến 10", "info");
        $("#txtScore2Cell").focus();
        return false;
    }

    if (!$.isNumeric($('#txtScore3Cell').val())) {
        $.notify("Điểm 1 tiết phải là số", "info");
        $("#txtScore3Cell").focus();
        return false;
    }

    if ($('#txtScore3Cell').val() < 0 | $('#txtScore3Cell').val() > 10) {
        $.notify("Điểm 1 tiết phải có điểm từ 0 đến 10", "info");
        $("#txtScore3Cell").focus();
        return false;
    }


    var param = {};
    param.strKeyWordStudent = $('#txtStudentIDModal').val();
    param.strKeyWordYear = $('#txtYearModal').val();
    param.strKeyWordSemeter = $('#txtSemeterModal').val();
    param.strKeyWordSubject = $('#sltKeyWordSubject').attr('data-value');
    param.strKeyWordScore1 = $('#txtScore1Cell').val();
    param.strKeyWordScore2 = $('#txtScore2Cell').val();
    param.strKeyWordScore3 = $('#txtScore3Cell').val();

    $.ajax({
        url: "/Score/ScoreEdit",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
            $("#btnEditScore").button('loading');
        },
        success: function (data) {
            if (data.iserror == false) {
                $.notify("Sửa điểm thành công", "info");
                var strtext2 = $('#txtStudentIDModal').val();
                $('.StudentIDCell:contains("' + strtext2 + '")').closest('tr').find('.Score1Cell').html($('#txtScore1Cell').val());
                $('.StudentIDCell:contains("' + strtext2 + '")').closest('tr').find('.Score2Cell').html($('#txtScore2Cell').val());
                $('.StudentIDCell:contains("' + strtext2 + '")').closest('tr').find('.Score3Cell').html($('#txtScore3Cell').val());
                CloseScoreEdit();
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

            $("#btnEditScore").button('reset');
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            $("#btnSearch").button('reset');
        }
    });
}
