﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class SystemSettingsDTO
    {
        public string SettingID { get; set; }
        public int MinimumAged { get; set; }
        public int MaximumAged { get; set; }
        public int MaximumStudentNumber { get; set; }
        public double StandardScore { get; set; }
    }
}
