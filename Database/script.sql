USE [master]
GO
/****** Object:  Database [StudentManagementDB]    Script Date: 31/05/2017 4:14:15 PM ******/
CREATE DATABASE [StudentManagementDB]
ALTER DATABASE [StudentManagementDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [StudentManagementDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [StudentManagementDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [StudentManagementDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [StudentManagementDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [StudentManagementDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [StudentManagementDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [StudentManagementDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [StudentManagementDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [StudentManagementDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [StudentManagementDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [StudentManagementDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [StudentManagementDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [StudentManagementDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [StudentManagementDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [StudentManagementDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [StudentManagementDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [StudentManagementDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [StudentManagementDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [StudentManagementDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [StudentManagementDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [StudentManagementDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [StudentManagementDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [StudentManagementDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [StudentManagementDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [StudentManagementDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [StudentManagementDB] SET  MULTI_USER 
GO
ALTER DATABASE [StudentManagementDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [StudentManagementDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [StudentManagementDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [StudentManagementDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [StudentManagementDB]
GO
/****** Object:  StoredProcedure [dbo].[checkPerKey]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[checkPerKey](
	@USENAME varchar(10),
	@KEY varchar(10)
)
as begin
select  Employee_Keys.IDKey
from Employees join Employee_Keys on(Employees.EmployeeID = Employee_Keys.IDEmployee)
where Employees.EmployeeID = @USENAME and Employee_Keys.IDKey = @KEY
UNION
select  Role_Keys.IDKey
from Employees join Role_Keys on(Employees.RoleID = Role_Keys.IDRole)
where Employees.EmployeeID = @USENAME and Role_Keys.IDKey = @KEY
end

GO
/****** Object:  StoredProcedure [dbo].[GetSubjectList]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetSubjectList]
as begin
select *
from Subjects
end

GO
/****** Object:  StoredProcedure [dbo].[usp_changepass]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_changepass](
@USERNAME varchar(10),
@PWS nvarchar(50)
)
as begin
update Employees
set Employees.Password = @PWS
where Employees.EmployeeID = @USERNAME

select *
from Employees
where Employees.EmployeeID = @USERNAME and Employees.Password = @PWS
end

GO
/****** Object:  StoredProcedure [dbo].[usp_keypermissiondata]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_keypermissiondata](
@username varchar(10)
)
as begin
select  Employee_Keys.IDKey
from Employees join Employee_Keys on(Employees.EmployeeID = Employee_Keys.IDEmployee)
where Employees.EmployeeID = @username
UNION
select  Role_Keys.IDKey
from Employees join Role_Keys on(Employees.RoleID = Role_Keys.IDRole)
where Employees.EmployeeID = @username
end

GO
/****** Object:  StoredProcedure [dbo].[usp_logincheck]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_logincheck](
@USERNAME varchar(10),
@PWS nvarchar(50)
)
as begin
select *
from Employees
where Employees.EmployeeID = @USERNAME and Employees.Password = @PWS
end

GO
/****** Object:  StoredProcedure [dbo].[usp_loginname]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[usp_loginname](
	@USERNAME varchar(10)
)
as begin
	select FirstName, LastName
	from Employees
	where EmployeeID = @USERNAME
end

GO
/****** Object:  StoredProcedure [dbo].[uspAlterIDByTable]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[uspAlterIDByTable]
	@tenbang varchar(max),
	@next_key char(6) out
AS
BEGIN
	DECLARE @sql nvarchar(max), @loaima varchar(max)
	--Lay loai ma theo tung truong hop
	IF(@tenbang = 'PermissionKeyList')
		SET @loaima = 'IDKey'
    IF(@tenbang = 'EmloyeeRoles')
		SET @loaima = 'RoleID'
	IF(@tenbang = 'Students')
		SET @loaima = 'StudentID'	
	IF(@tenbang = 'FamilyInformation')
		SET @loaima = 'MemberID'
	IF(@tenbang = 'SystemSettings')
		SET @loaima = 'SettingID'
		
	IF(@tenbang = 'Subjects')
		SET @loaima = 'SubjectID'
	--Lay ma cao nhat
	SET @sql = 'SELECT TOP 1 @cur_key = RTRIM(' + @loaima + ') FROM ' + @tenbang + ' ORDER BY ' + @loaima + ' DESC'
	DECLARE @cur_key CHAR(6)
	EXECUTE sp_executesql @sql, N'@cur_key CHAR(6) OUT', @cur_key OUT
	PRINT @cur_key
	--Phan ky tu dau cua ma
	DECLARE @str_begin CHAR(2)
	IF(@tenbang = 'PermissionKeyList')
		SET @str_begin = 'P'
    IF(@tenbang = 'EmloyeeRoles')
		SET @str_begin = 'E'
	 IF(@tenbang = 'Students')
		SET @str_begin = 'S'
	IF(@tenbang = 'FamilyInformation')
		SET @str_begin = 'F'
	IF(@tenbang = 'SystemSettings')
		SET @str_begin = 'SS'
	IF(@tenbang = 'Subjects')
		SET @str_begin = 'MH'
	--Lay phan so cua ma
	DECLARE @str_end CHAR(4)
	SET @str_end = ISNULL(RIGHT(@cur_key, 4), 0)
	--Lay so cua phan so
	DECLARE @num_end INT
	SET @num_end = CAST(@str_end AS INT)
	--Tao phan sau moi cua ma
	DECLARE @new_str_end char(4)
	SET @new_str_end = CAST((@num_end + 1) AS CHAR(4))
	--Chen them so 0 phia truoc phan so cua ma moi
	SET @new_str_end = REPLICATE('0', 4 - LEN(RTRIM(@new_str_end))) + RTRIM(@new_str_end)
	--Tao ma moi
	SET @next_key = RTRIM(@str_begin) + RTRIM(@new_str_end)
END

GO
/****** Object:  StoredProcedure [dbo].[uspChangeClassesName]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[uspChangeClassesName]
(
	@ClassesID varchar(10),
	@ClassesName varchar(50)
)
as begin 
	update Classes
	set ClassName = @ClassesName
	where ClassID = @ClassesID
	
	select *
	from Classes
	where ClassID = @ClassesID
	AND ClassName = @ClassesName
end

GO
/****** Object:  StoredProcedure [dbo].[uspClasses_GetAllClasses]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspClasses_GetAllClasses]
AS 
BEGIN
	SELECT ClassID,[ClassName],[Grade]
    FROM [StudentManagementDB].[dbo].[Classes]
END

GO
/****** Object:  StoredProcedure [dbo].[uspClassesStudents_Delete]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspClassesStudents_Delete]
	@ClassID as varchar(10),
	@Year as varchar(4)
AS
	DELETE FROM Classes_Students Where Year = @Year and ClassID = @ClassID

GO
/****** Object:  StoredProcedure [dbo].[uspClassesStudents_DeleteStudentFromClass]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspClassesStudents_DeleteStudentFromClass]
	@Year as varchar(4),
	@ClassID as varchar(10),
	@StudentID as varchar(10)
AS
	Delete 
	From  Classes_Students 
	Where	Year = @Year And
			ClassID = @ClassID And
			StudentID = @StudentID

GO
/****** Object:  StoredProcedure [dbo].[uspClassesStudents_GetStudentListByClassID]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspClassesStudents_GetStudentListByClassID]
    @PageSize int 
	,@PageIndex int
	,@Year varchar(4)
	,@ClassID varchar(10)
AS
BEGIN
SELECT 
		 temp.STT
		,temp.[StudentID]
		,temp.[FirstName]
		,temp.[LastName]
		,temp.[Address]
		,temp.[District]
		,temp.[CityorProvince]
		,temp.[Country]
		,temp.[PostalCode]
		,temp.[PhoneNumber]
		,temp.[EmailAddress]
		,temp.[GraduationYear]
		,temp.[Birthday]
		,temp.[IsDeleted]
		,temp.[Gender]
		,temp.[Grade]
		,temp.TotalRows
	FROM
		(
		    SELECT   ROW_NUMBER() OVER (ORDER BY S.StudentID ASC) AS 'STT'
					,S.[StudentID]
					,S.[FirstName]
					,S.[LastName]
					,S.[Address]
					,S.[District]
					,S.[CityorProvince]
					,S.[Country]
					,S.[PostalCode]
					,S.[PhoneNumber]
					,S.[EmailAddress]
					,S.[GraduationYear]
					,S.[Birthday]
					,S.[IsDeleted]
					,S.[Gender]
					,S.[Grade]
					,SUM(1) OVER()AS 'TotalRows'
			FROM Students S, Classes_Students C 
			WHERE	C.StudentID = S.StudentID 
					AND C.Year = @Year
					ANd C.ClassID = @ClassID And S.IsDeleted = 'FALSE' AND C.IsDeleted = 'FALSE'
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)

END

GO
/****** Object:  StoredProcedure [dbo].[uspClassesStudents_GetStudentListByGrade]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspClassesStudents_GetStudentListByGrade]
@Grade as varchar(2),
@ClassID as varchar(10),
@Year as varchar(4)
AS 
BEGIN
	
	SELECT  DISTINCT  S.StudentID,S.FirstName,S.LastName,S.Birthday,s.Gender,S.Address,S.District,S.CityorProvince,s.Country,S.PostalCode,S2.StudentNumber
	FROM (Students S  JOIN Classes C ON S.Grade = C.Grade) 
	JOIN (Select COUNT(*) AS StudentNumber
			From Classes_Students cs 
			Where cs.Year = @Year And cs.ClassID = @ClassID and cs.IsDeleted = 'false') AS S2 On 1 = 1
	WHERE	S.HasClass = '0' AND 
			S.IsDeleted = '0' AND
			S.Grade = @Grade
END

GO
/****** Object:  StoredProcedure [dbo].[uspClassesStudents_Insert]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspClassesStudents_Insert]
	@ClassID as varchar(10),
	@Year as varchar(4),
	@StudentID as varchar(10)
AS
	INSERT INTO [StudentManagementDB].[dbo].[Classes_Students]
           ([Year]
           ,[ClassID]
           ,[StudentID]
           ,[IsDeleted])
     VALUES
           (@Year
           ,@ClassID
           ,@StudentID
           ,'False');

GO
/****** Object:  StoredProcedure [dbo].[uspCreateIDByTable]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[uspCreateIDByTable]
	@tenbang varchar(max),
	@next_key char(6) out
AS
BEGIN
	DECLARE @sql nvarchar(max), @loaima varchar(max)
	--Lay loai ma theo tung truong hop
	IF(@tenbang = 'PermissionKeyList')
		SET @loaima = 'IDKey'
    IF(@tenbang = 'EmloyeeRoles')
		SET @loaima = 'RoleID'
	IF(@tenbang = 'Students')
		SET @loaima = 'StudentID'	
	IF(@tenbang = 'FamilyInformation')
		SET @loaima = 'MemberID'
	IF(@tenbang = 'SystemSettings')
		SET @loaima = 'SettingID'
	--Lay ma cao nhat
	SET @sql = 'SELECT TOP 1 @cur_key = RTRIM(' + @loaima + ') FROM ' + @tenbang + ' ORDER BY ' + @loaima + ' DESC'
	DECLARE @cur_key CHAR(6)
	EXECUTE sp_executesql @sql, N'@cur_key CHAR(6) OUT', @cur_key OUT
	PRINT @cur_key
	--Phan ky tu dau cua ma
	DECLARE @str_begin CHAR(2)
	IF(@tenbang = 'PermissionKeyList')
		SET @str_begin = 'P'
    IF(@tenbang = 'EmloyeeRoles')
		SET @str_begin = 'E'
	 IF(@tenbang = 'Students')
		SET @str_begin = 'S'
	IF(@tenbang = 'FamilyInformation')
		SET @str_begin = 'F'
	IF(@tenbang = 'SystemSettings')
		SET @str_begin = 'SS'
	--Lay phan so cua ma
	DECLARE @str_end CHAR(4)
	SET @str_end = ISNULL(RIGHT(@cur_key, 4), 0)
	--Lay so cua phan so
	DECLARE @num_end INT
	SET @num_end = CAST(@str_end AS INT)
	--Tao phan sau moi cua ma
	DECLARE @new_str_end char(4)
	SET @new_str_end = CAST((@num_end + 1) AS CHAR(4))
	--Chen them so 0 phia truoc phan so cua ma moi
	SET @new_str_end = REPLICATE('0', 4 - LEN(RTRIM(@new_str_end))) + RTRIM(@new_str_end)
	--Tao ma moi
	SET @next_key = RTRIM(@str_begin) + RTRIM(@new_str_end)
END

GO
/****** Object:  StoredProcedure [dbo].[uspDeleteClasses]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[uspDeleteClasses]
(
	@ClassesID varchar(10)
)
as begin 
	update Classes
	set IsDeleted = 'True'
	where ClassID = @ClassesID
	
	select *
	from Classes
	where ClassID = @ClassesID
	AND IsDeleted = 'False'
end

GO
/****** Object:  StoredProcedure [dbo].[uspDeleteEmployees]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspDeleteEmployees](
	@EmployeeID varchar(10)
 )
 as begin
	DELETE FROM Employees
	Where Employees.EmployeeID = @EmployeeID
end

GO
/****** Object:  StoredProcedure [dbo].[uspEmloyeeRoles_ADD]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEmloyeeRoles_ADD]
	@RoleName nvarchar(50)
AS
	DECLARE @RoleID varchar(10)
	EXEC uspAlterIDByTable 'EmloyeeRoles', @RoleID out

	INSERT 
	INTO EmloyeeRoles 
	(
		RoleID,
		RoleName
	)
	VALUES 
	(
		@RoleID,
		@RoleName
	)

	SELECT @RoleID;

GO
/****** Object:  StoredProcedure [dbo].[uspEmloyeeRoles_DEL]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEmloyeeRoles_DEL]
	@RoleID varchar(10)
AS
	DELETE FROM EmloyeeRoles
	WHERE RoleID = @RoleID

GO
/****** Object:  StoredProcedure [dbo].[uspEmloyeeRoles_SEL]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEmloyeeRoles_SEL]
	@RoleID varchar(10)
AS
	SELECT
		EmloyeeRoles.RoleID,
		EmloyeeRoles.RoleName
	FROM EmloyeeRoles
	WHERE EmloyeeRoles.RoleID = @RoleID

GO
/****** Object:  StoredProcedure [dbo].[uspEmloyeeRoles_UPD]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEmloyeeRoles_UPD]
	@RoleID varchar(10),
	@RoleName nvarchar(50)
AS
	UPDATE EmloyeeRoles
	SET RoleName = @RoleName
	WHERE RoleID = @RoleID

GO
/****** Object:  StoredProcedure [dbo].[uspEmloyees_ByRole_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspEmloyees_ByRole_SRH]
	@RoleID varchar(10)
AS
	SELECT Employees.*
	FROM Employees
    WHERE Employees.RoleID = @RoleID

GO
/****** Object:  StoredProcedure [dbo].[uspFamilyInfomation_Delete]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspFamilyInfomation_Delete]
		@StudentID as nvarchar(10)
		
AS
	Delete From FamilyInformation where StudentID = @StudentID

GO
/****** Object:  StoredProcedure [dbo].[uspFamilyInfomation_Insert]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspFamilyInfomation_Insert]
		 @Name as nvarchar(50)
		,@PhoneNumber	as nvarchar(15)		
		,@EmailAddress as nvarchar(255)
		,@Relationship as nvarchar(2)
		,@StudentID as nvarchar(10)
		
AS
	DECLARE @MemberID varchar(10)
	EXEC uspAlterIDByTable 'FamilyInformation', @MemberID out
	set @MemberID = RTRIM(@MemberID)
	INSERT INTO [StudentManagementDB].[dbo].[FamilyInformation]
           ([MemberID]
           ,[Name]
           ,[PhoneNumber]
           ,[Relationship]
           ,[StudentID]
           ,[EmailAddress])
     VALUES
           (@MemberID
           ,@Name
           ,@PhoneNumber
           ,@Relationship
           ,@StudentID
           ,@EmailAddress)
     Select @MemberID

GO
/****** Object:  StoredProcedure [dbo].[uspFamilyInformation_GetMemberByID]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspFamilyInformation_GetMemberByID]
	@StudentID varchar(10)
AS
	SELECT *
	FROM FamilyInformation
    WHERE FamilyInformation.StudentID = @StudentID

GO
/****** Object:  StoredProcedure [dbo].[uspGetClasses_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspGetClasses_SRH]
    @PageSize int,   
	@PageIndex int,
	@KeyWord nvarchar(100)
AS Begin
	SELECT 
		temp.STT,
		temp.ClassID,
		temp.ClassName,
		temp.Grade,
		temp.TotalRows
	FROM
		(
			SELECT
				ROW_NUMBER() OVER (ORDER BY Classes.ClassID ASC) AS 'STT', 
				Classes.ClassID,
				Classes.ClassName,
				Classes.Grade,
				SUM(1) OVER()AS 'TotalRows'
			FROM Classes
			WHERE 
				IsDeleted = 'False'
				AND(@KeyWord is null
					OR Classes.ClassID LIKE ('%' + @KeyWord + '%')
					OR Classes.ClassName LIKE ('%' + @KeyWord + '%'))
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)
 end

GO
/****** Object:  StoredProcedure [dbo].[uspGetClassListYear]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[uspGetClassListYear]
(
@KeyWordYear varchar(4)
)
as begin
select distinct Classes_Students.ClassID, Classes.ClassName
from Classes_Students join Classes on(Classes.ClassID = Classes_Students.ClassID)
where Classes_Students.Year = @KeyWordYear
AND Classes_Students.IsDeleted = 'False'
AND Classes.IsDeleted = 'False'
end

GO
/****** Object:  StoredProcedure [dbo].[uspGetDetailEmployeesByID]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspGetDetailEmployeesByID](
	@EmployeeID varchar(10)
)
as begin
	select EmployeeID, LastName, FirstName, Address, District,CityorProvince, PhoneNumber,EmailAddress,Employees.RoleID,EmloyeeRoles.RoleName
	from Employees join EmloyeeRoles on(Employees.RoleID = EmloyeeRoles.RoleID)
	where EmployeeID = @EmployeeID
end

GO
/****** Object:  StoredProcedure [dbo].[uspGetEmloyeeRoles_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGetEmloyeeRoles_SRH] 
    @PageSize int,   
	@PageIndex int,
	@KeyWord nvarchar(100)
AS
	SELECT 
		temp.STT,
		temp.RoleID,
		temp.RoleName,
		temp.TotalRows
	FROM
		(
			SELECT
				ROW_NUMBER() OVER (ORDER BY EmloyeeRoles.RoleID DESC) AS 'STT', 
				EmloyeeRoles.RoleID,
				EmloyeeRoles.RoleName,
				SUM(1) OVER()AS 'TotalRows'
			FROM EmloyeeRoles
			WHERE 
				@KeyWord is null
				OR EmloyeeRoles.RoleID LIKE ('%' + @KeyWord + '%')
				OR EmloyeeRoles.RoleName LIKE ('%' + @KeyWord + '%')
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)

GO
/****** Object:  StoredProcedure [dbo].[uspGetEmloyees_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGetEmloyees_SRH]
    @PageSize int,   
	@PageIndex int,
	@KeyWord nvarchar(100)
AS Begin
	SELECT 
		temp.STT,
		temp.EmployeeID,
		temp.FullName,
		temp.TotalRows
	FROM
		(
			SELECT
				ROW_NUMBER() OVER (ORDER BY Employees.EmployeeID ASC) AS 'STT', 
				Employees.EmployeeID,
				(Employees.LastName + ' ' + Employees.FirstName) AS FullName,
				SUM(1) OVER()AS 'TotalRows'
			FROM Employees
			WHERE 
				@KeyWord is null
				OR Employees.EmployeeID LIKE ('%' + @KeyWord + '%')
				OR Employees.LastName + ' ' +Employees.FirstName LIKE ('%' + @KeyWord + '%')
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)
 end

GO
/****** Object:  StoredProcedure [dbo].[uspGetEmployeesByID]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspGetEmployeesByID](
	@EmployeeID varchar(10)
)
as begin
	select *
	from Employees
	where EmployeeID = @EmployeeID
end

GO
/****** Object:  StoredProcedure [dbo].[uspGetReport]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspGetReport](
    @PageSize int,   
	@PageIndex int,
	@KeyWordYear varchar(4),
	@KeyWordSemeter varchar(1)
)
AS BEGIN
	declare @standardscore float 
	set @standardscore = (select StandardScore from SystemSettings)
	
	SELECT 
		temp.STT,
		temp.ClassName,
		temp.SISO,
		temp.SOLUONGDAT,
		temp.TYLE,
		temp.TotalRows
	FROM
		(
			select 
				ROW_NUMBER() OVER (ORDER BY TEMPTABLE.ClassID ASC) AS 'STT',
				*, 
				(cast(TEMPTABLE.SOLUONGDAT as float)/cast(TEMPTABLE.SISO as float) * 100) AS TYLE,
				SUM(1) OVER()AS 'TotalRows'
			from
				(	select Classes_Students.Year, Classes_Students.ClassID, Classes.ClassName, IsNull(COUNT(Classes_Students.StudentID),0) AS SISO, IsNull(SLDL.SOLUONGDAT,0) AS SOLUONGDAT
					from Classes_Students	join Classes on(Classes_Students.ClassID = Classes.ClassID) 
											left join (	select distinct CSD.ClassID, COUNT(Scores.StudentID) over(partition by CSD.ClassID) AS SOLUONGDAT
													from Scores join Classes_Students CSD on(CSD.StudentID = Scores.StudentID)
													where Scores.Semester = @KeyWordSemeter and Scores.Year = @KeyWordYear and CSD.IsDeleted ='False'
													group by CSD.ClassID, Scores.StudentID, Scores.Year, Scores.Semester
													having Avg(((cast(Scores.Score1 as float) + cast(Scores.Score2 as float) + cast(Scores.Score3 as float))/3)) >= @standardscore) AS SLDL on(SLDL.ClassID = Classes_Students.ClassID)
					where Classes_Students.IsDeleted = 'False' and Classes.IsDeleted = 'False'
			group by Classes_Students.Year, Classes_Students.ClassID, Classes.ClassName,SLDL.SOLUONGDAT	) AS TEMPTABLE
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetReport2]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspGetReport2](
    @PageSize int,   
	@PageIndex int,
	@KeyWordYear varchar(4),
	@KeyWordSemeter varchar(1),
	@KeyWordSubject varchar(10)
)
AS BEGIN
	declare @standardscore float 
	set @standardscore = (select StandardScore from SystemSettings)


	SELECT 
		temp.STT,
		temp.ClassName,
		temp.SISO,
		temp.SOLUONGDAT,
		temp.TYLE,
		temp.TotalRows
	FROM
		(
			select 
				ROW_NUMBER() OVER (ORDER BY TEMPTABLE.ClassID ASC) AS 'STT',
				*, 
				(cast(TEMPTABLE.SOLUONGDAT as float)/cast(TEMPTABLE.SISO as float) * 100) AS TYLE,
				SUM(1) OVER()AS 'TotalRows'
			from
				(	select Classes_Students.Year, Classes_Students.ClassID, Classes.ClassName, IsNull(COUNT(Classes_Students.StudentID),0) AS SISO, IsNull(SLDL.SOLUONGDAT,0) AS SOLUONGDAT
					from Classes_Students	join Classes on(Classes_Students.ClassID = Classes.ClassID) 
											left join (	select distinct CSD.ClassID, COUNT(Scores.StudentID) over(partition by CSD.ClassID) AS SOLUONGDAT
													from Scores join Classes_Students CSD on(CSD.StudentID = Scores.StudentID)
													where Scores.Semester = @KeyWordSemeter and Scores.Year = @KeyWordYear and Scores.SubjectID = @KeyWordSubject and CSD.IsDeleted ='False'
													group by CSD.ClassID, Scores.StudentID, Scores.Year, Scores.Semester
													having Avg(((cast(Scores.Score1 as float) + cast(Scores.Score2 as float) + cast(Scores.Score3 as float))/3)) >= @standardscore) AS SLDL on(SLDL.ClassID = Classes_Students.ClassID)
													where Classes_Students.IsDeleted = 'False' and Classes.IsDeleted = 'False'
			group by Classes_Students.Year, Classes_Students.ClassID, Classes.ClassName,SLDL.SOLUONGDAT	) AS TEMPTABLE
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetReportFull]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspGetReportFull](
	@KeyWordYear varchar(4),
	@KeyWordSemeter varchar(1)
)
AS BEGIN
	declare @standardscore float 
	set @standardscore = (select StandardScore from SystemSettings)
	SELECT 
		temp.STT,
		temp.ClassName as Lớp,
		temp.SISO,
		temp.SOLUONGDAT ,
		temp.TYLE
	FROM
		(
			select 
				ROW_NUMBER() OVER (ORDER BY TEMPTABLE.ClassID ASC) AS 'STT',
				*, 
				(cast(TEMPTABLE.SOLUONGDAT as float)/cast(TEMPTABLE.SISO as float) * 100) AS TYLE
			from
				(	select Classes_Students.Year, Classes_Students.ClassID, Classes.ClassName, IsNull(COUNT(Classes_Students.StudentID),0) AS SISO, IsNull(SLDL.SOLUONGDAT,0) AS SOLUONGDAT
					from Classes_Students	join Classes on(Classes_Students.ClassID = Classes.ClassID) 
											left join (	select distinct CSD.ClassID, COUNT(Scores.StudentID) over(partition by CSD.ClassID) AS SOLUONGDAT
													from Scores join Classes_Students CSD on(CSD.StudentID = Scores.StudentID)
													where Scores.Semester = @KeyWordSemeter and Scores.Year = @KeyWordYear and CSD.IsDeleted ='False'
													group by CSD.ClassID, Scores.StudentID, Scores.Year, Scores.Semester
													having Avg(((cast(Scores.Score1 as float) + cast(Scores.Score2 as float) + cast(Scores.Score3 as float))/3)) >= @standardscore) AS SLDL on(SLDL.ClassID = Classes_Students.ClassID)
													where Classes_Students.IsDeleted = 'False' and Classes.IsDeleted = 'False'
			group by Classes_Students.Year, Classes_Students.ClassID, Classes.ClassName,SLDL.SOLUONGDAT	) AS TEMPTABLE
		) AS temp
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetRoleList]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspGetRoleList]
as begin
	select *
	from EmloyeeRoles
end

GO
/****** Object:  StoredProcedure [dbo].[uspGetScore]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspGetScore](
    @PageSize int,   
	@PageIndex int,
	@KeyWordStudent varchar(10),
	@KeyWordYear varchar(4),
	@KeyWordClass varchar(10),
	@KeyWordSubject varchar(10),
	@KeyWordSemeter varchar(1)
)
AS BEGIN
	SELECT 
		*
	FROM
		(
			select 
				ROW_NUMBER() OVER (ORDER BY TEMPTABLE.StudentID ASC) AS 'STT',
				*, 
				SUM(1) OVER()AS 'TotalRows'
			from
				(	select Students.StudentID, (Students.LastName + ' ' + Students.FirstName) AS FullName, Scores.Year, Scores.Semester, Scores.Score1, Scores.Score2, Scores.Score3
					from Scores join Students on(Scores.StudentID = Students.StudentID)
								join Subjects on(Scores.SubjectID = Subjects.SubjectID)
								join Classes_Students on(Classes_Students.StudentID = Students.StudentID)
					where	Scores.Year = @KeyWordYear
							AND Classes_Students.IsDeleted = 'False'
							AND Scores.Semester = @KeyWordSemeter
							AND Scores.SubjectID = @KeyWordSubject
							AND Classes_Students.ClassID = @KeyWordClass
							AND (@KeyWordStudent = '' or Scores.StudentID = @KeyWordStudent)	) AS TEMPTABLE
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)
END

GO
/****** Object:  StoredProcedure [dbo].[uspGetScoreCorrections_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGetScoreCorrections_SRH]
    @PageSize int,   
	@PageIndex int,
	@KeyWord nvarchar(100),
	@StudentID varchar(10),
	@Year varchar(4),
	@Semester varchar(1),
	@SubjectID varchar(10),
	@UserCreateID varchar(10)
AS
	SELECT 
		temp.STT,
		temp.ScoreCorrectionID,
		temp.ScoreID,
		temp.UserCreateID,
		temp.IsAccepted,
		temp.UserDecideID,
		temp.InsertedDatetime,
		temp.LastModified,
		temp.StudentID,
		temp.Year,
		temp.Semester,
		temp.SubjectID,
		temp.TotalRows
	FROM
	(
		SELECT	ROW_NUMBER() OVER (ORDER BY SC.IsAccepted, SC.ScoreCorrectionID DESC) AS 'STT', 
				SC.ScoreCorrectionID,
				SC.ScoreID,
				SC.UserCreateID,
				SC.IsAccepted,
				SC.UserDecideID,
				SC.InsertedDatetime,
				SC.LastModified,
				S.StudentID,
				S.Year,
				S.Semester,
				S.SubjectID,
				SUM(1) OVER()AS 'TotalRows'
		FROM	ScoreCorrections AS SC, Scores AS S
		WHERE	SC.ScoreID = S.INDEXAUTO AND
				(@StudentID is null	OR S.StudentID LIKE ('%' + @StudentID + '%')) AND
				(@Year is null	OR S.Year LIKE ('%' + @Year + '%')) AND
				(@Semester is null	OR S.Semester LIKE ('%' + @Semester + '%')) AND
				(@SubjectID is null	OR S.SubjectID LIKE ('%' + @SubjectID + '%')) AND
				(@UserCreateID is null OR SC.UserCreateID LIKE ('%' + @UserCreateID + '%'))
	) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)

GO
/****** Object:  StoredProcedure [dbo].[uspGetScoreForCorrections_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGetScoreForCorrections_SRH]
    @PageSize int,   
	@PageIndex int,
	@KeyWord nvarchar(100),
	@StudentID varchar(10),
	@Year varchar(4),
	@Semester varchar(1),
	@SubjectID varchar(10)
AS
	SELECT 
		temp.STT,
		temp.ScoreID,
		temp.Year,
		temp.Semester,
		temp.SubjectID,
		temp.StudentID,
		temp.Score1,
		temp.Score2,
		temp.Score3,
		temp.TotalRows
	FROM
	(
		SELECT	ROW_NUMBER() OVER (ORDER BY S.INDEXAUTO DESC) AS 'STT', 
				S.INDEXAUTO as ScoreID,
				S.StudentID,
				S.Year,
				S.Semester,
				S.SubjectID,
				S.Score1,
				S.Score2,
				S.Score3,
				SUM(1) OVER()AS 'TotalRows'
		FROM	Scores AS S
		WHERE	
				(@StudentID is null	OR S.StudentID LIKE ('%' + @StudentID + '%')) AND
				(@Year is null	OR S.Year LIKE ('%' + @Year + '%')) AND
				(@Semester is null	OR S.Semester LIKE ('%' + @Semester + '%')) AND
				(@SubjectID is null	OR S.SubjectID LIKE ('%' + @SubjectID + '%'))
	) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)

GO
/****** Object:  StoredProcedure [dbo].[uspGetSubjectList]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[uspGetSubjectList]
as begin
select *
from Subjects
where Subjects.IsDeleted = 'False'
end

GO
/****** Object:  StoredProcedure [dbo].[uspGetSubjects_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGetSubjects_SRH]
    @PageSize int,   
	@PageIndex int,
	@KeyWord nvarchar(100)
AS
	SELECT 
		temp.STT,
		temp.SubjectID,
		temp.SubjectName,
		temp.TotalRows
	FROM
		(
			SELECT
				ROW_NUMBER() OVER (ORDER BY Subjects.SubjectID DESC) AS 'STT', 
				Subjects.SubjectID,
				Subjects.SubjectName,
				SUM(1) OVER()AS 'TotalRows'
			FROM Subjects
			WHERE 
				(@KeyWord is null
				OR Subjects.SubjectID LIKE ('%' + @KeyWord + '%')
				OR Subjects.SubjectName LIKE ('%' + @KeyWord + '%'))
				AND Subjects.IsDeleted = 0
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)

GO
/****** Object:  StoredProcedure [dbo].[uspGetSystemSettings_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--uspGetSystemSettings_SRH
CREATE PROCEDURE [dbo].[uspGetSystemSettings_SRH]
    @PageSize int,   
	@PageIndex int,
	@KeyWord nvarchar(100)
AS
	SELECT 
		temp.STT,
		temp.SettingID,
		temp.Value,
		temp.Object,
		temp.TotalRows
	FROM
		(
			SELECT
				ROW_NUMBER() OVER (ORDER BY SystemSettings.SettingID DESC) AS 'STT', 
				SystemSettings.SettingID,
				SystemSettings.Value,
				SystemSettings.Object,
				SUM(1) OVER()AS 'TotalRows'
			FROM SystemSettings
			WHERE 
				(@KeyWord is null
				OR SystemSettings.SettingID LIKE ('%' + @KeyWord + '%')
				OR SystemSettings.Object LIKE ('%' + @KeyWord + '%'))
				AND SystemSettings.IsDeleted = 0
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)

GO
/****** Object:  StoredProcedure [dbo].[uspGetYearList]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspGetYearList]
AS BEGIN
	select distinct Classes_Students.Year
	from Classes_Students join Classes on(Classes_Students.ClassID = Classes.ClassID)
	where Classes_Students.IsDeleted = 'False' And Classes.IsDeleted = 'False'
END

GO
/****** Object:  StoredProcedure [dbo].[uspInsertClasses]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[uspInsertClasses]
(
	@ClassesName varchar(50),
	@ClassesGrade varchar(2)
)
as begin
	declare @sql nvarchar(max), @next_key char(6)
	SET @sql = 'SELECT TOP 1 @cur_key = RTRIM(ClassID) FROM Classes ORDER BY Classes.ClassID DESC'
	DECLARE @cur_key CHAR(6)
	EXECUTE sp_executesql @sql, N'@cur_key CHAR(6) OUT', @cur_key OUT
	--Phan ky tu dau cua ma
	DECLARE @str_begin CHAR(2)
		SET @str_begin = 'C'
	--Lay phan so cua ma
	DECLARE @str_end CHAR(4)
	SET @str_end = ISNULL(RIGHT(@cur_key, 5), 0)
	--Lay so cua phan so
	DECLARE @num_end INT
	SET @num_end = CAST(@str_end AS INT)
	--Tao phan sau moi cua ma
	DECLARE @new_str_end char(4)
	SET @new_str_end = CAST((@num_end + 1) AS CHAR(3))
	--Chen them so 0 phia truoc phan so cua ma moi
	SET @new_str_end = REPLICATE('0', 3 - LEN(RTRIM(@new_str_end))) + RTRIM(@new_str_end)
	--Tao ma moi
	SET @next_key = RTRIM(@str_begin) + RTRIM(@new_str_end)
	
	insert into Classes
	values(@next_key, @ClassesName, @ClassesGrade, 'False')
	
	select *
	from Classes
	where ClassID = @next_key
end

GO
/****** Object:  StoredProcedure [dbo].[uspInsertEmployee]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspInsertEmployee]
(
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@Address varchar(6),
	@Distric nvarchar(200),
	@City nvarchar(100),
	@Phone varchar(11),
	@Email varchar(max),
	@Role varchar(10),
	@Pass varchar(max)
)
as begin

	declare @sql nvarchar(max), @next_key char(6)
	SET @sql = 'SELECT TOP 1 @cur_key = RTRIM(EmployeeID) FROM Employees ORDER BY EmployeeID DESC'
	DECLARE @cur_key CHAR(6)
	EXECUTE sp_executesql @sql, N'@cur_key CHAR(6) OUT', @cur_key OUT
	--Phan ky tu dau cua ma
	DECLARE @str_begin CHAR(2)
		SET @str_begin = 'E'
	--Lay phan so cua ma
	DECLARE @str_end CHAR(4)
	SET @str_end = ISNULL(RIGHT(@cur_key, 5), 0)
	--Lay so cua phan so
	DECLARE @num_end INT
	SET @num_end = CAST(@str_end AS INT)
	--Tao phan sau moi cua ma
	DECLARE @new_str_end char(4)
	SET @new_str_end = CAST((@num_end + 1) AS CHAR(3))
	--Chen them so 0 phia truoc phan so cua ma moi
	SET @new_str_end = REPLICATE('0', 3 - LEN(RTRIM(@new_str_end))) + RTRIM(@new_str_end)
	--Tao ma moi
	SET @next_key = RTRIM(@str_begin) + RTRIM(@new_str_end)
	select @next_key
	
	insert into Employees(EmployeeID,FirstName,LastName,Address,District,CityorProvince,PhoneNumber,EmailAddress,RoleID,IsDeleted,Password)
	values(@next_key, @FirstName, @LastName, @Address, @Distric, @City, @Phone, @Email, @Role, 0, @Pass)
	
	select *
	from Employees
	where Employees.EmployeeID = @next_key
end

GO
/****** Object:  StoredProcedure [dbo].[uspPermission_ByRole_SEL]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspPermission_ByRole_SEL]
	@RoleID varchar(10)
AS
	SELECT
		PermissionKeyList.IDKey,
		PermissionKeyList.KeyDescription
	FROM 
		Role_Keys
			JOIN PermissionKeyList
			ON Role_Keys.IDKey = PermissionKeyList.IDKey
	WHERE 
		Role_Keys.IDRole = @RoleID

GO
/****** Object:  StoredProcedure [dbo].[uspPermissionKeyList_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspPermissionKeyList_SRH]
    @PageSize int,   
	@PageIndex int,
	@KeyWord nvarchar(100)
AS
	SELECT 
		temp.STT,
		temp.IDKey,
		temp.KeyDescription,
		temp.TotalRows
	FROM
		(
			SELECT
				ROW_NUMBER() OVER (ORDER BY PermissionKeyList.IDKey DESC) AS 'STT', 
				PermissionKeyList.IDKey,
				PermissionKeyList.KeyDescription,
				SUM(1) OVER()AS 'TotalRows'
			FROM PermissionKeyList
			WHERE 
				@KeyWord is null
				OR PermissionKeyList.IDKey LIKE ('%' + @KeyWord + '%')
				OR PermissionKeyList.KeyDescription LIKE ('%' + @KeyWord + '%')
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)

GO
/****** Object:  StoredProcedure [dbo].[uspRole_Keys_ADD]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRole_Keys_ADD]
	@RoleID varchar(10),
	@LstPermissionID varchar(1000)
AS
	DECLARE @PermissionID VARCHAR(10)

    WHILE LEN(@LstPermissionID) > 0
    BEGIN
		SET @PermissionID = LEFT(@LstPermissionID, 
									ISNULL(NULLIF(CHARINDEX(',', @LstPermissionID) - 1, -1),
									LEN(@LstPermissionID)))
        SET @LstPermissionID = SUBSTRING(@LstPermissionID,
                                     ISNULL(NULLIF(CHARINDEX(',', @LstPermissionID), 0),
                                     LEN(@LstPermissionID)) + 1, LEN(@LstPermissionID))

        INSERT INTO Role_Keys(IDRole, IDKey)
        VALUES (@RoleID, @PermissionID)
    END

GO
/****** Object:  StoredProcedure [dbo].[uspRole_Keys_DEL]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRole_Keys_DEL]
	@RoleID varchar(10)
AS
	DELETE FROM Role_Keys
	WHERE IDRole = @RoleID

GO
/****** Object:  StoredProcedure [dbo].[uspScoreCorrection_ADD]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspScoreCorrection_ADD]
	@ScoreCorrectionID int = null,
	@ScoreID int = null,
	@UserCreateID varchar(10) = null,
	@IsAccepted bit = null,
	@UserDecideID varchar(10) = null,
	@InsertedDatetime datetime = null,
	@LastModified datetime = null
AS
	DECLARE @output table( ID int)
	IF (@IsAccepted = '')
		SET @IsAccepted = null
	SET @UserDecideID = null
	SET @InsertedDatetime = GETDATE()
	SET @LastModified = @InsertedDatetime
	INSERT ScoreCorrections
	(
		ScoreID,
		UserCreateID,
		IsAccepted,
		UserDecideID,
		InsertedDatetime,
		LastModified
	)
	OUTPUT inserted.ScoreCorrectionID INTO  @output
	VALUES 
	(
		@ScoreID,
		@UserCreateID,
		@IsAccepted,
		@UserDecideID,
		@InsertedDatetime,
		@LastModified
	)

	SELECT ID
	FROM @output

GO
/****** Object:  StoredProcedure [dbo].[uspScoreCorrection_SEL]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspScoreCorrection_SEL]
	@ScoreCorrectionID int
AS
	SELECT	SC.ScoreCorrectionID,
			SC.ScoreID,
			SC.UserCreateID,
			SC.IsAccepted,
			SC.UserDecideID,
			SC.InsertedDatetime,
			SC.LastModified,
			S.Year,
			S.Semester,
			S.SubjectID,
			S.StudentID
	FROM	ScoreCorrections as SC, Scores as S
	WHERE	SC.ScoreID = S.INDEXAUTO AND ScoreCorrectionID = @ScoreCorrectionID

GO
/****** Object:  StoredProcedure [dbo].[uspScoreCorrectionDetail_ADD]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[uspScoreCorrectionDetail_ADD]
	@ScoreCorrectionDetailID int = NULL,
	@ScoreCorrectionID int = NULL,
	@ScoreType int = NULL,
	@ScoreOld float = NULL,
	@ScoreNew float = NULL
AS
	DECLARE @output table( ID int)
	INSERT ScoreCorrectionDetails
	(
		ScoreCorrectionID, 
		ScoreType, 
		ScoreOld, 
		ScoreNew
	)
	OUTPUT inserted.ScoreCorrectionDetailID INTO  @output
	VALUES 
	(
		@ScoreCorrectionID, 
		@ScoreType, 
		@ScoreOld, 
		@ScoreNew
	)

	SELECT ID
	FROM @output;

GO
/****** Object:  StoredProcedure [dbo].[uspScoreCorrectionDetail_BySC_SEL]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspScoreCorrectionDetail_BySC_SEL]
	@ScoreCorrectionID int
AS
	SELECT  ScoreCorrectionDetailID, ScoreCorrectionID, 
			ScoreType, ScoreOld, ScoreNew
	FROM	ScoreCorrectionDetails
	WHERE	ScoreCorrectionID = @ScoreCorrectionID
	ORDER BY ScoreType

GO
/****** Object:  StoredProcedure [dbo].[uspScoreEdit]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[uspScoreEdit]
(
	@KeyWordStudent varchar(10),
	@KeyWordYear varchar(4),
	@KeyWordSemester varchar(1),
	@KeyWordSubject varchar(10),
	@KeyWordScore1 float,
	@KeyWordScore2 float,
	@KeyWordScore3 float
)
as begin
	UPDATE Scores
	set Score1 = @KeyWordScore1, Score2 = @KeyWordScore2, Score3 = @KeyWordScore3
	where	Scores.StudentID = @KeyWordStudent
			and Scores.Year = @KeyWordYear
			and Scores.Semester = @KeyWordSemester
			and Scores.SubjectID = @KeyWordSubject
end

GO
/****** Object:  StoredProcedure [dbo].[uspScores_GetScoresByStudentID]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspScores_GetScoresByStudentID]
		 @StudentID as varchar(10)
		,@Year	as varchar(4)		
		
AS
	Select S.StudentID 
	From Scores S
	Where S.StudentID = @StudentID And S.Year = @Year

GO
/****** Object:  StoredProcedure [dbo].[uspScores_Insert]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspScores_Insert]
		 @StudentID as varchar(10)
		,@Year	as varchar(4)		
		
AS
	
	DECLARE @SUBJECTID AS VARCHAR(10)
	DECLARE cs_SUBJLST CURSOR FOR (SELECT [SubjectID]
					FROM [StudentManagementDB].[dbo].[Subjects])
	OPEN cs_SUBJLST
	FETCH NEXT FROM  cs_SUBJLST INTO @SUBJECTID
	WHILE @@FETCH_STATUS = 0 --(5)
	BEGIN
		INSERT INTO [StudentManagementDB].[dbo].[Scores]
           ([StudentID]
           ,[Year]
           ,[Semester]
           ,[SubjectID]
           ,[Score1]
           ,[Score2]
           ,[Score3])
        VALUES
           (@StudentID
           ,@Year
           ,'1'
           ,@SUBJECTID
           ,'0'
           ,'0'
           ,'0')
           
           INSERT INTO [StudentManagementDB].[dbo].[Scores]
           ([StudentID]
           ,[Year]
           ,[Semester]
           ,[SubjectID]
           ,[Score1]
           ,[Score2]
           ,[Score3])
        VALUES
           (@StudentID
           ,@Year
           ,'2'
           ,@SUBJECTID
           ,'0'
           ,'0'
           ,'0')
			FETCH NEXT FROM  cs_SUBJLST INTO @SUBJECTID
	END
	CLOSE cs_SUBJLST 
	DEALLOCATE cs_SUBJLST

GO
/****** Object:  StoredProcedure [dbo].[uspSetAcceptScoreCorrection]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[uspSetAcceptScoreCorrection]
	@ScoreCorrectionID int = null,
	@IsAccepted bit = null, 
	@UserDecideID varchar(10)
AS
	IF (@IsAccepted = 1) 
	BEGIN
		DECLARE @score1 float, @score2 float, @score3 float, @scoreid int
	
		SELECT	@scoreid = ScoreID
		FROM	ScoreCorrections
		WHERE	ScoreCorrectionID = @ScoreCorrectionID
		SELECT	@score1 = Score1, @score2 = Score2, @score3 = Score3
		FROM	Scores
		WHERE	INDEXAUTO = @scoreid
	
		DECLARE cur CURSOR
		FOR SELECT	ScoreType, ScoreNew
		FROM	ScoreCorrectionDetails
		WHERE	ScoreCorrectionID = @ScoreCorrectionID
		ORDER BY ScoreType
		OPEN cur
		DECLARE @ScoreType int, @ScoreNew float
		FETCH NEXT FROM cur INTO @ScoreType, @ScoreNew
		WHILE(@@FETCH_STATUS = 0)
		BEGIN
			IF (@ScoreType = 0) 
				SET @score1 = @ScoreNew
			ELSE IF (@ScoreType = 1) 
				SET @score2 = @ScoreNew
			ELSE IF (@ScoreType = 2) 
				SET @score3 = @ScoreNew

			FETCH NEXT FROM cur INTO @ScoreType, @ScoreNew
		END
		UPDATE	Scores
		SET		Score1 = @score1, 
				Score2 = @score2, 
				Score3 = @score3
		WHERE	INDEXAUTO = @scoreid
		CLOSE cur
		DEALLOCATE cur
	END
	
	UPDATE	ScoreCorrections
	SET		IsAccepted = @IsAccepted,
			UserDecideID = @UserDecideID,
			LastModified = GETDATE()
	WHERE	ScoreCorrectionID = @ScoreCorrectionID
	SELECT @ScoreCorrectionID

GO
/****** Object:  StoredProcedure [dbo].[uspStudents_Delete]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspStudents_Delete]
		@StudentID as varchar(10)
		
AS
	UPDATE [StudentManagementDB].[dbo].[Students]
	   SET [IsDeleted] = 'TRUE'		
	 WHERE [StudentID] = @StudentID
	 
	 	UPDATE [StudentManagementDB].[dbo].[Classes_Students]
	   SET [IsDeleted] = 'TRUE'		
	 WHERE [StudentID] = @StudentID

GO
/****** Object:  StoredProcedure [dbo].[uspStudents_GetList]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspStudents_GetList] 
    @PageSize int,   
	@PageIndex int,
	@KeyWord nvarchar(100)
AS
	SELECT 
		temp.STT,
		temp.StudentID,
		temp.FirstName,
		temp.LastName,
		temp.TotalRows
	FROM
		(
			SELECT
				ROW_NUMBER() OVER (ORDER BY Students.StudentID DESC) AS 'STT', 
				Students.StudentID,
				Students.FirstName,
				Students.LastName,
				SUM(1) OVER()AS 'TotalRows'
			FROM Students
			WHERE 
				(@KeyWord is null
				OR Students.StudentID LIKE ('%' + @KeyWord + '%')
				OR Students.FirstName LIKE ('%' + @KeyWord + '%')
				OR Students.LastName LIKE ('%' + @KeyWord + '%')) And Students.IsDeleted = '0'
		) AS temp
     WHERE @PageSize < 0 
			OR
          (temp.STT > @PageIndex * @PageSize AND temp.STT <= (@PageIndex + 1) * @PageSize)

GO
/****** Object:  StoredProcedure [dbo].[uspStudents_GetStudentByID]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspStudents_GetStudentByID]
	@StudentID varchar(10)
AS
	SELECT *
	FROM Students
    WHERE Students.StudentID = @StudentID And Students.IsDeleted = '0'

GO
/****** Object:  StoredProcedure [dbo].[uspStudents_Insert]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspStudents_Insert]
		 @FirstName as nvarchar(50)
		,@LastName	as nvarchar(50)		
		,@Address as nvarchar(255)
		,@District as nvarchar(255)
		,@CityProvince as nvarchar(255)
		,@Country as nvarchar(255)
		,@PostalCode as varchar(10)
		,@EmailAddress as nvarchar(255)
		,@PhoneNumber as varchar(15)
		,@GraduationYear as varchar(4)
		,@Birthday as date
		,@IsDeleted as bit
		,@Gender as nvarchar(1)
		,@Grade as varchar(2)
		,@YearOfAdmission as varchar(4)
AS
	DECLARE @StudentID varchar(10)
	EXEC uspAlterIDByTable 'Students', @StudentID out
	set @StudentID =  RTRIM(@StudentID)
	INSERT INTO [StudentManagementDB].[dbo].[Students]
           ([StudentID]
           ,[FirstName]
           ,[LastName]
           ,[Address]
           ,[District]
           ,[CityorProvince]
           ,[Country]
           ,[PostalCode]
           ,[PhoneNumber]
           ,[EmailAddress]
           ,[GraduationYear]
           ,[Birthday]
           ,[IsDeleted]
           ,[Gender]
           ,[Grade]
           ,[HasClass]
           ,[YearOfAdmission])
     VALUES
           ( @StudentID
            ,@FirstName
			,@LastName			
			,@Address
			,@District
			,@CityProvince
			,@Country
			,@PostalCode
			,@PhoneNumber
			,@EmailAddress
			,@GraduationYear
			,@Birthday
			,@IsDeleted
			,@Gender
			,@Grade
			,'FALSE'
			,@YearOfAdmission)

	SELECT @StudentID as StudentID;

GO
/****** Object:  StoredProcedure [dbo].[uspStudents_Update]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspStudents_Update]
		@StudentID as varchar(10)
		,@FirstName as nvarchar(50)
		,@LastName	as nvarchar(50)		
		,@Address as nvarchar(255)
		,@District as nvarchar(255)
		,@CityProvince as nvarchar(255)
		,@Country as nvarchar(255)
		,@PostalCode as varchar(10)
		,@EmailAddress as nvarchar(255)
		,@PhoneNumber as varchar(15)
		,@GraduationYear as varchar(4)
		,@Birthday as date
		,@IsDeleted as bit
		,@Gender as nvarchar(1)
		,@Grade as varchar(2)
AS
	UPDATE [StudentManagementDB].[dbo].[Students]
	   SET [FirstName] = @FirstName
		  ,[LastName] = @LastName
		  ,[Address] = @Address
		  ,[District] = @District
		  ,[CityorProvince] =@CityProvince
		  ,[Country] = @Country
		  ,[PostalCode] = @PostalCode
		  ,[PhoneNumber] = @PhoneNumber
		  ,[EmailAddress] = @EmailAddress
		  ,[GraduationYear] = @GraduationYear
		  ,[Birthday] = @Birthday
		  ,[Gender] = @Gender
		  ,[Grade] = @Grade
	 WHERE [StudentID] = @StudentID
	 Select @StudentID

GO
/****** Object:  StoredProcedure [dbo].[uspStudents_UpdateClassStatus]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspStudents_UpdateClassStatus]
	@StudentID as varchar(10)
AS
	UPDATE Students SET HasClass = 'TRUE' WHERE StudentID = @StudentID

GO
/****** Object:  StoredProcedure [dbo].[uspStudents_UpdateClassStatus_False]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspStudents_UpdateClassStatus_False]
	@StudentID as varchar(10)
AS
	UPDATE Students SET HasClass = 'FALSE' WHERE StudentID = @StudentID

GO
/****** Object:  StoredProcedure [dbo].[uspSubjects_ADD]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspSubjects_ADD]
	@SubjectName nvarchar(255)
AS
	DECLARE @SubjectID varchar(10)
	EXEC uspAlterIDByTable 'Subjects', @SubjectID out

	INSERT 
	INTO Subjects 
	(
		SubjectID,
		SubjectName,
		IsDeleted
	)
	VALUES 
	(
		@SubjectID,
		@SubjectName,
		0
	)
	Select @SubjectID

GO
/****** Object:  StoredProcedure [dbo].[uspSubjects_DEL]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspSubjects_DEL]
	@SubjectID varchar(10)
AS
	UPDATE Subjects
	SET IsDeleted = 1
	WHERE SubjectID = @SubjectID

GO
/****** Object:  StoredProcedure [dbo].[uspSubjects_SEL]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspSubjects_SEL]
	@SubjectID varchar(10)
AS
	SELECT
		Subjects.SubjectID,
		Subjects.SubjectName
	FROM Subjects
	WHERE Subjects.SubjectID = @SubjectID

GO
/****** Object:  StoredProcedure [dbo].[uspSubjects_UPD]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspSubjects_UPD]
	@SubjectID varchar(10),
	@SubjectName nvarchar(255)
AS
	UPDATE Subjects
	SET SubjectName = @SubjectName
	WHERE SubjectID = @SubjectID

GO
/****** Object:  StoredProcedure [dbo].[uspSystemSettings_ADD]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspSystemSettings_ADD]
	@Value varchar(10),
	@Object nvarchar(50)
AS
	DECLARE @SettingID varchar(10)
	EXEC uspAlterIDByTable 'SystemSettings', @SettingID out

	INSERT 
	INTO SystemSettings 
	(
		SettingID,
		Value,
		Object,
		IsDeleted
	)
	VALUES 
	(
		@SettingID,
		@Value,
		@Object,
		0
	)

GO
/****** Object:  StoredProcedure [dbo].[uspSystemSettings_ByID_SRH]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--uspSystemSettings_ByID_SRH
CREATE PROCEDURE [dbo].[uspSystemSettings_ByID_SRH]
	@SettingID varchar(10)
AS
	SELECT SystemSettings.*
	FROM SystemSettings
    WHERE SystemSettings.SettingID = @SettingID

GO
/****** Object:  StoredProcedure [dbo].[uspSystemSettings_DEL]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspSystemSettings_DEL]
	@SettingID varchar(10)
AS
	UPDATE SystemSettings
	SET IsDeleted = 1
	WHERE SettingID = @SettingID

GO
/****** Object:  StoredProcedure [dbo].[uspSystemSettings_GetSetting]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspSystemSettings_GetSetting]
as 
begin
	Select * From SystemSettings 
end

GO
/****** Object:  StoredProcedure [dbo].[uspSystemSettings_UPD]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--uspSystemSettings_UPD
CREATE PROCEDURE [dbo].[uspSystemSettings_UPD]
	@SettingID varchar(10),
	@Value varchar(10),
	@Object nvarchar(50)
AS
	UPDATE SystemSettings
	SET Value = @Value, Object = @Object
	WHERE SettingID = @SettingID

GO
/****** Object:  StoredProcedure [dbo].[uspSystemSettings_Update]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspSystemSettings_Update]
@SettingID as varchar(10),
@MinimumAged as int,
@MaximumAged as int,
@MaximumStudentNumber as int,
@StandardScore as float
AS 
BEGIN
	UPDATE [StudentManagementDB].[dbo].[SystemSettings]
	   SET [MinimumAged] = @MinimumAged
		  ,[MaximumAged] = @MaximumAged
		  ,[MaxinumStudentNumber] = @MaximumStudentNumber
		  ,[StandardScore] = @StandardScore
	 WHERE [SettingID] = @SettingID
END

GO
/****** Object:  StoredProcedure [dbo].[uspUpdateEmployee]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspUpdateEmployee]
(
	@EmployeeID varchar(10),
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@Address varchar(6),
	@Distric nvarchar(200),
	@City nvarchar(100),
	@Phone varchar(11),
	@Email varchar(max),
	@Role varchar(10)
)
as begin
	update Employees
	set FirstName = @FirstName, LastName = @LastName, Address = @Address, District = @Distric, CityorProvince = @City, PhoneNumber = @Phone, @Email = @Email, RoleID = @Role
	where EmployeeID = @EmployeeID
	
	select *
	from Employees
	where Employees.EmployeeID = @EmployeeID
end

GO
/****** Object:  Table [dbo].[Classes]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Classes](
	[ClassID] [varchar](10) NOT NULL,
	[ClassName] [varchar](10) NULL,
	[Grade] [varchar](2) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Classes_Students]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Classes_Students](
	[Year] [varchar](4) NOT NULL,
	[ClassID] [varchar](10) NOT NULL,
	[StudentID] [varchar](10) NOT NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_ClasssStudent] PRIMARY KEY CLUSTERED 
(
	[Year] ASC,
	[ClassID] ASC,
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmloyeeRoles]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmloyeeRoles](
	[RoleID] [varchar](10) NOT NULL,
	[RoleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_EmloyeeRoles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employee_Keys]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee_Keys](
	[IDEmployee] [varchar](10) NOT NULL,
	[IDKey] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IDEmployee] ASC,
	[IDKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employees](
	[EmployeeID] [varchar](10) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[District] [nvarchar](255) NULL,
	[CityorProvince] [nvarchar](255) NULL,
	[PhoneNumber] [varchar](15) NULL,
	[EmailAddress] [nvarchar](255) NULL,
	[RoleID] [varchar](10) NULL,
	[IsDeleted] [bit] NULL,
	[Password] [nvarchar](max) NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FamilyInformation]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FamilyInformation](
	[MemberID] [varchar](10) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[PhoneNumber] [varchar](15) NULL,
	[Relationship] [varchar](1) NULL,
	[StudentID] [varchar](10) NULL,
	[EmailAddress] [nvarchar](255) NULL,
 CONSTRAINT [PK_FamilyInformation] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PermissionKeyList]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PermissionKeyList](
	[IDKey] [varchar](10) NOT NULL,
	[KeyDescription] [nvarchar](max) NULL,
 CONSTRAINT [PK__Permissi__939E78BB1FCDBCEB] PRIMARY KEY CLUSTERED 
(
	[IDKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role_Keys]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role_Keys](
	[IDRole] [varchar](10) NOT NULL,
	[IDKey] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IDRole] ASC,
	[IDKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScoreCorrectionDetails]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScoreCorrectionDetails](
	[ScoreCorrectionDetailID] [int] IDENTITY(1,1) NOT NULL,
	[ScoreCorrectionID] [int] NULL,
	[ScoreType] [int] NULL,
	[ScoreOld] [float] NULL,
	[ScoreNew] [float] NULL,
 CONSTRAINT [PK_ScoreCorrectionDetails] PRIMARY KEY CLUSTERED 
(
	[ScoreCorrectionDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScoreCorrections]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScoreCorrections](
	[ScoreCorrectionID] [int] IDENTITY(1,1) NOT NULL,
	[ScoreID] [int] NOT NULL,
	[UserCreateID] [varchar](10) NULL,
	[IsAccepted] [bit] NULL,
	[UserDecideID] [varchar](10) NULL,
	[InsertedDatetime] [datetime] NULL,
	[LastModified] [datetime] NULL,
 CONSTRAINT [PK_ScoreCorrection] PRIMARY KEY CLUSTERED 
(
	[ScoreCorrectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Scores]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Scores](
	[INDEXAUTO] [int] IDENTITY(1,1) NOT NULL,
	[StudentID] [varchar](10) NOT NULL,
	[Year] [varchar](4) NOT NULL,
	[Semester] [varchar](1) NOT NULL,
	[SubjectID] [varchar](10) NOT NULL,
	[Score1] [float] NULL,
	[Score2] [float] NULL,
	[Score3] [float] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Students]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Students](
	[StudentID] [varchar](10) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Gender] [nvarchar](1) NULL,
	[Birthday] [date] NULL,
	[Grade] [varchar](2) NULL,
	[YearOfAdmission] [varchar](4) NULL,
	[Address] [nvarchar](255) NULL,
	[District] [nvarchar](255) NULL,
	[CityorProvince] [nvarchar](255) NULL,
	[Country] [nvarchar](255) NULL,
	[PostalCode] [varchar](10) NULL,
	[PhoneNumber] [varchar](15) NULL,
	[EmailAddress] [nvarchar](255) NULL,
	[GraduationYear] [varchar](4) NULL,
	[HasClass] [bit] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subjects]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subjects](
	[SubjectID] [varchar](10) NOT NULL,
	[SubjectName] [nvarchar](255) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Subjects] PRIMARY KEY CLUSTERED 
(
	[SubjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SystemSettings]    Script Date: 31/05/2017 4:14:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemSettings](
	[SettingID] [varchar](10) NOT NULL,
	[MinimumAged] [int] NULL,
	[MaximumAged] [int] NULL,
	[MaxinumStudentNumber] [int] NULL,
	[StandardScore] [float] NULL,
 CONSTRAINT [PK_SystemSettings] PRIMARY KEY CLUSTERED 
(
	[SettingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C001', N'10A1', N'10', 0)
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C002', N'10A2', N'10', 0)
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C003', N'10A3', N'10', 0)
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C004', N'10A4', N'10', 0)
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C005', N'11A1', N'11', 0)
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C006', N'11A2', N'11', 0)
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C007', N'11A3', N'11', 0)
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C008', N'12A1', N'12', 0)
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C009', N'12A2', N'12', 0)
GO
INSERT [dbo].[Classes] ([ClassID], [ClassName], [Grade], [IsDeleted]) VALUES (N'C010  ', N'1', N'10', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0001', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0002', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0003', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0004', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0005', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0011', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0012', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0013', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0014', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0015', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0032', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0033', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0034', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0035', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0043', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0083', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0089', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0090', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0091', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0092', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0093', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0099', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0100', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0101', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0102', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0103', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0109', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C001', N'S0110', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0041', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0042', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0061', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0062', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0063', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0064', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0065', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0080', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0081', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C002', N'S0082', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0016', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0017', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0018', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0019', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0020', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0021', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0022', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0023', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0024', 1)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0046', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0047', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0048', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0049', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0051', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0052', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0053', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0054', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0060', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0066', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0067', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0068', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0069', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0071', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0072', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0073', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C005', N'S0074', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0025', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0026', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0027', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0028', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0029', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0030', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0055', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0056', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0057', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0058', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0059', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0070', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0075', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0076', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0077', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0078', 0)
GO
INSERT [dbo].[Classes_Students] ([Year], [ClassID], [StudentID], [IsDeleted]) VALUES (N'2017', N'C008', N'S0079', 0)
GO
INSERT [dbo].[EmloyeeRoles] ([RoleID], [RoleName]) VALUES (N'1', N'test')
GO
INSERT [dbo].[EmloyeeRoles] ([RoleID], [RoleName]) VALUES (N'E0019 ', N'admin')
GO
INSERT [dbo].[EmloyeeRoles] ([RoleID], [RoleName]) VALUES (N'E0020 ', N'Giáo viên')
GO
INSERT [dbo].[EmloyeeRoles] ([RoleID], [RoleName]) VALUES (N'E0021 ', N'Cán bộ phòng đào tạo')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E001', N'A', N'Tran Van', N'157', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245', N'phamhuutai95@gmail.com', N'E0020 ', 0, N'81dc9bdb52d04dc20036dbd8313ed055')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E002', N'B', N'Tran Van', N'213', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245', N'tvb@qlhs.cs', N'E0021 ', 0, N'81dc9bdb52d04dc20036dbd8313ed055')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E003', N'C', N'Tran Van', N'214', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245688', N'tvc@qlhs.cs', N'1', 0, N'81dc9bdb52d04dc20036dbd8313ed055')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E004  ', N'D', N'Tran Van', N'256', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245688', N'tvd@qlhs.cs', N'1', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E005  ', N'E', N'Tran Van', N'234', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245688', N'tve@qlhs.cs', N'1', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E006  ', N'F', N'Tran Van', N'123', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245688', N'tvf@qlhs.cs', N'1', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E007  ', N'G', N'Tran Van', N'456', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245688', N'tvg@qlhs.cs', N'1', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E010  ', N'H', N'Tran Van', N'567', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245688', N'tvh@qlhs.cs', N'1', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E011', N'J', N'Tran Van', N'12/34', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245688', N'tvj@qlhs.cs', N'1', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E012  ', N'K', N'Tran Van', N'22', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245688', N'tvk@qlhs.cs', N'1', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E013', N'Q', N'Tran Van', N'34', N'Nguyen dinh chieu', N'Ho Chi Minh', N'0913245688', N'tvq@qlhs.cs', N'1', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E014  ', N'W', N'Tran Van', N'123', N'Nguyen Hue', N'Ho Chi Minh', N'09123456772', N'tvw@qlhs.cs', N'1', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[Employees] ([EmployeeID], [FirstName], [LastName], [Address], [District], [CityorProvince], [PhoneNumber], [EmailAddress], [RoleID], [IsDeleted], [Password]) VALUES (N'E015  ', N'Admin', N'Administrator', N'', N'', N'', N'0000000', N'admin@qlhs.com', N'E0019 ', 0, N'827ccb0eea8a706c4c34a16891f84e7b')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0001', N'1423', N'', N'2', N'S0019', N'132@123.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0002', N'123', N'', N'2', N'S0020', N'123@123.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0003', N'123', N'', N'2', N'S0020', N'123@123.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0004', N'123', N'', N'3', N'S0021', N'123@123.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0005', N'3123', N'', N'3', N'S0021', N'123@123.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0006', N'132123', N'', N'2', N'S0027', N'123132@132.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0007', N' aaaa ', N' 125125251 ', N'1', N'S0028', N'  aaa@123.com ')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0008', N'142142', N'14214', N'3', N'S0029', N'123@123.COM')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0010', N'12341', N'3123123', N'1', N'S0031', N'132@132.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0011', N'Nguyễn Văn', N'0165668899', N'1', N'S0030', N'nam211@123.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0012', N'Nguyên Hải Nam 1', N'01699087127', N'2', N'S0032', N'namnguyen123@gmail.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0013', N'Nguyễn Hải Nam 2', N'01699087128', N'2', N'S0032', N'nhnam944@gmail.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0014', N' Nguyển Hải Nam ', N' 01699087127 ', N'1', N'S0110', N'  nhnam94@gmail.com ')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0015', N'Nguyễn Văn A', N'012233445566', N'2', N'S0103', N'vana@gmail.com')
GO
INSERT [dbo].[FamilyInformation] ([MemberID], [Name], [PhoneNumber], [Relationship], [StudentID], [EmailAddress]) VALUES (N'F0016', N'Nguyễn Văn A', N'01256897788', N'1', N'S0092', N'nvan@gmail.com')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_BCMH_D', N'Delete Báo Cáo tổng kết Môn học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_BCMH_I', N'Insert Báo Cáo tổng kết Môn học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_BCMH_U', N'Update Báo Cáo tổng kết Môn học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_BCMH_V', N'View Báo Cáo tổng kết Môn học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_BCTK_D', N'Delete Báo Cáo tổng kết')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_BCTK_I', N'Insert Báo Cáo tổng kết')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_BCTK_U', N'Update Báo Cáo tổng kết')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_BCTK_V', N'View Báo Cáo tổng kết')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_DSHS_D', N'Delete Danh sách học sinh')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_DSHS_I', N'Insert Danh sách học sinh')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_DSHS_U', N'Update Danh sách học sinh')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_DSHS_V', N'View Danh sách học sinh')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLCD_U', N'Quản lý cài đặt')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLCD_V', N'Xem Quản lý cài đặt')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLCH_D', N'Delete Quản lý cấu hình')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLCH_I', N'Insert Quản lý cấu hình')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLCH_U', N'Update Quản lý cấu hình')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLCH_V', N'View Quản lý cấu hình')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLD_D', N'Delete Quản lý điểm')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLD_I', N'Insert Quản lý điểm')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLD_U', N'Update Quản lý điểm')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLD_V', N'View Quản lý  điểm')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLHS_D', N'Delete QUản lý học sinh')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLHS_I', N'Insert Quản lý học sinh')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLHS_U', N'Update Quản lý học sinh')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLHS_V', N'View Quản lý học sinh')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLLH_D', N'Delete Quản lý lớp học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLLH_I', N'Insert Quản lý lớp học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLLH_U', N'Update Quản lý lớp học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLLH_V', N'View Quản lý lớp học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLMH_D', N'Delete Quản lý môn học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLMH_I', N'Insert Quản lý môn học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLMH_U', N'Update Quản lý môn học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLMH_V', N'View Quản lý môn học')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLND_D', N'Delete QUản lý người dùng')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLND_I', N'Insert Quản Lý người dùng')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLND_U', N'Update Quản Lý người dùng')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLND_V', N'Xem Quản lý người dùng')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLNND_D', N'Delete Quản lý Nhóm người dùng')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLNND_I', N'Insert Quản lý Nhóm người dùng')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLNND_U', N'Update Quản lý Nhóm người dùng')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLNND_V', N'View Quản lý Quản lý Nhóm người dùng')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLPD_D', N'Delete Quản lý Phiếu hiệu chỉnh điểm')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLPD_I', N'Insert Quản lý Phiếu hiệu chỉnh điểm')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLPD_U', N'Update Quản lý Phiếu hiệu chỉnh điểm')
GO
INSERT [dbo].[PermissionKeyList] ([IDKey], [KeyDescription]) VALUES (N'PK_QLPD_V', N'View Quản lý Phiếu hiệu chỉnh điểm')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_BCMH_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_BCMH_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_BCMH_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_BCMH_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_BCTK_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_BCTK_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_BCTK_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_BCTK_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_DSHS_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_DSHS_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_DSHS_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_DSHS_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLCD_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLCD_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLCH_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLCH_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLCH_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLCH_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLD_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLD_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLD_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLD_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLHS_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLHS_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLHS_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLHS_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLLH_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLLH_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLLH_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLLH_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLMH_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLMH_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLMH_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLMH_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLND_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLND_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLND_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLND_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLNND_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLNND_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLNND_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLNND_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLPD_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLPD_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLPD_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0019', N'PK_QLPD_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_BCMH_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_BCTK_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_DSHS_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_QLD_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_QLD_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_QLD_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_QLD_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_QLHS_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_QLLH_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_QLPD_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0020 ', N'PK_QLPD_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_BCMH_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_BCMH_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_BCMH_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_BCMH_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_BCTK_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_BCTK_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_BCTK_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_BCTK_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_DSHS_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_DSHS_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_DSHS_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_DSHS_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLD_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLHS_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLHS_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLHS_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLHS_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLLH_D')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLLH_I')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLLH_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLLH_V')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLPD_U')
GO
INSERT [dbo].[Role_Keys] ([IDRole], [IDKey]) VALUES (N'E0021 ', N'PK_QLPD_V')
GO
SET IDENTITY_INSERT [dbo].[ScoreCorrectionDetails] ON 

GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (7, 10, 0, 0, 9)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (8, 10, 1, 0, 1)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (9, 10, 2, 0, 0)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (10, 11, 1, 0, 10)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (11, 11, 2, 0, 5.75)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (12, 12, 0, 0, 10)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (13, 13, 0, 0, 10)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (1012, 1009, 0, 0, 1)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (1013, 1009, 1, 0, 2)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (1014, 1010, 0, 1, 0)
GO
INSERT [dbo].[ScoreCorrectionDetails] ([ScoreCorrectionDetailID], [ScoreCorrectionID], [ScoreType], [ScoreOld], [ScoreNew]) VALUES (1015, 1010, 1, 2, 1)
GO
SET IDENTITY_INSERT [dbo].[ScoreCorrectionDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[ScoreCorrections] ON 

GO
INSERT [dbo].[ScoreCorrections] ([ScoreCorrectionID], [ScoreID], [UserCreateID], [IsAccepted], [UserDecideID], [InsertedDatetime], [LastModified]) VALUES (10, 1466, N'E001', 0, N'E015', CAST(0x0000A78200A3BA93 AS DateTime), CAST(0x0000A78200A5CBA2 AS DateTime))
GO
INSERT [dbo].[ScoreCorrections] ([ScoreCorrectionID], [ScoreID], [UserCreateID], [IsAccepted], [UserDecideID], [InsertedDatetime], [LastModified]) VALUES (11, 1466, N'E001', 1, N'E015', CAST(0x0000A78200A44ADB AS DateTime), CAST(0x0000A78200A58DD3 AS DateTime))
GO
INSERT [dbo].[ScoreCorrections] ([ScoreCorrectionID], [ScoreID], [UserCreateID], [IsAccepted], [UserDecideID], [InsertedDatetime], [LastModified]) VALUES (12, 1465, N'E001', 1, N'E015', CAST(0x0000A78200A47533 AS DateTime), CAST(0x0000A78400F1D157 AS DateTime))
GO
INSERT [dbo].[ScoreCorrections] ([ScoreCorrectionID], [ScoreID], [UserCreateID], [IsAccepted], [UserDecideID], [InsertedDatetime], [LastModified]) VALUES (13, 1467, N'E015', 0, N'E015', CAST(0x0000A78200AC6E84 AS DateTime), CAST(0x0000A78400F1C4C2 AS DateTime))
GO
INSERT [dbo].[ScoreCorrections] ([ScoreCorrectionID], [ScoreID], [UserCreateID], [IsAccepted], [UserDecideID], [InsertedDatetime], [LastModified]) VALUES (1009, 208, N'E001', 1, N'E015', CAST(0x0000A784010381BF AS DateTime), CAST(0x0000A784010437BD AS DateTime))
GO
INSERT [dbo].[ScoreCorrections] ([ScoreCorrectionID], [ScoreID], [UserCreateID], [IsAccepted], [UserDecideID], [InsertedDatetime], [LastModified]) VALUES (1010, 208, N'E015', 0, N'E015', CAST(0x0000A78401046186 AS DateTime), CAST(0x0000A78401046C1D AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[ScoreCorrections] OFF
GO
SET IDENTITY_INSERT [dbo].[Scores] ON 

GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (64, N'S0002', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (65, N'S0002', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (66, N'S0002', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (67, N'S0002', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (68, N'S0002', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (69, N'S0002', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (70, N'S0002', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (71, N'S0002', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (72, N'S0002', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (73, N'S0002', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (74, N'S0002', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (75, N'S0002', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (76, N'S0002', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (77, N'S0002', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (78, N'S0002', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (79, N'S0002', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (80, N'S0002', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (81, N'S0002', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (82, N'S0003', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (83, N'S0003', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (84, N'S0003', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (85, N'S0003', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (86, N'S0003', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (87, N'S0003', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (88, N'S0003', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (89, N'S0003', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (90, N'S0003', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (91, N'S0003', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (92, N'S0003', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (93, N'S0003', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (94, N'S0003', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (95, N'S0003', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (96, N'S0003', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (97, N'S0003', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (98, N'S0003', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (99, N'S0003', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (100, N'S0004', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (101, N'S0004', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (102, N'S0004', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (103, N'S0004', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (104, N'S0004', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (105, N'S0004', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (106, N'S0004', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (107, N'S0004', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (108, N'S0004', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (109, N'S0004', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (110, N'S0004', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (111, N'S0004', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (112, N'S0004', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (113, N'S0004', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (114, N'S0004', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (115, N'S0004', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (116, N'S0004', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (117, N'S0004', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (118, N'S0005', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (119, N'S0005', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (120, N'S0005', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (121, N'S0005', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (122, N'S0005', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (123, N'S0005', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (124, N'S0005', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (125, N'S0005', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (126, N'S0005', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (127, N'S0005', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (128, N'S0005', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (129, N'S0005', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (130, N'S0005', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (131, N'S0005', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (132, N'S0005', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (133, N'S0005', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (134, N'S0005', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (135, N'S0005', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (136, N'S0011', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (137, N'S0011', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (138, N'S0011', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (139, N'S0011', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (140, N'S0011', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (141, N'S0011', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (142, N'S0011', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (143, N'S0011', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (144, N'S0011', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (145, N'S0011', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (146, N'S0011', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (147, N'S0011', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (148, N'S0011', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (149, N'S0011', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (150, N'S0011', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (151, N'S0011', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (152, N'S0011', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (153, N'S0011', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (154, N'S0013', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (155, N'S0013', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (156, N'S0013', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (157, N'S0013', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (158, N'S0013', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (159, N'S0013', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (160, N'S0013', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (161, N'S0013', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (162, N'S0013', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (163, N'S0013', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (164, N'S0013', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (165, N'S0013', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (166, N'S0013', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (167, N'S0013', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (168, N'S0013', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (169, N'S0013', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (170, N'S0013', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (171, N'S0013', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (172, N'S0014', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (173, N'S0014', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (174, N'S0014', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (175, N'S0014', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (176, N'S0014', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (177, N'S0014', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (178, N'S0014', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (179, N'S0014', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (180, N'S0014', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (181, N'S0014', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (182, N'S0014', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (183, N'S0014', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (184, N'S0014', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (185, N'S0014', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (186, N'S0014', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (187, N'S0014', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (188, N'S0014', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (189, N'S0014', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (190, N'S0015', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (191, N'S0015', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (192, N'S0015', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (193, N'S0015', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (194, N'S0015', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (195, N'S0015', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (196, N'S0015', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (197, N'S0015', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (198, N'S0015', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (199, N'S0015', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (200, N'S0015', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (201, N'S0015', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (202, N'S0015', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (203, N'S0015', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (204, N'S0015', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (205, N'S0015', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (206, N'S0015', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (207, N'S0015', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (208, N'S0032', N'2017', N'1', N'MH001', 1, 2, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (209, N'S0032', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (210, N'S0032', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (211, N'S0032', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (212, N'S0032', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (213, N'S0032', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (214, N'S0032', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (215, N'S0032', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (216, N'S0032', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (217, N'S0032', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (218, N'S0032', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (219, N'S0032', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (220, N'S0032', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (221, N'S0032', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (222, N'S0032', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (223, N'S0032', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (224, N'S0032', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (225, N'S0032', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (226, N'S0033', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (227, N'S0033', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (228, N'S0033', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (229, N'S0033', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (230, N'S0033', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (231, N'S0033', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (232, N'S0033', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (233, N'S0033', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (234, N'S0033', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (235, N'S0033', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (236, N'S0033', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (237, N'S0033', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (238, N'S0033', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (239, N'S0033', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (240, N'S0033', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (241, N'S0033', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (242, N'S0033', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (243, N'S0033', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (244, N'S0034', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (245, N'S0034', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (246, N'S0034', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (247, N'S0034', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (248, N'S0034', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (249, N'S0034', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (250, N'S0034', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (251, N'S0034', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (252, N'S0034', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (253, N'S0034', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (254, N'S0034', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (255, N'S0034', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (256, N'S0034', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (257, N'S0034', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (258, N'S0034', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (259, N'S0034', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (260, N'S0034', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (261, N'S0034', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (262, N'S0035', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (263, N'S0035', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (264, N'S0035', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (265, N'S0035', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (266, N'S0035', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (267, N'S0035', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (268, N'S0035', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (269, N'S0035', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (270, N'S0035', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (271, N'S0035', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (272, N'S0035', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (273, N'S0035', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (274, N'S0035', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (275, N'S0035', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (276, N'S0035', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (277, N'S0035', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (278, N'S0035', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (279, N'S0035', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (280, N'S0041', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (281, N'S0041', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (282, N'S0041', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (283, N'S0041', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (284, N'S0041', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (285, N'S0041', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (286, N'S0041', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (287, N'S0041', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (288, N'S0041', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (289, N'S0041', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (290, N'S0041', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (291, N'S0041', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (292, N'S0041', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (293, N'S0041', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (294, N'S0041', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (295, N'S0041', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (296, N'S0041', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (297, N'S0041', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (298, N'S0042', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (299, N'S0042', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (300, N'S0042', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (301, N'S0042', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (302, N'S0042', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (303, N'S0042', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (304, N'S0042', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (305, N'S0042', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (306, N'S0042', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (307, N'S0042', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (308, N'S0042', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (309, N'S0042', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (310, N'S0042', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (311, N'S0042', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (312, N'S0042', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (313, N'S0042', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (314, N'S0042', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (315, N'S0042', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (316, N'S0043', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (317, N'S0043', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (318, N'S0043', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (319, N'S0043', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (320, N'S0043', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (321, N'S0043', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (322, N'S0043', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (323, N'S0043', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (324, N'S0043', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (325, N'S0043', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (326, N'S0043', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (327, N'S0043', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (328, N'S0043', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (329, N'S0043', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (330, N'S0043', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (331, N'S0043', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (332, N'S0043', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (333, N'S0043', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (334, N'S0093', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (335, N'S0093', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (336, N'S0093', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (337, N'S0093', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (338, N'S0093', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (339, N'S0093', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (340, N'S0093', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (341, N'S0093', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (342, N'S0093', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (343, N'S0093', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (344, N'S0093', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (345, N'S0093', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (346, N'S0093', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (347, N'S0093', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (348, N'S0093', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (349, N'S0093', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (350, N'S0093', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (351, N'S0093', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (352, N'S0102', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (353, N'S0102', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (354, N'S0102', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (355, N'S0102', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (356, N'S0102', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (357, N'S0102', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (358, N'S0102', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (359, N'S0102', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (360, N'S0102', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (361, N'S0102', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (362, N'S0102', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (363, N'S0102', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (364, N'S0102', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (365, N'S0102', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (366, N'S0102', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (367, N'S0102', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (368, N'S0102', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (369, N'S0102', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (370, N'S0061', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (371, N'S0061', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (372, N'S0061', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (373, N'S0061', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (374, N'S0061', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (375, N'S0061', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (376, N'S0061', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (377, N'S0061', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (378, N'S0061', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (379, N'S0061', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (380, N'S0061', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (381, N'S0061', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (382, N'S0061', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (383, N'S0061', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (384, N'S0061', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (385, N'S0061', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (386, N'S0061', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (387, N'S0061', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (388, N'S0062', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (389, N'S0062', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (390, N'S0062', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (391, N'S0062', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (392, N'S0062', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (393, N'S0062', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (394, N'S0062', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (395, N'S0062', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (396, N'S0062', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (397, N'S0062', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (398, N'S0062', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (399, N'S0062', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (400, N'S0062', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (401, N'S0062', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (402, N'S0062', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (403, N'S0062', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (404, N'S0062', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (405, N'S0062', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (406, N'S0063', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (407, N'S0063', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (408, N'S0063', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (409, N'S0063', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (410, N'S0063', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (411, N'S0063', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (412, N'S0063', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (413, N'S0063', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (414, N'S0063', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (415, N'S0063', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (416, N'S0063', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (417, N'S0063', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (418, N'S0063', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (419, N'S0063', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (420, N'S0063', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (421, N'S0063', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (422, N'S0063', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (423, N'S0063', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (424, N'S0016', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (425, N'S0016', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (426, N'S0016', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (427, N'S0016', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (428, N'S0016', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (429, N'S0016', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (430, N'S0016', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (431, N'S0016', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (432, N'S0016', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (433, N'S0016', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (434, N'S0016', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (435, N'S0016', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (436, N'S0016', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (437, N'S0016', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (438, N'S0016', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (439, N'S0016', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (440, N'S0016', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (441, N'S0016', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (442, N'S0017', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (443, N'S0017', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (444, N'S0017', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (445, N'S0017', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (446, N'S0017', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (447, N'S0017', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (448, N'S0017', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (449, N'S0017', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (450, N'S0017', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (451, N'S0017', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (452, N'S0017', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (453, N'S0017', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (454, N'S0017', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (455, N'S0017', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (456, N'S0017', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (457, N'S0017', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (458, N'S0017', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (459, N'S0017', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (460, N'S0018', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (461, N'S0018', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (462, N'S0018', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (463, N'S0018', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (464, N'S0018', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (465, N'S0018', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (466, N'S0018', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (467, N'S0018', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (468, N'S0018', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (469, N'S0018', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (470, N'S0018', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (471, N'S0018', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (472, N'S0018', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (473, N'S0018', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (474, N'S0018', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (475, N'S0018', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (476, N'S0018', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (477, N'S0018', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (478, N'S0019', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (479, N'S0019', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (480, N'S0019', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (481, N'S0019', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (482, N'S0019', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (483, N'S0019', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (484, N'S0019', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (485, N'S0019', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (486, N'S0019', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (487, N'S0019', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (488, N'S0019', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (489, N'S0019', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (490, N'S0019', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (491, N'S0019', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (492, N'S0019', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (493, N'S0019', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (494, N'S0019', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (495, N'S0019', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (496, N'S0020', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (497, N'S0020', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (498, N'S0020', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (499, N'S0020', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (500, N'S0020', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (501, N'S0020', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (502, N'S0020', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (503, N'S0020', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (504, N'S0020', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (505, N'S0020', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (506, N'S0020', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (507, N'S0020', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (508, N'S0020', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (509, N'S0020', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (510, N'S0020', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (511, N'S0020', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (512, N'S0020', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (513, N'S0020', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (514, N'S0021', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (515, N'S0021', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (516, N'S0021', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (517, N'S0021', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (518, N'S0021', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (519, N'S0021', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (520, N'S0021', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (521, N'S0021', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (522, N'S0021', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (523, N'S0021', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (524, N'S0021', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (525, N'S0021', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (526, N'S0021', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (527, N'S0021', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (528, N'S0021', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (529, N'S0021', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (530, N'S0021', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (531, N'S0021', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (532, N'S0022', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (533, N'S0022', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (534, N'S0022', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (535, N'S0022', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (536, N'S0022', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (537, N'S0022', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (538, N'S0022', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (539, N'S0022', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (540, N'S0022', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (541, N'S0022', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (542, N'S0022', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (543, N'S0022', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (544, N'S0022', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (545, N'S0022', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (546, N'S0022', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (547, N'S0022', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (548, N'S0022', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (549, N'S0022', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (550, N'S0023', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (551, N'S0023', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (552, N'S0023', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (553, N'S0023', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (554, N'S0023', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (555, N'S0023', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (556, N'S0023', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (557, N'S0023', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (558, N'S0023', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (559, N'S0023', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (560, N'S0023', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (561, N'S0023', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (562, N'S0023', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (563, N'S0023', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (564, N'S0023', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (565, N'S0023', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (566, N'S0023', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (567, N'S0023', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (568, N'S0024', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (569, N'S0024', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (570, N'S0024', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (571, N'S0024', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (572, N'S0024', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (573, N'S0024', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (574, N'S0024', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (575, N'S0024', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (576, N'S0024', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (577, N'S0024', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (578, N'S0024', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (579, N'S0024', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (580, N'S0024', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (581, N'S0024', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (582, N'S0024', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (583, N'S0024', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (584, N'S0024', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (585, N'S0024', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (586, N'S0046', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (587, N'S0046', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (588, N'S0046', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (589, N'S0046', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (590, N'S0046', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (591, N'S0046', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (592, N'S0046', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (593, N'S0046', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (594, N'S0046', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (595, N'S0046', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (596, N'S0046', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (597, N'S0046', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (598, N'S0046', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (599, N'S0046', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (600, N'S0046', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (601, N'S0046', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (602, N'S0046', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (603, N'S0046', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (604, N'S0047', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (605, N'S0047', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (606, N'S0047', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (607, N'S0047', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (608, N'S0047', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (609, N'S0047', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (610, N'S0047', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (611, N'S0047', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (612, N'S0047', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (613, N'S0047', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (614, N'S0047', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (615, N'S0047', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (616, N'S0047', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (617, N'S0047', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (618, N'S0047', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (619, N'S0047', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (620, N'S0047', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (621, N'S0047', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (622, N'S0051', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (623, N'S0051', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (624, N'S0051', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (625, N'S0051', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (626, N'S0051', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (627, N'S0051', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (628, N'S0051', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (629, N'S0051', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (630, N'S0051', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (631, N'S0051', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (632, N'S0051', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (633, N'S0051', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (634, N'S0051', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (635, N'S0051', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (636, N'S0051', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (637, N'S0051', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (638, N'S0051', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (639, N'S0051', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (640, N'S0052', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (641, N'S0052', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (642, N'S0052', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (643, N'S0052', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (644, N'S0052', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (645, N'S0052', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (646, N'S0052', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (647, N'S0052', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (648, N'S0052', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (649, N'S0052', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (650, N'S0052', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (651, N'S0052', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (652, N'S0052', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (653, N'S0052', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (654, N'S0052', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (655, N'S0052', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (656, N'S0052', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (657, N'S0052', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (658, N'S0067', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (659, N'S0067', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (660, N'S0067', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (661, N'S0067', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (662, N'S0067', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (663, N'S0067', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (664, N'S0067', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (665, N'S0067', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (666, N'S0067', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (667, N'S0067', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (668, N'S0067', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (669, N'S0067', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (670, N'S0067', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (671, N'S0067', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (672, N'S0067', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (673, N'S0067', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (674, N'S0067', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (675, N'S0067', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (676, N'S0068', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (677, N'S0068', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (678, N'S0068', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (679, N'S0068', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (680, N'S0068', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (681, N'S0068', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (682, N'S0068', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (683, N'S0068', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (684, N'S0068', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (685, N'S0068', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (686, N'S0068', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (687, N'S0068', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (688, N'S0068', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (689, N'S0068', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (690, N'S0068', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (691, N'S0068', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (692, N'S0068', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (693, N'S0068', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (694, N'S0069', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (695, N'S0069', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (696, N'S0069', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (697, N'S0069', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (698, N'S0069', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (699, N'S0069', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (700, N'S0069', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (701, N'S0069', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (702, N'S0069', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (703, N'S0069', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (704, N'S0069', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (705, N'S0069', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (706, N'S0069', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (707, N'S0069', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (708, N'S0069', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (709, N'S0069', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (710, N'S0069', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (711, N'S0069', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (712, N'S0071', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (713, N'S0071', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (714, N'S0071', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (715, N'S0071', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (716, N'S0071', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (717, N'S0071', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (718, N'S0071', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (719, N'S0071', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (720, N'S0071', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (721, N'S0071', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (722, N'S0071', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (723, N'S0071', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (724, N'S0071', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (725, N'S0071', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (726, N'S0071', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (727, N'S0071', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (728, N'S0071', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (729, N'S0071', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (730, N'S0072', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (731, N'S0072', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (732, N'S0072', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (733, N'S0072', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (734, N'S0072', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (735, N'S0072', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (736, N'S0072', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (737, N'S0072', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (738, N'S0072', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (739, N'S0072', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (740, N'S0072', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (741, N'S0072', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (742, N'S0072', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (743, N'S0072', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (744, N'S0072', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (745, N'S0072', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (746, N'S0072', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (747, N'S0072', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (748, N'S0073', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (749, N'S0073', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (750, N'S0073', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (751, N'S0073', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (752, N'S0073', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (753, N'S0073', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (754, N'S0073', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (755, N'S0073', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (756, N'S0073', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (757, N'S0073', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (758, N'S0073', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (759, N'S0073', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (760, N'S0073', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (761, N'S0073', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (762, N'S0073', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (763, N'S0073', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (764, N'S0073', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (765, N'S0073', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (766, N'S0074', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (767, N'S0074', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (768, N'S0074', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (769, N'S0074', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (770, N'S0074', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (771, N'S0074', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (772, N'S0074', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (773, N'S0074', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (774, N'S0074', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (775, N'S0074', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (776, N'S0074', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (777, N'S0074', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (778, N'S0074', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (779, N'S0074', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (780, N'S0074', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (781, N'S0074', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (782, N'S0074', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (783, N'S0074', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (784, N'S0064', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (785, N'S0064', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (786, N'S0064', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (787, N'S0064', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (788, N'S0064', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (789, N'S0064', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (790, N'S0064', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (791, N'S0064', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (792, N'S0064', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (793, N'S0064', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (794, N'S0064', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (795, N'S0064', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (796, N'S0064', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (797, N'S0064', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (798, N'S0064', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (799, N'S0064', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (800, N'S0064', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (801, N'S0064', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (802, N'S0065', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (803, N'S0065', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (804, N'S0065', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (805, N'S0065', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (806, N'S0065', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (807, N'S0065', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (808, N'S0065', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (809, N'S0065', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (810, N'S0065', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (811, N'S0065', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (812, N'S0065', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (813, N'S0065', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (814, N'S0065', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (815, N'S0065', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (816, N'S0065', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (817, N'S0065', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (818, N'S0065', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (819, N'S0065', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (820, N'S0081', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (821, N'S0081', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (822, N'S0081', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (823, N'S0081', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (824, N'S0081', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (825, N'S0081', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (826, N'S0081', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (827, N'S0081', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (828, N'S0081', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (829, N'S0081', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (830, N'S0081', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (831, N'S0081', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (832, N'S0081', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (833, N'S0081', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (834, N'S0081', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (835, N'S0081', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (836, N'S0081', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (837, N'S0081', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (838, N'S0080', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (839, N'S0080', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (840, N'S0080', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (841, N'S0080', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (842, N'S0080', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (843, N'S0080', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (844, N'S0080', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (845, N'S0080', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (846, N'S0080', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (847, N'S0080', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (848, N'S0080', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (849, N'S0080', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (850, N'S0080', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (851, N'S0080', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (852, N'S0080', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (853, N'S0080', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (854, N'S0080', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (855, N'S0080', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (856, N'S0082', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (857, N'S0082', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (858, N'S0082', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (859, N'S0082', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (860, N'S0082', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (861, N'S0082', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (862, N'S0082', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (863, N'S0082', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (864, N'S0082', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (865, N'S0082', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (866, N'S0082', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (867, N'S0082', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (868, N'S0082', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (869, N'S0082', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (870, N'S0082', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (871, N'S0082', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (872, N'S0082', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (873, N'S0082', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (874, N'S0083', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (875, N'S0083', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (876, N'S0083', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (877, N'S0083', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (878, N'S0083', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (879, N'S0083', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (880, N'S0083', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (881, N'S0083', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (882, N'S0083', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (883, N'S0083', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (884, N'S0083', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (885, N'S0083', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (886, N'S0083', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (887, N'S0083', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (888, N'S0083', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (889, N'S0083', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (890, N'S0083', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (891, N'S0083', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (892, N'S0091', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (893, N'S0091', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (894, N'S0091', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (895, N'S0091', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (896, N'S0091', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (897, N'S0091', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (898, N'S0091', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (899, N'S0091', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (900, N'S0091', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (901, N'S0091', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (902, N'S0091', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (903, N'S0091', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (904, N'S0091', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (905, N'S0091', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (906, N'S0091', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (907, N'S0091', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (908, N'S0091', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (909, N'S0091', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (910, N'S0092', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (911, N'S0092', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (912, N'S0092', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (913, N'S0092', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (914, N'S0092', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (915, N'S0092', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (916, N'S0092', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (917, N'S0092', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (918, N'S0092', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (919, N'S0092', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (920, N'S0092', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (921, N'S0092', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (922, N'S0092', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (923, N'S0092', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (924, N'S0092', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (925, N'S0092', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (926, N'S0092', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (927, N'S0092', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (928, N'S0099', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (929, N'S0099', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (930, N'S0099', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (931, N'S0099', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (932, N'S0099', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (933, N'S0099', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (934, N'S0099', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (935, N'S0099', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (936, N'S0099', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (937, N'S0099', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (938, N'S0099', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (939, N'S0099', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (940, N'S0099', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (941, N'S0099', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (942, N'S0099', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (943, N'S0099', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (944, N'S0099', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (945, N'S0099', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (946, N'S0100', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (947, N'S0100', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (948, N'S0100', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (949, N'S0100', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (950, N'S0100', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (951, N'S0100', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (952, N'S0100', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (953, N'S0100', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (954, N'S0100', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (955, N'S0100', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (956, N'S0100', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (957, N'S0100', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (958, N'S0100', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (959, N'S0100', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (960, N'S0100', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (961, N'S0100', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (962, N'S0100', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (963, N'S0100', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (964, N'S0101', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (965, N'S0101', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (966, N'S0101', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (967, N'S0101', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (968, N'S0101', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (969, N'S0101', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (970, N'S0101', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (971, N'S0101', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (972, N'S0101', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (973, N'S0101', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (974, N'S0101', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (975, N'S0101', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (976, N'S0101', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (977, N'S0101', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (978, N'S0101', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (979, N'S0101', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (980, N'S0101', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (981, N'S0101', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (982, N'S0103', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (983, N'S0103', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (984, N'S0103', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (985, N'S0103', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (986, N'S0103', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (987, N'S0103', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (988, N'S0103', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (989, N'S0103', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (990, N'S0103', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (991, N'S0103', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (992, N'S0103', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (993, N'S0103', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (994, N'S0103', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (995, N'S0103', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (996, N'S0103', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (997, N'S0103', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (998, N'S0103', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (999, N'S0103', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1000, N'S0109', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1001, N'S0109', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1002, N'S0109', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1003, N'S0109', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1004, N'S0109', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1005, N'S0109', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1006, N'S0109', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1007, N'S0109', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1008, N'S0109', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1009, N'S0109', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1010, N'S0109', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1011, N'S0109', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1012, N'S0109', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1013, N'S0109', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1014, N'S0109', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1015, N'S0109', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1016, N'S0109', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1017, N'S0109', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1018, N'S0089', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1019, N'S0089', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1020, N'S0089', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1021, N'S0089', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1022, N'S0089', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1023, N'S0089', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1024, N'S0089', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1025, N'S0089', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1026, N'S0089', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1027, N'S0089', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1028, N'S0089', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1029, N'S0089', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1030, N'S0089', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1031, N'S0089', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1032, N'S0089', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1033, N'S0089', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1034, N'S0089', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1035, N'S0089', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1036, N'S0090', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1037, N'S0090', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1038, N'S0090', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1039, N'S0090', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1040, N'S0090', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1041, N'S0090', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1042, N'S0090', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1043, N'S0090', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1044, N'S0090', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1045, N'S0090', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1046, N'S0090', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1047, N'S0090', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1048, N'S0090', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1049, N'S0090', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1050, N'S0090', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1051, N'S0090', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1052, N'S0090', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1053, N'S0090', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1054, N'S0048', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1055, N'S0048', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1056, N'S0048', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1057, N'S0048', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1058, N'S0048', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1059, N'S0048', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1060, N'S0048', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1061, N'S0048', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1062, N'S0048', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1063, N'S0048', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1064, N'S0048', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1065, N'S0048', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1066, N'S0048', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1067, N'S0048', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1068, N'S0048', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1069, N'S0048', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1070, N'S0048', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1071, N'S0048', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1072, N'S0049', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1073, N'S0049', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1074, N'S0049', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1075, N'S0049', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1076, N'S0049', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1077, N'S0049', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1078, N'S0049', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1079, N'S0049', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1080, N'S0049', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1081, N'S0049', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1082, N'S0049', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1083, N'S0049', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1084, N'S0049', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1085, N'S0049', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1086, N'S0049', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1087, N'S0049', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1088, N'S0049', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1089, N'S0049', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1090, N'S0053', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1091, N'S0053', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1092, N'S0053', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1093, N'S0053', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1094, N'S0053', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1095, N'S0053', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1096, N'S0053', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1097, N'S0053', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1098, N'S0053', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1099, N'S0053', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1100, N'S0053', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1101, N'S0053', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1102, N'S0053', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1103, N'S0053', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1104, N'S0053', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1105, N'S0053', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1106, N'S0053', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1107, N'S0053', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1108, N'S0054', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1109, N'S0054', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1110, N'S0054', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1111, N'S0054', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1112, N'S0054', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1113, N'S0054', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1114, N'S0054', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1115, N'S0054', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1116, N'S0054', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1117, N'S0054', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1118, N'S0054', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1119, N'S0054', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1120, N'S0054', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1121, N'S0054', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1122, N'S0054', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1123, N'S0054', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1124, N'S0054', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1125, N'S0054', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1126, N'S0060', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1127, N'S0060', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1128, N'S0060', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1129, N'S0060', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1130, N'S0060', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1131, N'S0060', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1132, N'S0060', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1133, N'S0060', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1134, N'S0060', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1135, N'S0060', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1136, N'S0060', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1137, N'S0060', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1138, N'S0060', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1139, N'S0060', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1140, N'S0060', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1141, N'S0060', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1142, N'S0060', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1143, N'S0060', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1144, N'S0066', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1145, N'S0066', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1146, N'S0066', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1147, N'S0066', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1148, N'S0066', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1149, N'S0066', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1150, N'S0066', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1151, N'S0066', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1152, N'S0066', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1153, N'S0066', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1154, N'S0066', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1155, N'S0066', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1156, N'S0066', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1157, N'S0066', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1158, N'S0066', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1159, N'S0066', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1160, N'S0066', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1161, N'S0066', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1162, N'S0025', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1163, N'S0025', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1164, N'S0025', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1165, N'S0025', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1166, N'S0025', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1167, N'S0025', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1168, N'S0025', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1169, N'S0025', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1170, N'S0025', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1171, N'S0025', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1172, N'S0025', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1173, N'S0025', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1174, N'S0025', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1175, N'S0025', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1176, N'S0025', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1177, N'S0025', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1178, N'S0025', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1179, N'S0025', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1180, N'S0026', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1181, N'S0026', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1182, N'S0026', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1183, N'S0026', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1184, N'S0026', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1185, N'S0026', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1186, N'S0026', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1187, N'S0026', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1188, N'S0026', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1189, N'S0026', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1190, N'S0026', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1191, N'S0026', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1192, N'S0026', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1193, N'S0026', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1194, N'S0026', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1195, N'S0026', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1196, N'S0026', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1197, N'S0026', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1198, N'S0027', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1199, N'S0027', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1200, N'S0027', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1201, N'S0027', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1202, N'S0027', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1203, N'S0027', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1204, N'S0027', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1205, N'S0027', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1206, N'S0027', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1207, N'S0027', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1208, N'S0027', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1209, N'S0027', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1210, N'S0027', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1211, N'S0027', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1212, N'S0027', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1213, N'S0027', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1214, N'S0027', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1215, N'S0027', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1216, N'S0028', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1217, N'S0028', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1218, N'S0028', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1219, N'S0028', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1220, N'S0028', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1221, N'S0028', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1222, N'S0028', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1223, N'S0028', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1224, N'S0028', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1225, N'S0028', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1226, N'S0028', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1227, N'S0028', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1228, N'S0028', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1229, N'S0028', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1230, N'S0028', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1231, N'S0028', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1232, N'S0028', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1233, N'S0028', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1234, N'S0029', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1235, N'S0029', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1236, N'S0029', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1237, N'S0029', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1238, N'S0029', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1239, N'S0029', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1240, N'S0029', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1241, N'S0029', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1242, N'S0029', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1243, N'S0029', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1244, N'S0029', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1245, N'S0029', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1246, N'S0029', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1247, N'S0029', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1248, N'S0029', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1249, N'S0029', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1250, N'S0029', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1251, N'S0029', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1252, N'S0030', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1253, N'S0030', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1254, N'S0030', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1255, N'S0030', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1256, N'S0030', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1257, N'S0030', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1258, N'S0030', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1259, N'S0030', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1260, N'S0030', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1261, N'S0030', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1262, N'S0030', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1263, N'S0030', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1264, N'S0030', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1265, N'S0030', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1266, N'S0030', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1267, N'S0030', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1268, N'S0030', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1269, N'S0030', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1270, N'S0055', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1271, N'S0055', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1272, N'S0055', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1273, N'S0055', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1274, N'S0055', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1275, N'S0055', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1276, N'S0055', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1277, N'S0055', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1278, N'S0055', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1279, N'S0055', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1280, N'S0055', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1281, N'S0055', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1282, N'S0055', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1283, N'S0055', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1284, N'S0055', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1285, N'S0055', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1286, N'S0055', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1287, N'S0055', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1288, N'S0056', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1289, N'S0056', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1290, N'S0056', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1291, N'S0056', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1292, N'S0056', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1293, N'S0056', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1294, N'S0056', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1295, N'S0056', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1296, N'S0056', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1297, N'S0056', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1298, N'S0056', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1299, N'S0056', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1300, N'S0056', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1301, N'S0056', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1302, N'S0056', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1303, N'S0056', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1304, N'S0056', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1305, N'S0056', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1306, N'S0057', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1307, N'S0057', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1308, N'S0057', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1309, N'S0057', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1310, N'S0057', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1311, N'S0057', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1312, N'S0057', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1313, N'S0057', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1314, N'S0057', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1315, N'S0057', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1316, N'S0057', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1317, N'S0057', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1318, N'S0057', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1319, N'S0057', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1320, N'S0057', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1321, N'S0057', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1322, N'S0057', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1323, N'S0057', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1324, N'S0058', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1325, N'S0058', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1326, N'S0058', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1327, N'S0058', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1328, N'S0058', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1329, N'S0058', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1330, N'S0058', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1331, N'S0058', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1332, N'S0058', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1333, N'S0058', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1334, N'S0058', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1335, N'S0058', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1336, N'S0058', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1337, N'S0058', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1338, N'S0058', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1339, N'S0058', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1340, N'S0058', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1341, N'S0058', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1342, N'S0059', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1343, N'S0059', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1344, N'S0059', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1345, N'S0059', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1346, N'S0059', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1347, N'S0059', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1348, N'S0059', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1349, N'S0059', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1350, N'S0059', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1351, N'S0059', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1352, N'S0059', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1353, N'S0059', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1354, N'S0059', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1355, N'S0059', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1356, N'S0059', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1357, N'S0059', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1358, N'S0059', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1359, N'S0059', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1360, N'S0070', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1361, N'S0070', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1362, N'S0070', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1363, N'S0070', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1364, N'S0070', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1365, N'S0070', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1366, N'S0070', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1367, N'S0070', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1368, N'S0070', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1369, N'S0070', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1370, N'S0070', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1371, N'S0070', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1372, N'S0070', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1373, N'S0070', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1374, N'S0070', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1375, N'S0070', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1376, N'S0070', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1377, N'S0070', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1378, N'S0075', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1379, N'S0075', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1380, N'S0075', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1381, N'S0075', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1382, N'S0075', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1383, N'S0075', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1384, N'S0075', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1385, N'S0075', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1386, N'S0075', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1387, N'S0075', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1388, N'S0075', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1389, N'S0075', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1390, N'S0075', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1391, N'S0075', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1392, N'S0075', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1393, N'S0075', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1394, N'S0075', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1395, N'S0075', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1396, N'S0076', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1397, N'S0076', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1398, N'S0076', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1399, N'S0076', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1400, N'S0076', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1401, N'S0076', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1402, N'S0076', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1403, N'S0076', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1404, N'S0076', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1405, N'S0076', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1406, N'S0076', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1407, N'S0076', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1408, N'S0076', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1409, N'S0076', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1410, N'S0076', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1411, N'S0076', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1412, N'S0076', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1413, N'S0076', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1414, N'S0077', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1415, N'S0077', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1416, N'S0077', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1417, N'S0077', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1418, N'S0077', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1419, N'S0077', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1420, N'S0077', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1421, N'S0077', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1422, N'S0077', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1423, N'S0077', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1424, N'S0077', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1425, N'S0077', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1426, N'S0077', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1427, N'S0077', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1428, N'S0077', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1429, N'S0077', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1430, N'S0077', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1431, N'S0077', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1432, N'S0078', N'2017', N'1', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1433, N'S0078', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1434, N'S0078', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1435, N'S0078', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1436, N'S0078', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1437, N'S0078', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1438, N'S0078', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1439, N'S0078', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1440, N'S0078', N'2017', N'1', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1441, N'S0078', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1442, N'S0078', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1443, N'S0078', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1444, N'S0078', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1445, N'S0078', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1446, N'S0078', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1447, N'S0078', N'2017', N'2', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1448, N'S0078', N'2017', N'1', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1449, N'S0078', N'2017', N'2', N'MH009', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1450, N'S0079', N'2017', N'1', N'MH001', 1, 1, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1451, N'S0079', N'2017', N'2', N'MH001', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1452, N'S0079', N'2017', N'1', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1453, N'S0079', N'2017', N'2', N'MH002', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1454, N'S0079', N'2017', N'1', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1455, N'S0079', N'2017', N'2', N'MH003', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1456, N'S0079', N'2017', N'1', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1457, N'S0079', N'2017', N'2', N'MH004', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1458, N'S0079', N'2017', N'1', N'MH005', 1, 1, 1)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1459, N'S0079', N'2017', N'2', N'MH005', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1460, N'S0079', N'2017', N'1', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1461, N'S0079', N'2017', N'2', N'MH006', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1462, N'S0079', N'2017', N'1', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1463, N'S0079', N'2017', N'2', N'MH007', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1464, N'S0079', N'2017', N'1', N'MH008', 0, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1465, N'S0079', N'2017', N'2', N'MH008', 10, 0, 0)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1466, N'S0079', N'2017', N'1', N'MH009', 0, 10, 5.75)
GO
INSERT [dbo].[Scores] ([INDEXAUTO], [StudentID], [Year], [Semester], [SubjectID], [Score1], [Score2], [Score3]) VALUES (1467, N'S0079', N'2017', N'2', N'MH009', 0, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[Scores] OFF
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0001', N'Hải Nam', N'Nguyễn ', N'0', CAST(0xDA1C0B00 AS Date), N'10', N'2017', N'52/17 77th Street Tan Quy Ward', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'nhnam94@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0002', N'Văn Tèo', N'Nguyễn ', N'1', CAST(0x2A190B00 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0003', N'Văn Tí', N'Lê ', N'0', CAST(0x24240B00 AS Date), N'10', N'2017', N'222 Hạ Long', N'Hải Châu', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0004', N'Hải Châu', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0005', N'Văn Tèo', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0006', N'Lê ', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0007', N'Hải Châu', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0008', N'VietNam', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0009', N'Hải Châu', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0010', N'Hải Châu', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0011', N'Văn Tèo', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0012', N'Huy', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'4442 Nguyễn Văn Linh', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0013', N'Hải Châu', N'Phạm', N'0', CAST(0xC23C0B00 AS Date), N'10', N'2017', N'132 Hồng Bàng', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'ti@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0014', N'Văn Tèo', N'Phạm', N'1', CAST(0xCD3C0B00 AS Date), N'10', N'2017', N'123 Điện Biên Phủ', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'ti@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0015', N'Hoàng', N'Phạm', N'0', CAST(0xCA3C0B00 AS Date), N'10', N'2017', N'222 Hạ Long', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'ti@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0016', N'Văn Tèo', N'Phạm', N'1', CAST(0xCA3C0B00 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0017', N'Văn Tèo', N'Phạm', N'0', CAST(0xC23C0B00 AS Date), N'11', N'2017', N'252 Điện Biên Phủ', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0018', N'Văn Tèo', N'Phạm', N'1', CAST(0xC33C0B00 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0019', N'Hải Châu', N'Hoàng', N'0', CAST(0xCA3C0B00 AS Date), N'11', N'2017', N'252 Điện Biên Phủ', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0020', N'Nguyễn ', N'Hoàng', N'0', CAST(0xD23C0B00 AS Date), N'11', N'2017', N'4442 Nguyễn Văn Linh', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0021 ', N'Nguyễn ', N'Hoàng', N'1', CAST(0xCA3C0B00 AS Date), N'11', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0022 ', N'Hải Long', N'Hoàng', N'1', CAST(0x00000000 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0023 ', N'Hải Long', N'Hoàng', N'0', CAST(0x00000000 AS Date), N'11', N'2017', N'252 Điện Biên Phủ', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0024 ', N'Yến', N'Nguyễn', N'1', CAST(0x00000000 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0025 ', N'Lộc', N'Hoàng', N'1', CAST(0x1D230B00 AS Date), N'12', N'2017', N'124 Hải Châu', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0026 ', N'Hải Châu', N'Phan', N'0', CAST(0xB4210B00 AS Date), N'12', N'2017', N'132132 Hải Châu', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0027 ', N'Hải Long', N'Phan', N'0', CAST(0x83240B00 AS Date), N'12', N'2017', N'252 Điện Biên Phủ', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0028 ', N'Hải Long', N'Phan', N'1', CAST(0x852E0B00 AS Date), N'12', N'2017', N'52/17 77th Street', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0029 ', N'Nam', N'Phan', N'0', CAST(0x47200B00 AS Date), N'12', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0030 ', N'Hải Nam', N'Phan', N'0', CAST(0x39200B00 AS Date), N'12', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'namnguyen@132.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0031 ', N'Hương', N'Phan', N'1', CAST(0x17230B00 AS Date), N'12', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'namnguyen@132.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0032', N'Hải Nam', N'Nguyễn', N'1', CAST(0x4B240B00 AS Date), N'10', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận 7', N'TP Hồ Chí Minh', N'Việt Nam', N'790000', N'01215530663', N'nhnam94@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0033', N'Văn Tí', N'Lê ', N'0', CAST(0x24240B00 AS Date), N'10', N'2017', N'222 Hạ Long', N'Hải Châu', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0034', N'Hải Châu', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0035', N'Văn Tèo', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0036', N'Lê ', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0037', N'Hải Châu', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0038', N'VietNam', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0039', N'VietNam', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0040', N'VietNam', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0041', N'VietNam', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0042', N'Huy', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'4442 Nguyễn Văn Linh', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0043', N'Hải Châu', N'Phạm', N'0', CAST(0xC23C0B00 AS Date), N'10', N'2017', N'132 Hồng Bàng', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'ti@gmail.com', N'', 1, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0044', N'Văn Tèo', N'Phạm', N'1', CAST(0xCD3C0B00 AS Date), N'10', N'2017', N'123 Điện Biên Phủ', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'ti@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0045', N'Hoàng', N'Phạm', N'0', CAST(0xCA3C0B00 AS Date), N'10', N'2017', N'222 Hạ Long', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'ti@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0046', N'Việt Phong', N'Phạm', N'1', CAST(0xCA3C0B00 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0047', N'Văn Tèo', N'Phạm', N'0', CAST(0xC23C0B00 AS Date), N'11', N'2017', N'252 Điện Biên Phủ', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0048', N'Việt Phong', N'Phạm', N'1', CAST(0xC33C0B00 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0049', N'Hải Châu', N'Hoàng', N'0', CAST(0xCA3C0B00 AS Date), N'11', N'2017', N'252 Điện Biên Phủ', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0050', N'Nguyễn ', N'Hoàng', N'0', CAST(0xD23C0B00 AS Date), N'11', N'2017', N'4442 Nguyễn Văn Linh', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0051 ', N'Nguyễn ', N'Hoàng', N'1', CAST(0xCA3C0B00 AS Date), N'11', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0052 ', N'Tính', N'Hoàng', N'1', CAST(0x00000000 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0053 ', N'Nam', N'Hoàng', N'0', CAST(0x00000000 AS Date), N'11', N'2017', N'252 Điện Biên Phủ', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0054 ', N'Yến', N'Hoàng', N'1', CAST(0x00000000 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0055 ', N'Lộc', N'Hoàng', N'1', CAST(0x1D230B00 AS Date), N'12', N'2017', N'124 Hải Châu', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0056 ', N'Hải Châu', N'Phan', N'0', CAST(0xB4210B00 AS Date), N'12', N'2017', N'132132 Hải Châu', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0057 ', N'Hải', N'Phan', N'0', CAST(0x83240B00 AS Date), N'12', N'2017', N'252 Điện Biên Phủ', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0058 ', N'Lê ', N'Phan', N'1', CAST(0x852E0B00 AS Date), N'12', N'2017', N'52/17 77th Street', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0059 ', N'Nam', N'Phan', N'0', CAST(0x47200B00 AS Date), N'12', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0060', N'Nguyễn ', N'Hoàng', N'0', CAST(0xD23C0B00 AS Date), N'11', N'2017', N'4442 Nguyễn Văn Linh', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0061', N'Hải Long', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0062', N'Huy', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'4442 Nguyễn Văn Linh', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0063', N'Hải Châu', N'Phạm', N'0', CAST(0xC23C0B00 AS Date), N'10', N'2017', N'132 Hồng Bàng', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0064', N'Văn Tèo', N'Phạm', N'1', CAST(0xCD3C0B00 AS Date), N'10', N'2017', N'123 Điện Biên Phủ', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0065', N'Hoàng', N'Phạm', N'0', CAST(0xCA3C0B00 AS Date), N'10', N'2017', N'222 Hạ Long', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0066', N'Văn Tèo', N'Phí', N'1', CAST(0xCA3C0B00 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0067', N'Văn Tèo', N'Phạm', N'0', CAST(0xC23C0B00 AS Date), N'11', N'2017', N'252 Điện Biên Phủ', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0068', N'Nam', N'Phạm', N'1', CAST(0xC33C0B00 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0069', N'Hải Châu', N'Hoàng', N'0', CAST(0xCA3C0B00 AS Date), N'11', N'2017', N'252 Điện Biên Phủ', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0070 ', N'Hải Nam', N'Phan', N'0', CAST(0x39200B00 AS Date), N'12', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'namnguyen@132.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0071 ', N'Nguyễn ', N'Hoàng', N'1', CAST(0xCA3C0B00 AS Date), N'11', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0072 ', N'Tính', N'Hoàng', N'1', CAST(0x00000000 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0073 ', N'Việt Phong', N'Hoàng', N'0', CAST(0x00000000 AS Date), N'11', N'2017', N'252 Điện Biên Phủ', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0074 ', N'Yến', N'Hoàng', N'1', CAST(0x00000000 AS Date), N'11', N'2017', N'222 Hạ Long', N'Quận Bình Thạnh', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0075 ', N'Lộc', N'Hoàng', N'1', CAST(0x1D230B00 AS Date), N'12', N'2017', N'124 Hải Châu', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0076 ', N'Hải Châu', N'Phan', N'0', CAST(0xB4210B00 AS Date), N'12', N'2017', N'132132 Hải Châu', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0077 ', N'Hải', N'Phan', N'0', CAST(0x83240B00 AS Date), N'12', N'2017', N'252 Điện Biên Phủ', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0078 ', N'Lê ', N'Phan', N'1', CAST(0x852E0B00 AS Date), N'12', N'2017', N'52/17 77th Street', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0079 ', N'Nam', N'Phan', N'0', CAST(0x47200B00 AS Date), N'12', N'2017', N'52/17 đường 77 p.Tân Quy', N'Quận 1', N'Ho Chi Minh City', N'VietNam', N'790000', N'01215530663', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0080', N'Huy', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'4442 Nguyễn Văn Linh', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0081', N'Văn Tí', N'Lê ', N'0', CAST(0x24240B00 AS Date), N'10', N'2017', N'222 Hạ Long', N'Hải Châu', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0082', N'Hải Châu', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0083', N'Hải Long', N'Phạm', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0084', N'Hải Long', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0085', N'Hải Châu', N'Phạm', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0086', N'VietNam', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0087', N'VietNam', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0088', N'Ngọc ', N'Phạm', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0089', N'VietNam', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0090', N'Huy', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'4442 Nguyễn Văn Linh', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0091', N'Văn Tí', N'Lê ', N'0', CAST(0x24240B00 AS Date), N'10', N'2017', N'222 Hạ Long', N'Hải Châu', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0092', N'Hải Châu', N'Nguyễn', N'', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0093', N'Văn Tèo', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0094', N'Lê ', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0095', N'Hải Châu', N'Lý', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0096', N'VietNam', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0097', N'Nam', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0098', N'VietNam', N'Đào', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0099', N'VietNam', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0100', N'Huy', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'4442 Nguyễn Văn Linh', N'Quận 3', N'Ho Chi Minh City', N'VietNam', N'790000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0101', N'Văn Tí', N'Lê ', N'0', CAST(0x24240B00 AS Date), N'10', N'2017', N'222 Hạ Long', N'Hải Châu', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0102', N'Hải Châu', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0103', N'Văn Tèo', N'Lý', N'1', CAST(0x34210B00 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0104', N'Việt Phong', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0105', N'Hải Châu', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0106', N'Nam', N'Quách', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'ti@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0107', N'Hải Phong', N'Nguyễn ', N'0', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0108', N'Nam', N'Nguyên', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Hải Châu', N'Da Nang', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 0, 1)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0109', N'Hải Châu', N'Nguyễn ', N'1', CAST(0x00000000 AS Date), N'10', N'2017', N'252 Điện Biên Phủ', N'Quận 7', N'Ho Chi Minh City', N'VietNam', N'585000', N'01699087127', N'teo@gmail.com', N'', 1, 0)
GO
INSERT [dbo].[Students] ([StudentID], [FirstName], [LastName], [Gender], [Birthday], [Grade], [YearOfAdmission], [Address], [District], [CityorProvince], [Country], [PostalCode], [PhoneNumber], [EmailAddress], [GraduationYear], [HasClass], [IsDeleted]) VALUES (N'S0110', N'Hải Phong', N'Nguyễn', N'1', CAST(0x83240B00 AS Date), N'10', N'2017', N'123 Sư Vạn Hạnh P.10', N'Quận 10', N'TP Hồ Chí Minh', N'Việt Nam', N'790090', N'0123334333', N'haiphong@gmail.com', N' ', 1, 1)
GO
INSERT [dbo].[Subjects] ([SubjectID], [SubjectName], [IsDeleted]) VALUES (N'MH001', N'Toán', 0)
GO
INSERT [dbo].[Subjects] ([SubjectID], [SubjectName], [IsDeleted]) VALUES (N'MH002', N'Vật Lý', 0)
GO
INSERT [dbo].[Subjects] ([SubjectID], [SubjectName], [IsDeleted]) VALUES (N'MH003', N'Hóa Học', 0)
GO
INSERT [dbo].[Subjects] ([SubjectID], [SubjectName], [IsDeleted]) VALUES (N'MH004', N'Sinh Học ', 0)
GO
INSERT [dbo].[Subjects] ([SubjectID], [SubjectName], [IsDeleted]) VALUES (N'MH005', N'Văn Học ', 0)
GO
INSERT [dbo].[Subjects] ([SubjectID], [SubjectName], [IsDeleted]) VALUES (N'MH006', N'Lịch Sử', 0)
GO
INSERT [dbo].[Subjects] ([SubjectID], [SubjectName], [IsDeleted]) VALUES (N'MH007', N'Địa Lý', 0)
GO
INSERT [dbo].[Subjects] ([SubjectID], [SubjectName], [IsDeleted]) VALUES (N'MH008', N'Đạo Đức', 0)
GO
INSERT [dbo].[Subjects] ([SubjectID], [SubjectName], [IsDeleted]) VALUES (N'MH009', N'Thể Dục', 0)
GO
INSERT [dbo].[SystemSettings] ([SettingID], [MinimumAged], [MaximumAged], [MaxinumStudentNumber], [StandardScore]) VALUES (N'ST001', 15, 20, 40, 5)
GO
/****** Object:  Index [UQ__Scores__EC0B14CD5629CD9C]    Script Date: 31/05/2017 4:14:16 PM ******/
ALTER TABLE [dbo].[Scores] ADD  CONSTRAINT [UQ__Scores__EC0B14CD5629CD9C] UNIQUE NONCLUSTERED 
(
	[INDEXAUTO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Classes_Students]  WITH CHECK ADD  CONSTRAINT [FK_Classs_Student_Classes] FOREIGN KEY([ClassID])
REFERENCES [dbo].[Classes] ([ClassID])
GO
ALTER TABLE [dbo].[Classes_Students] CHECK CONSTRAINT [FK_Classs_Student_Classes]
GO
ALTER TABLE [dbo].[Classes_Students]  WITH CHECK ADD  CONSTRAINT [FK_Classs_Student_Students] FOREIGN KEY([StudentID])
REFERENCES [dbo].[Students] ([StudentID])
GO
ALTER TABLE [dbo].[Classes_Students] CHECK CONSTRAINT [FK_Classs_Student_Students]
GO
ALTER TABLE [dbo].[Employee_Keys]  WITH CHECK ADD  CONSTRAINT [FK_EK_E] FOREIGN KEY([IDEmployee])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO
ALTER TABLE [dbo].[Employee_Keys] CHECK CONSTRAINT [FK_EK_E]
GO
ALTER TABLE [dbo].[Employee_Keys]  WITH CHECK ADD  CONSTRAINT [FK_EK_K] FOREIGN KEY([IDKey])
REFERENCES [dbo].[PermissionKeyList] ([IDKey])
GO
ALTER TABLE [dbo].[Employee_Keys] CHECK CONSTRAINT [FK_EK_K]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_EmloyeeRoles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[EmloyeeRoles] ([RoleID])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_EmloyeeRoles]
GO
ALTER TABLE [dbo].[FamilyInformation]  WITH CHECK ADD  CONSTRAINT [FK_FamilyInformation_Students1] FOREIGN KEY([StudentID])
REFERENCES [dbo].[Students] ([StudentID])
GO
ALTER TABLE [dbo].[FamilyInformation] CHECK CONSTRAINT [FK_FamilyInformation_Students1]
GO
ALTER TABLE [dbo].[Role_Keys]  WITH CHECK ADD  CONSTRAINT [FK_RK_R] FOREIGN KEY([IDRole])
REFERENCES [dbo].[EmloyeeRoles] ([RoleID])
GO
ALTER TABLE [dbo].[Role_Keys] CHECK CONSTRAINT [FK_RK_R]
GO
ALTER TABLE [dbo].[Role_Keys]  WITH CHECK ADD  CONSTRAINT [FK_RL_PK] FOREIGN KEY([IDKey])
REFERENCES [dbo].[PermissionKeyList] ([IDKey])
GO
ALTER TABLE [dbo].[Role_Keys] CHECK CONSTRAINT [FK_RL_PK]
GO
ALTER TABLE [dbo].[ScoreCorrectionDetails]  WITH CHECK ADD  CONSTRAINT [FK_ScoreCorrectionDetails_ScoreCorrections1] FOREIGN KEY([ScoreCorrectionID])
REFERENCES [dbo].[ScoreCorrections] ([ScoreCorrectionID])
GO
ALTER TABLE [dbo].[ScoreCorrectionDetails] CHECK CONSTRAINT [FK_ScoreCorrectionDetails_ScoreCorrections1]
GO
ALTER TABLE [dbo].[ScoreCorrections]  WITH CHECK ADD  CONSTRAINT [FK_ScoreCorrections_Employees] FOREIGN KEY([UserCreateID])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO
ALTER TABLE [dbo].[ScoreCorrections] CHECK CONSTRAINT [FK_ScoreCorrections_Employees]
GO
ALTER TABLE [dbo].[ScoreCorrections]  WITH CHECK ADD  CONSTRAINT [FK_ScoreCorrections_Employees1] FOREIGN KEY([UserDecideID])
REFERENCES [dbo].[Employees] ([EmployeeID])
GO
ALTER TABLE [dbo].[ScoreCorrections] CHECK CONSTRAINT [FK_ScoreCorrections_Employees1]
GO
ALTER TABLE [dbo].[Scores]  WITH CHECK ADD  CONSTRAINT [FK_Scores_Students] FOREIGN KEY([StudentID])
REFERENCES [dbo].[Students] ([StudentID])
GO
ALTER TABLE [dbo].[Scores] CHECK CONSTRAINT [FK_Scores_Students]
GO
ALTER TABLE [dbo].[Scores]  WITH CHECK ADD  CONSTRAINT [FK_Scores_Subjects] FOREIGN KEY([SubjectID])
REFERENCES [dbo].[Subjects] ([SubjectID])
GO
ALTER TABLE [dbo].[Scores] CHECK CONSTRAINT [FK_Scores_Subjects]
GO
USE [master]
GO
ALTER DATABASE [StudentManagementDB] SET  READ_WRITE 
GO
