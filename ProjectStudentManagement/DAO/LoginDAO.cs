﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;

namespace DAO
{
    public class LoginDAO
    {
        /// <summary>
        /// Hàm kiểm tra tài khoản đăng nhập
        /// </summary>
        /// <param name="User">Thông tin người dùng</param>
        /// <returns></returns>
        public string loginCheck(UserDTO User)
        {
            
            object[] objKeyWords = new[] {
                "@USERNAME", User.Username,
                "@PWS", DataConnect.MD5Hash(User.Password)
            };

            DataTable dtbLoginData = new DataConnect().callUSP("usp_logincheck", objKeyWords);
            //Nếu tồn tại dòng dữ liệu chứng tỏ đăng nhập thành công
            if (dtbLoginData != null && dtbLoginData.Rows.Count != 0)
            {
                return User.Username;
            }
            return null;
        }

        /// <summary>
        /// Lấy danh sách quyền theo người dùng
        /// </summary>
        /// <param name="strUsername">Mã người dùng</param>
        /// <returns></returns>
        public DataTable GetKeyPermission(string strUsername)
        {
            object[] objKeyWords = new[] {
                "@Username", strUsername
            };

            DataTable dtbKeyPermission = new DataConnect().callUSP("usp_keypermissiondata", objKeyWords);
            if (dtbKeyPermission != null && dtbKeyPermission.Rows.Count != 0)
            {
                return dtbKeyPermission;
            }
            return null;
        }


        public static bool CheckKeyPermission(string strUsername, string strKey)
        {
            if(strUsername == null | strUsername == "")
            {
                return false;
            }
            object[] objKeyWords = new[] {
                "@USENAME", strUsername,
                "@KEY", strKey
            };

            DataTable dtbKeyPermission = new DataConnect().callUSP("checkPerKey", objKeyWords);
            if (dtbKeyPermission != null && dtbKeyPermission.Rows.Count != 0)
            {
                return true;
            }
            return false;
        }


        public string NameLoginAcc(string useName)
        {

            object[] objKeyWords = new[] {
                "@USERNAME", useName
            };

            DataTable dtbLoginData = new DataConnect().callUSP("usp_loginname", objKeyWords);
            //Nếu tồn tại dòng dữ liệu chứng tỏ đăng nhập thành công
            if (dtbLoginData != null && dtbLoginData.Rows.Count != 0)
            {
                string sNameAcc = (string)dtbLoginData.Rows[0][1];
                sNameAcc += " ";
                sNameAcc += (string)dtbLoginData.Rows[0][0];
                return sNameAcc;
            }
            return null;
        }
    }
}
