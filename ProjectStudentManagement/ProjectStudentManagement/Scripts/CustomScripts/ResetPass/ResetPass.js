﻿$(document).ready(function () {
    //Khởi tạo sự kiện cho nút đăng nhập
    InitResetPass();
})

//Khởi tạo sự kiện cho nút đăng nhập
function InitResetPass() {
    $("#btnResetPass").click(function () {
        var username = $("#txtUsername").val();

        //Kiểm tra dữ liệu
        if (username == "") {
            $.notify("Vui lòng nhập Tên đăng nhập", "info");
            $("#inptUsername").focus();
            return false;
        }

        $.ajax({
            url: "/Home/DefaultPass",
            type: "POST",
            data: JSON.stringify({ strUsername: username }),
            contentType: 'application/json; charset=UTF-8',
            dataType: "json",
            beforeSend: function () {
                showLoading("btnResetPass");
            },
            success: function (data) {
                if (data.iserror == false) {
                    $('#ModalSendPass').modal('toggle');
                } else {
                    if (data.message != "") {
                        $.notify(data.message, "error");
                    } else {
                        $.notify("Đã có lỗi xảy ra vui lòng thông báo với quản trị viên!", "error");
                    }
                }

                hideLoading("btnResetPass");
            },
            error: function (xhr, status, error) {
                hideLoading("btnResetPass");
                $.notify("Đã có lỗi xảy ra vui lòng thông báo với quản trị viên!", "error");
            }
        });
    });
}

//Quay về trang chủ khi reset pass thành công
function ConfirmSuccessReset() {
    setTimeout(function () { window.location.href = "/Home/Login" }, 500);
}