﻿var IsLoadServer = false;
$(document).ready(function () {

    //Khoi tao su kien sau khi update
    $('#frmClassesStudentsUpdate').ajaxForm(function (data) {
        ajaxUpdateSuccess(data);
    });
    $('#sltKeyWordYear option:first-child').attr("selected", "selected");
    $('#sltKeyWordClass option:first-child').attr("selected", "selected");
    $('#sltKeyWordYear')[0].selectedIndex = 0;
    $('#sltKeyWordClass')[0].selectedIndex = 0;
    InitProcessInTable();
    InitYear();
    GetClassList();
    InitAddStudent(intmaximumStudentNumber);
    //InitAddListStudent(8);
    InitDisabledControl();
    SltYearOnChange();
    SltClassOnChange();
    btnSearchClick();

    $('#sltKeyWordClass').keypress(function (e) {
        var key = e.which;
        if (key == 13) {
            $('#btnSearch').click();
            return false;
        }
    });
    $('#sltKeyWordClass').keypress(function (e) {
        var key = e.which;
        if (key == 13) {
            $('#btnSearch').click();
            return false;
        }
    });
});
//START----INIT----
function InitDisabledControl() {
    $("#btnAddStudentToClass").attr('disabled', 'disabled');
    $("#btnInsert").attr('disabled', 'disabled');
    $("#sltKeyWordClass").attr('disabled', 'disabled');
    $("#btnSearch").attr('disabled', 'disabled');
    $("#btnUpdate").attr('disabled', 'disabled');

}
function InitYear() {
    var year = new Date().getFullYear();
    for (var i = year; i >= year - 20; i--) {
        $("#sltKeyWordYear").append("<option value='" + i + "'>Niên khóa " + i + " </option>");
    }
}
function InitProcessInTable() {
    $('#ContentStudents tr').each(function () {
        $(this).find('td span[data-name="Detail"]').click(function () {
            var strStudentID = $(this).closest('td').attr('data-studentid');
            setTimeout(function () { window.location.href = "/quan-ly-hoc-sinh/chi-tiet/" + strStudentID }, 500);
        });

        $(this).find('td span[data-name="btnRemoveStudent"]').click(function () {
            var strStudentID = $(this).closest('td').attr('data-studentid');
            var strMessage = "Bạn có muốn xóa học sinh có mã" + strStudentID + " ra khỏi danh sách lớp?"

            $('#divMessage').html(strMessage);
            $('#ModalConfirmDelete').modal('toggle');
            $('#divMessage').attr('data-id', strStudentID);
        });
    });
}
function InitAddStudent(maximumNumber) {
    $('input[data-name="btnAddStudent"]').each(function () {
        $(this).click(function () {
            var length = parseInt($('#studentNumber').val());
            if (this.checked == false) {
                $(this).parent().parent().parent().attr("data-selected", "false");
                $(this).parent().parent().parent().removeClass('success');
                length -= 1;
                $('#studentNumber').val(length);
                $('input[data-name="btnAddStudent"]').each(function () {
                    if (this.checked == false) {
                        $(this).removeAttr("disabled");
                    }
                });
            }
            else {
                if (length == maximumNumber - 1) {
                    $('input[data-name="btnAddStudent"]').each(function () {
                        if (this.checked == false) {
                            $(this).attr("disabled", "disabled");

                        }
                    });
                    $.notify("Đã có đủ " + maximumNumber + " học sinh cho một lớp", "warn");
                }
                if (length > maximumNumber) {
                    $.notify("Đã vượt quá giới hạn " + maximumNumber + " học sinh cho một lớp", "warn");
                } else {
                    $(this).parent().parent().parent().addClass('success');
                    $(this).parent().parent().parent().attr("data-selected", "true");
                    length += 1;
                    $('#studentNumber').val(length);
                }
            }
            var selectedrow = $('tr[data-selected="true"]').length;
            if (selectedrow > 0) {
                $('#btnUpdate').removeAttr('disabled');
            }else {
                $('#btnUpdate').attr('disabled', 'disabled');
            }

        });
    });
}

//END----INIT----

//START----CHECK FUNCTION----
function CheckValidate() {
    var strKeyWordYear = $('#sltKeyWordYear').val();
    var sltKeyWordClass = $('#sltKeyWordClass').val();
    if (strKeyWordYear == null) {
        $.notify("Vui lòng chọn Niên Khóa", "error");
        return false;
    }
    if (sltKeyWordClass == null) {
        $.notify("Vui lòng chọn Lớp học", "error");
        return false;
    }
    return true;
}
function checkIDKeyMainTable(idkey) {
    var check = true;

    $('#ContentStudents tr').each(function () {
        var idkeytemp = $(this).find('td span[data-name="btnRemoveStudent"]').attr('data-idkey');

        if (idkeytemp == idkey)
            check = false;
    });
    return check;
}
//END----CHECK FUNCTION----

//START----EVENT----
function btnSearchClick() {
    $("#btnSearch").click(function () {
         $("#btnAddStudentToClass").removeAttr('disabled');   
    });
    //$("#btnSearch").click(function () {
    //    var year = new Date().getFullYear();
    //    var sltyear = $('#sltKeyWordYear').val();
    //    if (sltyear == year) {
    //        $("#btnAddStudentToClass").removeAttr('disabled');
    //    }
    //    else {
    //        $("#btnAddStudentToClass").attr('disabled', 'disabled');
    //    }
    //});
}
function SltClassOnChange() {
    $("#sltKeyWordClass").change(function () {
        $("#btnSearch").removeAttr('disabled');
        $("#btnAddStudentToClass").attr('disabled', 'disabled');
        ResetTable();
    });

}
function SltYearOnChange() {
    $("#sltKeyWordYear").change(function () {
        $("#sltKeyWordClass").removeAttr('disabled');
        $("#btnAddStudentToClass").attr('disabled', 'disabled');
        ResetTable();
    });

}
function ResetTable() {
    $('#ContentStudents tr').remove();
    $('#ContentStudents').append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
    $('#divPaging').hide();
    $('#total-record').html('');
}
function OpenModalStudentList() {
    $('#ModalStudentList').modal('toggle');
}
function CloseModalStudentList() {
    $('#ModalStudentList').modal('toggle');
}
function BackManage() {
    setTimeout(function () { window.location.href = "/quan-ly-lop-hoc" }, 500);
}
//END----EVENT----

//START----GET DATA----
function GetClassList() {

    $.ajax({
        url: "/ClassesStudents/GetClassList",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            if (data.iserror == false) {
                var obj = JSON.parse(data.classlistcontent);

                var objsize = Object.keys(obj).length;
                for (var i = 0; i < objsize; i++) {
                    $("#sltKeyWordClass").append("<option data-value='" + obj[i].Grade + "' value='" + obj[i].ClassID + "'>Lớp " + obj[i].ClassName + " </option>");
                }
            }
            else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });

}

function GetStudentsByClassID() {
    var param = {};
    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.sltKeyWordClass = $('#sltKeyWordClass').val();

    $.ajax({
        url: "/ClassesStudents/GetStudentsByClassID",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
            $("#btnSearch").button('loading');
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentStudents').html(data.content);
                Paging(data.totalpages, data.totalrows, 1);
                InitProcessInTable();

                $('#sltKeyWordYear').attr('data-value', $('#sltKeyWordYear').val());
                $('#sltKeyWordClass').attr('data-value', $('#sltKeyWordClass').val());

            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
            $("#btnSearch").button('reset');
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            $("#btnSearch").button('reset');
        }
    });
}
function GetStudentListByGrade() {
    var maximumStudentNumber = intmaximumStudentNumber;
    var numberStudent = parseInt($('#total-record').html());
    if (numberStudent >= maximumStudentNumber){
        $.notify("Đã vượt quá số lượng học sinh tối đa. Không thể thêm!", "error");
    }
    else {
        var param = {};
        param.sltGrade = $('#sltKeyWordClass').children('option:selected').attr('data-value');
        param.sltClassID = $('#sltKeyWordClass').val();
        param.strKeyWordYear = $('#sltKeyWordYear').val();

        $.ajax({
            url: "/ClassesStudents/GetStudentListByGrade",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(param),
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (data) {
                if (data.iserror == false) {
                    OpenModalStudentList();
                    $('#ContentStudentsModal').html(data.content);
                    $('#studentNumber').val(data.studentnumber);

                    InitAddStudent(intmaximumStudentNumber);
                    //InitAddListStudent(8);
                } else {
                    $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
                }
            },
            error: function (xhr, status, error) {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        });
    }
   

}
//END----GET DATA----

//START----PAGING----
function Paging(totalPages, totalRows, startpage) {
    if (totalRows > parseInt(visiblepages)) {
        $('#paging').remove();
        $('#divPagingIndex').html("<ul id='paging' class='pagination'></ul>");
        $('#paging').twbsPagination({
            startPage: startpage,
            totalPages: totalPages,
            visiblePages: visiblepages,
            onPageClick: function (event, page) {
                //Ghi nhận chỉ số trang hiện tại
                $('#hdCurrentPage').val(page - 1); //Vì store bắt đầu từ 0 nên -1

                //Load data ứng với chỉ số page từ server
                if (IsLoadServer == true)
                    SearchAjaxPaging();

                //Thể hiện chỉ số record đang hiển thị trên giao diện
                var visiblePages = parseInt(visiblepages);
                var ToIndextemp = page * visiblePages;
                var FromIndex = (page - 1) * visiblePages + 1;
                //Lấy chỉ số đến trang (Nếu không là trang cuối)
                if (ToIndextemp < totalRows)
                    ToIndex = ToIndextemp;
                else
                    ToIndex = totalRows;

                $('#page-from-index').text(FromIndex);
                $('#page-to-index').text(ToIndex);
                $('#total-record').text(totalRows);

                IsLoadServer = true;
            }
        });

        $('#divPaging').css('display', 'block');
    } else {
        $('#total-record').text(totalRows);
        $('#divPaging').css('display', 'none');
    }
}
function SearchAjaxPaging() {
    //Nạp tham số
    var param = {};
    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.sltKeyWordClass = $('#sltKeyWordClass').val();
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/ClassesStudents/GetStudentsByClassID",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentStudents').html(data.content);
                InitProcessInTable();
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}
//END----PAGING----

//START----UPDATE DATA----
function UpdateStudentToClass() {
    var strKeyWordYear = $('#sltKeyWordYear').val();
    var sltKeyWordClass = $('#sltKeyWordClass').val();
    showLoading("btnUpdate");
    var arr = [];
    if ($('#ContentStudentsModal tr td').length > 1) {
        $('#ContentStudentsModal').find('tr[data-selected="true"]').each(function () {
            var idkey = $(this).find('td[data-name="idkey"]').html();
            var objName = {
                StudentID: idkey,
                Year: strKeyWordYear,
                ClassID: sltKeyWordClass,

            };

            arr.push(objName);
        });
        $('#hdStudentsList').val(JSON.stringify(arr));
    }
    $('#frmClassesStudentsUpdate').submit();
    return true;
}
function ajaxUpdateSuccess(data) {
    hideLoading("btnUpdate");

    if (data.iserror == false) {
        $.notify("Cập nhật danh sách lớp thành công.", "success");
        setTimeout(function () {
            setTimeout(function () { window.location.href = "/quan-ly-lop-hoc/" }, 500);
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}
//END----UPDATE DATA----
//START----DELETE----

function DeleleStudent() {
    $('#hdCurRowBeforeDelOnScreen').val($('#ContentStudents tr').length);
    param = {};
    param.strStudentID = $('#divMessage').attr('data-id');
    param.strSltYear = $('#sltKeyWordYear').val();
    param.strClassID = $('#sltKeyWordClass').val();

    $.ajax({
        url: "/ClassesStudents/DeleteStudentFromClass",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                SearchStudentAfterDelete(parseInt($('#hdCurRowBeforeDelOnScreen').val()) - 1);
            } else {
                if (data.message != "") {
                    $.notify(data.message, "error");
                } else {
                    $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
                }
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

function SearchStudentAfterDelete(CurRowAfterDelOnScreen) {
    //Nạp tham số
    if (CurRowAfterDelOnScreen == 0)
        $('#hdCurrentPage').val(parseInt($('#hdCurrentPage').val()) - 1);
    else
        $('#hdCurrentPage').val(parseInt($('#hdCurrentPage').val()));

    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.sltKeyWordClass = $('#sltKeyWordClass').val();
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/ClassesStudents/GetStudentsByClassID",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentStudents').html(data.content);
                Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()) + 1);
                InitProcessInTable();

            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}
//END----DELETE----
