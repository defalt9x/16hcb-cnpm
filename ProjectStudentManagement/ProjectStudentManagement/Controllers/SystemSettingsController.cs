﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectStudentManagement.Controllers
{
    public class SystemSettingsController : Controller
    {
        public ActionResult Index()
        {
            //Kiểm tra xem người dùng có được xem(truy cập vào chức năng)
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLCD_V") == false)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra xem người dùng có được phép cập nhật
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLCD_U") == false)
            {
                ViewBag.EditPermission = false;
            }
            else
            {
                ViewBag.EditPermission = true;
            }
            SystemSettingsDTO dto = new SystemSettingsDTO();
            DataTable dtb = new SystemSettingsDAO().SearchSystemSettings();
            if (dtb != null && dtb.Rows.Count != 0)
            {
                dto.SettingID = dtb.Rows[0]["SettingID"].ToString();
                dto.MaximumAged = Convert.ToInt32(dtb.Rows[0]["MaximumAged"].ToString());
                dto.MinimumAged = Convert.ToInt32(dtb.Rows[0]["MinimumAged"].ToString());
                dto.MaximumStudentNumber = Convert.ToInt32(dtb.Rows[0]["MaxinumStudentNumber"].ToString());
                dto.StandardScore = Convert.ToDouble(dtb.Rows[0]["StandardScore"].ToString());
            }
            return View(dto);
        }

        [HttpPost]
        public ActionResult SystemSettingsUpdate(FormCollection frm)
        {

            try
            {
                int intTemp = 0;
                double doubTemp = 0;
                SystemSettingsDTO dto = new SystemSettingsDTO();

                dto.SettingID = frm["txtSettingID"];

                int.TryParse(frm["txtMinimumAged"], out intTemp);
                dto.MinimumAged = intTemp;

                int.TryParse(frm["txtMaximumAged"], out intTemp);
                dto.MaximumAged = intTemp;

                int.TryParse(frm["txtMaximumStudentNumber"], out intTemp);
                dto.MaximumStudentNumber = intTemp;

                double.TryParse(frm["txtStandardScore"], out doubTemp);
                dto.StandardScore = doubTemp;

                bool bolSuccess = new SystemSettingsDAO().UpdateSystemSettings(dto);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception ex)
            {
                return Json(new { iserror = true });
            }

        }
    }
}
