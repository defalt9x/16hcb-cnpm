﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ScoresDAO
    {
        public DataTable GetSubjectList()
        {
            DataTable dtbReport = new DataConnect().callUSP("uspGetSubjectList", null);
            if (dtbReport != null && dtbReport.Rows.Count != 0)
            {
                return dtbReport;
            }

            return null;
        }

        public DataTable GetClassList(string strKeyWord)
        {
            object[] objKeyWords = new object[] {
                "@KeyWordYear", strKeyWord
            };
            DataTable dtbReport = new DataConnect().callUSP("uspGetClassListYear", objKeyWords);
            if (dtbReport != null && dtbReport.Rows.Count != 0)
            {
                return dtbReport;
            }

            return null;
        }

        public DataTable GetScore(string strKeyWordYear, string strKeyWordSemeter, string strKeyWordStudent, string strKeyWordClass, string strKeyWordSubject, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWordStudent", strKeyWordStudent,
                "@KeyWordYear", strKeyWordYear,
                "@KeyWordClass", strKeyWordClass,
                "@KeyWordSubject", strKeyWordSubject,
                "@KeyWordSemeter", strKeyWordSemeter
            };
            DataTable dtbReport = new DataConnect().callUSP("uspGetScore", objKeyWords);
            if (dtbReport != null && dtbReport.Rows.Count != 0)
            {
                return dtbReport;
            }

            return null;
        }


        public void ScoreEdit(string strKeyWordStudent, string strKeyWordYear, string strKeyWordSemeter, string strKeyWordSubject, string strKeyWordScore1, string strKeyWordScore2, string strKeyWordScore3)
        {
            object[] objKeyWords = new object[] {
                "@KeyWordStudent", strKeyWordStudent,
                "@KeyWordYear", strKeyWordYear,
                "@KeyWordSemester", strKeyWordSemeter,
                "@KeyWordSubject", strKeyWordSubject,
                "@KeyWordScore1", strKeyWordScore1,
                "@KeyWordScore2", strKeyWordScore2,
                "@KeyWordScore3", strKeyWordScore3
            };
            DataTable dtbReport = new DataConnect().callUSP("uspScoreEdit", objKeyWords);
        }

        public DataTable GetScoreBySubject(string strSubjectID)
        {
            object[] objKeyWords = new object[] {
                "@SubjectID", strSubjectID
            };

            DataTable dtbScores = new DataConnect().callUSP("uspGetScore_BySubject_SHR", objKeyWords);
            if (dtbScores != null && dtbScores.Rows.Count != 0)
            {
                return dtbScores;
            }

            return null;
        }
    }
}
