﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectStudentManagement.Controllers
{
    public class EmloyeeRolesController : Controller
    {
        #region Quản lý
        public ActionResult Index()
        {
            //Kiểm tra xem người dùng có được xem(truy cập vào chức năng)
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLNND_V") == false)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra xem người dùng có được phép thêm
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLNND_I") == false)
            {
                ViewBag.InsertPermission = false;
            }



            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            return View();
        }

        [HttpPost]
        public ActionResult SearchEmloyeeRoles(string strKeyWord, int intPageIndex = 0)
        {
            try
            {
                //Lấy cấu hình số dòng trên trang
                //int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTMLEmloyeeRoles = new StringBuilder();

                DataTable dtbEmloyeeRoles = new EmloyeeRolesDAO().SearchEmloyeeRoles(strKeyWord, intPageIndex, intVisiblePages);

                if (dtbEmloyeeRoles != null && dtbEmloyeeRoles.Rows.Count > 0)
                {
                    strHTMLEmloyeeRoles = GenHTMLEmloyeeRoles(dtbEmloyeeRoles);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtbEmloyeeRoles.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTMLEmloyeeRoles.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new { iserror = false, 
                                content = Convert.ToString(strHTMLEmloyeeRoles), 
                                totalpages = intTotalPages, 
                                totalrows = intTotalRows });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        private StringBuilder GenHTMLEmloyeeRoles(DataTable dtbPermission)
        {
            bool bPerEdit = true;
            bool bPerDelete = true;

            //Kiểm tra người dùng được xóa
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLNND_D") == false)
            {
                bPerDelete = false;
            }


            int intCountRow = dtbPermission.Rows.Count;
            StringBuilder strHTMLEmloyeeRoles = new StringBuilder();

            for (int i = 0; i < intCountRow; i++)
            {
                string strRoleID = Convert.ToString(dtbPermission.Rows[i]["RoleID"]).Trim();

                strHTMLEmloyeeRoles.Append("<tr>");
                strHTMLEmloyeeRoles.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i]["STT"]);
                strHTMLEmloyeeRoles.AppendFormat("<td>{0}</td>", strRoleID);
                strHTMLEmloyeeRoles.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i]["RoleName"]);
                string sEdit = "";
                string sDelete = "";
                if (bPerEdit != false)
                {
                    sEdit += "<span class='glyphicon glyphicon-pencil table-icon' data-name='Detail'></span>";
                }

                if (bPerDelete != false)
                {
                    sDelete += "<span class='glyphicon glyphicon-trash table-icon' data-name='Delete'></span>";
                }
                strHTMLEmloyeeRoles.AppendFormat("<td data-roleid='{0}'>" + sEdit + sDelete + "</td>", strRoleID);

                strHTMLEmloyeeRoles.Append("</tr>");
            }

            return strHTMLEmloyeeRoles;
        }

        public ActionResult EmloyeeRolesInsert()
        {
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            return View();
        }

        public ActionResult EmloyeeRolesUpdate(string strRoleID)
        {
            //Kiểm tra người dùng được sửa
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLNND_U") == false)
            {
                return RedirectToAction("Login", "Home");
            }
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            EmloyeeRolesDTO EmloyeeRoles = new EmloyeeRolesDAO().GetEmloyeeRolesByID(strRoleID);
            return View(EmloyeeRoles);
        }

        public ActionResult EmloyeeRolesDetail(string strRoleID)
        {
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLNND_V") == false)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra người dùng được sửa
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLNND_U") == false)
            {
                ViewBag.UpdatePermission = false;
            }
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            EmloyeeRolesDTO EmloyeeRoles = new EmloyeeRolesDAO().GetEmloyeeRolesByID(strRoleID);
            return View(EmloyeeRoles);
        }

        [HttpPost]
        public JsonResult DeleteEmloyeeRoles(string strRoleID)
        {
            try
            {
                string strMessageError = string.Empty;
                bool bolSuccess = new EmloyeeRolesDAO().DeleteEmloyeeRoles(strRoleID, ref strMessageError);

                if (bolSuccess == false)
                    return Json(new { iserror = true, message = strMessageError });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true, message = "" });
            }
        }
        #endregion

        #region Thêm
        [HttpPost]
        public ActionResult SearchPermission(string strKeyWord, int intPageIndex = 0)
        {
            try
            {
                //Lấy cấu hình số dòng trên trang
                int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTMLPermission = new StringBuilder();

                DataTable dtbPermission = new EmloyeeRolesDAO().SearchPermission(strKeyWord, intPageIndex, intPageSize);

                if (dtbPermission != null && dtbPermission.Rows.Count > 0)
                {
                    strHTMLPermission = GenHTMLPermission(dtbPermission);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtbPermission.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTMLPermission.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTMLPermission),
                    totalpages = intTotalPages,
                    totalrows = intTotalRows
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        private StringBuilder GenHTMLPermission(DataTable dtbPermission)
        {
            int intCountRow = dtbPermission.Rows.Count;
            StringBuilder strHTMLPermission = new StringBuilder();

            for (int i = 0; i < intCountRow; i++)
            {
                string strIDKey = Convert.ToString(dtbPermission.Rows[i]["IDKey"]).Trim();

                strHTMLPermission.AppendFormat("<tr id='tr_{0}'>", strIDKey);
                strHTMLPermission.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i]["STT"]);
                strHTMLPermission.AppendFormat("<td data-name='idkey'>{0}</td>", strIDKey);
                strHTMLPermission.AppendFormat("<td data-name='description'>{0}</td>", dtbPermission.Rows[i]["KeyDescription"]);
                strHTMLPermission.AppendFormat("<td><span class='glyphicon glyphicon-plus table-icon' data-name='btnAddPermission' data-idkey='{0}'></span></td>", strIDKey);
                strHTMLPermission.Append("</tr>");
            }

            return strHTMLPermission;
        }

        [HttpPost]
        public ActionResult SearchPermissionAll(string strKeyWord, int intPageIndex = 0)
        {
            try
            {
                StringBuilder strHTMLPermission = new StringBuilder();

                DataTable dtbPermission = new EmloyeeRolesDAO().SearchPermission(strKeyWord, -1, -1);

                if (dtbPermission != null && dtbPermission.Rows.Count > 0)
                {
                    strHTMLPermission = GenHTMLPermissionAll(dtbPermission);
                }
                else
                {
                    strHTMLPermission.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTMLPermission)
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        private StringBuilder GenHTMLPermissionAll(DataTable dtbPermission)
        {
            int intCountRow = dtbPermission.Rows.Count;
            StringBuilder strHTMLPermission = new StringBuilder();

            for (int i = 0; i < intCountRow; i++)
            {
                string strIDKey = Convert.ToString(dtbPermission.Rows[i]["IDKey"]).Trim();

                strHTMLPermission.AppendFormat("<tr id='tr_{0}'>", strIDKey);
                strHTMLPermission.AppendFormat("<td data-name='idkey'>{0}</td>",strIDKey);
                strHTMLPermission.AppendFormat("<td data-name='description'>{0}</td>", dtbPermission.Rows[i]["KeyDescription"]);
                strHTMLPermission.AppendFormat("<td><span class='glyphicon glyphicon-remove table-icon' data-name='btnRemovePermission' data-idkey='{0}'></span></td>", strIDKey);
                strHTMLPermission.Append("</tr>");
            }

            return strHTMLPermission;
        }

        [HttpPost]
        public JsonResult InsertEmloyeeRoles(FormCollection frm)
        {
            try
            {
                List<PermissionDTO> lstSubjectExemptRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PermissionDTO>>(frm["hdListPermission"]);
                string strRoleName = Convert.ToString(frm["txtRoleName"]);

                bool bolSuccess = new EmloyeeRolesDAO().InsertEmloyeeRoles(strRoleName, lstSubjectExemptRequest);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region Cập nhật
        [HttpPost]
        public JsonResult UpdateEmloyeeRoles(FormCollection frm)
        {
            try
            {
                List<PermissionDTO> lstSubjectExemptRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PermissionDTO>>(frm["hdListPermission"]);
                string strRoleID = Convert.ToString(frm["hdRoleID"]);
                string strRoleName = Convert.ToString(frm["txtRoleName"]);

                bool bolSuccess = new EmloyeeRolesDAO().UpdateEmloyeeRoles(strRoleID, strRoleName, lstSubjectExemptRequest);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion
    }
}