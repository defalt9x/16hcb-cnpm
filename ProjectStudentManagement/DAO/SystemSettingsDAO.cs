﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class SystemSettingsDAO
    {
        public DataTable SearchSystemSettings()
        {
            object[] objKeyWords = new object[] {            
            };

            DataTable dtbSystemSettings = new DataConnect().callUSP("uspSystemSettings_GetSetting", objKeyWords);
            if (dtbSystemSettings != null && dtbSystemSettings.Rows.Count != 0)
            {
                return dtbSystemSettings;
            }

            return null;
        }

        public bool InsertSystemSettings(string strValue, string strObject)
        {
            object[] objKeyWords = new object[] {
                "@Value", strValue,
                "@Object", strObject
            };

            return new DataConnect().uspExecuteNonQuery("uspSystemSettings_ADD", objKeyWords);
        }

        public bool UpdateSystemSettings(SystemSettingsDTO dto)
        {
            object[] objKeyWords = new object[] {
                "@SettingID",dto.SettingID,
                "@MinimumAged", dto.MinimumAged,
                "@MaximumAged", dto.MaximumAged,
                "@MaximumStudentNumber", dto.MaximumStudentNumber,
                "@StandardScore", dto.StandardScore
            };

            return new DataConnect().uspExecuteNonQuery("uspSystemSettings_Update", objKeyWords);
        }

        public bool DeleteSystemSettings(string strSettingID)
        {
            object[] objKeyWords = new object[] {
                "@SettingID", strSettingID
            };

            return new DataConnect().uspExecuteNonQuery("uspSystemSettings_DEL", objKeyWords);
        }

        //public SystemSettingsDTO GetSystemSettingsByID(string strSettingID)
        //{
        //    SystemSettingsDTO objResult = new SystemSettingsDTO();

        //    object[] objKeyWords = new object[] {
        //        "@SettingID", strSettingID
        //    };

        //    DataTable dtbSystemSettings = new DataConnect().callUSP("uspSystemSettings_ByID_SRH", objKeyWords);
        //    if (dtbSystemSettings != null && dtbSystemSettings.Rows.Count > 0)
        //    {
        //        objResult.SettingID = Convert.ToString(dtbSystemSettings.Rows[0]["SettingID"]).Trim();
        //        objResult.Value = Convert.ToString(dtbSystemSettings.Rows[0]["Value"]).Trim();
        //        objResult.Object = Convert.ToString(dtbSystemSettings.Rows[0]["Object"]).Trim();
        //        objResult.Deleted = Convert.ToBoolean(Convert.ToString(dtbSystemSettings.Rows[0]["IsDeleted"]).Trim());
        //        return objResult;
        //    }

        //    return null;
        //}
    }
}
