﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using DAO;
using Newtonsoft.Json;
using System.Configuration;
using System.Text;
using System.Data;
using ClosedXML.Excel;
using System.IO;

namespace ProjectStudentManagement.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }

        #region Login
        public ActionResult Login()
        {
            ViewBag.Message = "Trang đăng nhập";
            if (Session["Username"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult LoginCheck(UserDTO user)
        {
            try
            {
                LoginDAO DLogin = new LoginDAO();
                Session["Username"] = DLogin.loginCheck(user);
                Session["Fullname"] = DLogin.NameLoginAcc((string)Session["Username"]);
                if (Session["Username"] != null)
                {
                    return Json(new { iserror = false, chkUser = true });
                }
                return Json(new { iserror = false, chkUser = false });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region Logout
        [HttpPost]
        public ActionResult Logout()
        {

            if (Session["Username"] != null)
            {
                Session["Username"] = null;
                return Json(true);
            }
            return Json(false);


        }
        #endregion

        #region XmlMenu
        [HttpPost]
        public ActionResult craftXmlmenu()
        {
            XMLDAO DXML = new XMLDAO();
            return Content(DXML.DynamicXMLMenu(Server.MapPath(@"\Menu.xml"), (string)Session["Username"]));
        }
        #endregion

        #region Report

        public ActionResult Report()
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
            return View();
        }

        public ActionResult Report2()
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
            return View();
        }

        [HttpPost]
        public ActionResult GetYearList()
        {

            try
            {
                DataTable dtbManageReport = new ReportDAO().GetYearList();
                string sYearClassList = JsonConvert.SerializeObject(dtbManageReport);
                return Json(new { iserror = false, yearlistcontent = sYearClassList });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }


        [HttpPost]
        public ActionResult ReportSearch(string strKeyWordYear, string strKeyWordSemeter, int intPageIndex = 0)
        {
            try
            {
                //Lấy cấu hình số dòng trên trang
                //int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTMLManageReport = new StringBuilder();

                DataTable dtbManageReport = new ReportDAO().SearchReport(strKeyWordYear, strKeyWordSemeter, intPageIndex, intVisiblePages);

                if (dtbManageReport != null && dtbManageReport.Rows.Count > 0)
                {
                    strHTMLManageReport = GenHTMLReport(dtbManageReport);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtbManageReport.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTMLManageReport.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTMLManageReport),
                    totalpages = intTotalPages,
                    totalrows = intTotalRows
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }


        public ActionResult ReportSearch2(string strKeyWordYear, string strKeyWordSemeter, string strKeyWordSubject, int intPageIndex = 0)
        {
            try
            {
                //Lấy cấu hình số dòng trên trang
                //int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTMLManageReport = new StringBuilder();

                DataTable dtbManageReport = new ReportDAO().SearchReport2(strKeyWordYear, strKeyWordSemeter,strKeyWordSubject, intPageIndex, intVisiblePages);

                if (dtbManageReport != null && dtbManageReport.Rows.Count > 0)
                {
                    strHTMLManageReport = GenHTMLReport(dtbManageReport);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtbManageReport.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTMLManageReport.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTMLManageReport),
                    totalpages = intTotalPages,
                    totalrows = intTotalRows
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        private StringBuilder GenHTMLReport(DataTable dtbPermission)
        {
            int intCountRow = dtbPermission.Rows.Count;
            StringBuilder strHTMLEmloyeeRoles = new StringBuilder();

            for (int i = 0; i < intCountRow; i++)
            {

                strHTMLEmloyeeRoles.Append("<tr>");
                strHTMLEmloyeeRoles.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][0]);
                strHTMLEmloyeeRoles.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][1]);
                strHTMLEmloyeeRoles.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][2]);
                strHTMLEmloyeeRoles.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][3]);
                strHTMLEmloyeeRoles.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][4]);
                strHTMLEmloyeeRoles.Append("</tr>");
            }

            return strHTMLEmloyeeRoles;
        }

        [HttpGet]
        public ActionResult ExportExcel(string strKeyWordYear, string strKeyWordSemeter)
        {
            System.IO.Stream spreadsheetStream = new System.IO.MemoryStream();
            var wb = new XLWorkbook();
            var ws = wb.Worksheets.Add("tổng kết học kỳ");
            DataTable dtbManageReport = new ReportDAO().SearchReportFull(strKeyWordYear, strKeyWordSemeter);
            // From a DataTable
            ws.Cell("B4").Value = "BÁO CÁO TỔNG KẾT HỌC KỲ";
            ws.Cell("B4").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            ws.Cell("B4").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            ws.Cell("B4").Style.Font.Bold = true;
            ws.Cell("B4").Style.Font.FontSize = 20;

            ws.Range("B4:F5").Merge();
            ws.Cell(10, 2).InsertTable(dtbManageReport.AsEnumerable());

            ws.Columns().AdjustToContents();
            wb.SaveAs(spreadsheetStream);
            spreadsheetStream.Position = 0;

            return new FileStreamResult(spreadsheetStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") { FileDownloadName = "bao-cao-tong-ket-hoc-ky.xlsx" };
        }

        [HttpGet]
        public ActionResult PrintReport(string strKeyWordYear, string strKeyWordSemeter)
        {
            //Gen HTML
            DataTable dtbManageReport = new ReportDAO().SearchReport(strKeyWordYear, strKeyWordSemeter, -1, -1);
            //DataTable dtbManageReport = new StudentsDAO().GetStudentsList(null, -1, -1);
            StringBuilder strHTMLManageReport = GenHTMLReportPrint(dtbManageReport);
            ViewBag.strHTMLManageReport = Convert.ToString(strHTMLManageReport);
            return View();
        }

        private StringBuilder GenHTMLReportPrint(DataTable dtbPermission)
        {
            int intNumRow = dtbPermission.Rows.Count;
            StringBuilder strHTMLReport = new StringBuilder();
            int intNumRowPerPage = 30;
            int intCountRow = 1;

            strHTMLReport.Append(@"<table width='100%' class='table table-striped table-bordered table-hover' id='tblReport'>
                                                    <thead>
                                                        <tr>
                                                            <th style='width:3%;'>STT</th>
                                                            <th style='width:5%;'>Lớp</th>
                                                            <th style='width:3%;'>Sĩ Số</th>
                                                            <th style='width:3%;'>Số lượng đạt</th>
                                                            <th style='width:3%;'>Tỷ lệ</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>");

            for (int i = 0; i < intNumRow; i++)
            {
//                if (intCountRow > intNumRowPerPage || i == 0)
//                {
//                    if (i != 0)
//                    {
//                        strHTMLReport.Append("</tbody></table>");
//                        strHTMLReport.Append("<div style='page-break-after:always'></div>");
//                    }
//                    strHTMLReport.Append(@"<table width='100%' class='table table-striped table-bordered table-hover' id='tblReport'>
//                                                    <thead>
//                                                        <tr>
//                                                            <th style='width:3%;'>STT</th>
//                                                            <th style='width:5%;'>Lớp</th>
//                                                            <th style='width:3%;'>Sĩ Số</th>
//                                                            <th style='width:3%;'>Số lượng đạt</th>
//                                                            <th style='width:3%;'>Tỷ lệ</th>
//                                                        </tr>
//                                                    </thead>
//                                                    <tbody>");
//                }

                strHTMLReport.Append("<tr>");
                strHTMLReport.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][0]);
                strHTMLReport.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][1]);
                strHTMLReport.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][2]);
                strHTMLReport.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][3]);
                strHTMLReport.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i][4]);
                strHTMLReport.Append("</tr>");

                intCountRow = intCountRow + 1;
            }

            strHTMLReport.Append("</tbody></table>");

            return strHTMLReport;
        }

        #endregion

        #region ChangePass
        public ActionResult ChangePass()
        {
            ViewBag.Message = "Trang thay đổi mật khẩu";
            if (Session["Username"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassProgress(string strnewpass)
        {

            try
            {
                bool bChangePass = new ChangPassDAO().ChangePass((string)Session["Username"], strnewpass);
                return Json(new { iserror = false, successchange = bChangePass });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        [HttpPost]
        public ActionResult CheckPass(string stroldpass)
        {
            UserDTO user = new UserDTO();
            user.Username = (string)Session["Username"];
            user.Password = stroldpass;
            try
            {
                LoginDAO DLogin = new LoginDAO();
                string scheckPass = DLogin.loginCheck(user);
                if (scheckPass != null & scheckPass != "")
                {
                    return Json(new { iserror = false, chkUser = true });
                }
                return Json(new { iserror = false, chkUser = false });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region ResetPass
        public ActionResult ResetPass()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DefaultPass(string strUsername)
        {
            try
            {
                //Lấy email của tài khoản
                EmployeesDTO objEmployees = new EmployeesDAO().GetEmployeesByID(strUsername);

                if (objEmployees != null)
                {
                    //Phát sinh mật khẩu mới ngẫu nhiên
                    string strDefaultPass = DataConnect.RandomString(9);

                    //Thay đổi mật khẩu mặc định
                    bool bolChangePass = new ChangPassDAO().ChangePass(strUsername, strDefaultPass);
                    bool bolSendMail;
                    if (bolChangePass == true)
                    {
                        //Gửi mail cung cấp mật khẩu mới cho người dùng
                        string strSubject = "Hệ thống quản lý học sinh - Cung cấp mật khẩu mặt định";
                        string strBody = "Mật khẩu của tài khoản " + strUsername + " đã được hệ thống cập nhật thành: " + strDefaultPass;
                        bolSendMail = new DataConnect().SendMail(objEmployees.strEmail, strSubject, strBody);

                        if (bolSendMail == true)
                            return Json(new { iserror = false, message = "" });
                    }
                }
                else
                    return Json(new { iserror = true, message = "Tên đăng nhập không tồn tại!" });

                return Json(new { iserror = false, message = "" });
            }
            catch
            {
                return Json(new { iserror = true, message = "" });
            }
        }
        #endregion

        //[HttpPost]
        //public ActionResult GetKeyPermisson()
        //{
        //    LoginDAO DLogin = new LoginDAO();
        //    string json = JsonConvert.SerializeObject(DLogin.GetKeyPermission((string)Session["Username"]));
        //    return new ContentResult { Content = json, ContentType = "application/json" };
        //}
    }
}