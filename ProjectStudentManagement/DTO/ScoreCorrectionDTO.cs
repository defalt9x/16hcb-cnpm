﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ScoreCorrectionDTO
    {
        public string ScoreCorrectionID { get; set; }
        public string ScoreID { get; set; }
        public string UserCreateID { get; set; }
        public string IsAccepted { get; set; }
        public string UserDecideID { get; set; }
        public DateTime? InsertedDatetime { get; set; }
        public DateTime? LastModified { get; set; }
        
        // more
        public string UserCreateFullName { get; set; }
        public string UserDecideFullName { get; set; }
        public string StudentID { get; set; }
        public string Year { get; set; }
        public string Semester { get; set; }
        public string SubjectID { get; set; }

        public List<ScoreCorrectionDetailDTO> lstScoreCorrectionDetail { get; set; }
    }
}
