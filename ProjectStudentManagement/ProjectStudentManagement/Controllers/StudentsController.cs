﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectStudentManagement.Controllers
{
    public class StudentsController : Controller
    {
        // GET: Students
        public ActionResult Index()
        {
            //Kiểm tra xem người dùng có được xem(truy cập vào chức năng)
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLHS_V") == false)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra xem người dùng có được phép insert
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLHS_I") == false)
            {
                ViewBag.InsertPermission = false;
            }
            else
            {
                ViewBag.InsertPermission = true;
            }
         
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            return View();
        }
        public ActionResult GetStudentsList(string strKeyWord, int intPageIndex = 0)
        {
            try
            {
                //Lấy cấu hình số dòng trên trang
                //int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTML = new StringBuilder();

                DataTable dtb = new StudentsDAO().GetStudentsList(strKeyWord, intPageIndex, intVisiblePages);

                if (dtb != null && dtb.Rows.Count > 0)
                {
                    strHTML = GenHTMLStudents(dtb);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtb.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTML.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }
                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTML),
                    totalpages = intTotalPages,
                    totalrows = intTotalRows
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }
        private StringBuilder GenHTMLStudents(DataTable dtb)
        {
            int intCountRow = dtb.Rows.Count;
            StringBuilder strbuilder = new StringBuilder();
            bool bPerDelete = true;
            //Kiểm tra xem người dùng có được phép delete
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLHS_D") == false)
            {
                bPerDelete  = false;
            }
            
            for (int i = 0; i < intCountRow; i++)
            {
                string StudentID = Convert.ToString(dtb.Rows[i]["StudentID"]).Trim();
                strbuilder.Append("<tr>");
                strbuilder.AppendFormat("<td>{0}</td>", dtb.Rows[i]["STT"]);
                strbuilder.AppendFormat("<td>{0}</td>", dtb.Rows[i]["StudentID"]);
                strbuilder.AppendFormat("<td>{0}</td>", dtb.Rows[i]["LastName"]);
                strbuilder.AppendFormat("<td>{0}</td>", dtb.Rows[i]["FirstName"]);
                strbuilder.AppendFormat("<td data-studentid='{0}'>", StudentID);
                strbuilder.AppendFormat("<span class='glyphicon glyphicon-pencil table-icon' data-name='Detail'></span>");
                if (bPerDelete == true)
                {
                    strbuilder.AppendFormat("<span class='glyphicon glyphicon-trash table-icon' data-name='Delete'></span>");

                }
                strbuilder.AppendFormat("</td>");
                strbuilder.Append("</tr>");
            }

            return strbuilder;
        }
        public ActionResult StudentsDetails(string strStudentID)
        {
            //Kiểm tra xem người dùng có được phép update
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLHS_U") == false)
            {
                ViewBag.EditPermission = false;
            }
            else
            {
                ViewBag.EditPermission = true;
            }
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            StudentsDTO student = new StudentsDAO().GetStudentByID(strStudentID);
            var lstFamilyMember = new List<FamilyMemberDTO>();
            lstFamilyMember = new StudentsDAO().GetFamilyMemberListByID(strStudentID);
            student.lstFamilyMember = lstFamilyMember;
            Session["StudentDetailInfo"] = null;
            Session.Add("StudentDetailInfo", student);
            return View(student);
        }
        public ActionResult StudentsInsert()
        {
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
            DataTable dtb = new SystemSettingsDAO().SearchSystemSettings();
            if (dtb != null && dtb.Rows.Count != 0)
            {
                ViewBag.intMinimumAged = dtb.Rows[0]["MinimumAged"].ToString();
                ViewBag.intMaximumAged = dtb.Rows[0]["MaximumAged"].ToString();

            }
            else
            {
                ViewBag.intMinimumAged = "15";
                ViewBag.intMaximumAged = "20";
            }
            return View();
        }
        public ActionResult StudentsUpdate()
        {
            StudentsDTO student = new StudentsDTO();
            if (Session["StudentDetailInfo"] != null)
            {
                student = (StudentsDTO)Session["StudentDetailInfo"];
            }
            DataTable dtb = new SystemSettingsDAO().SearchSystemSettings();
            if (dtb != null && dtb.Rows.Count != 0)
            {
                ViewBag.intMinimumAged = dtb.Rows[0]["MinimumAged"].ToString();
                ViewBag.intMaximumAged = dtb.Rows[0]["MaximumAged"].ToString();

            }
            else
            {
                ViewBag.intMinimumAged = "15";
                ViewBag.intMaximumAged = "20";
            }
            return View(student);
        } 
        [HttpPost]
        public JsonResult StudentsInsert(FormCollection frm)
        {
            try
            {
                DateTime bod = new DateTime();
                StudentsDTO studentDTO = new StudentsDTO();
                studentDTO.FirstName = frm["txtFirstName"];
                studentDTO.LastName = frm["txtLastName"];
                studentDTO.Address = frm["txtAddress"];
                studentDTO.District = frm["txtDistrict"];
                studentDTO.Country = frm["txtCountry"];
                studentDTO.CityProvince = frm["txtCityProvince"];
                studentDTO.PostalCode = frm["txtPostalCode"];
                studentDTO.EmailAddress = frm["txtEmailAddress"];
                studentDTO.PhoneNumber = frm["txtPhoneNumber"];
                studentDTO.GraduationYear = String.Empty;
                DateTime.TryParse(frm["txtBirthday"], out bod);
                studentDTO.Birthday = bod;
                studentDTO.IsDeleted = false;
                studentDTO.Gender = frm["rdoGender"];
                studentDTO.Grade = frm["txtGrade"];
                studentDTO.YearOfAdmission = DateTime.Now.Year.ToString();
                List<FamilyMemberDTO> lstSubjectExemptRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FamilyMemberDTO>>(frm["hdListFamilyMember"]);

                bool bolSuccess = new StudentsDAO().StudentsInsert(studentDTO, lstSubjectExemptRequest);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception ex)
            {
                return Json(new { iserror = true });
            }
        }
        [HttpPost]
        public JsonResult StudentsUpdate(FormCollection frm)
        {
            try
            {
                DateTime bod = new DateTime();
                StudentsDTO studentDTO = new StudentsDTO();
                studentDTO.StudentID = frm["txtStudentID"];
                studentDTO.FirstName = frm["txtFirstName"];
                studentDTO.LastName = frm["txtLastName"];
                studentDTO.Address = frm["txtAddress"];
                studentDTO.District = frm["txtDistrict"];
                studentDTO.Country = frm["txtCountry"];
                studentDTO.CityProvince = frm["txtCityProvince"];
                studentDTO.PostalCode = frm["txtPostalCode"];
                studentDTO.EmailAddress = frm["txtEmailAddress"];
                studentDTO.PhoneNumber = frm["txtPhoneNumber"];
                studentDTO.GraduationYear = frm["txtGraduationYear"];
                DateTime.TryParse(frm["txtBirthday"], out bod);
                studentDTO.Birthday = bod;
                studentDTO.IsDeleted = false;
                studentDTO.Gender = frm["rdoGender"];
                studentDTO.Grade = frm["txtGrade"];

                List<FamilyMemberDTO> lstSubjectExemptRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FamilyMemberDTO>>(frm["hdListFamilyMember"]);

                bool bolSuccess = new StudentsDAO().StudentsUpdate(studentDTO, lstSubjectExemptRequest);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception ex)
            {
                return Json(new { iserror = true });
            }
        }
        [HttpPost]
        public JsonResult StudentsDelete(string strStudentID)
        {
            try
            {
                string strMessageError = string.Empty;
                bool bolSuccess = new StudentsDAO().StudentsDelete(strStudentID);

                if (bolSuccess == false)
                    return Json(new { iserror = true});

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true, message = "" });
            }
        }
    }

}