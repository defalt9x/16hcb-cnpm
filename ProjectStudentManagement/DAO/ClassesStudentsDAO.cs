﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ClassesStudentsDAO
    {
        public DataTable GetStudentListByClassID(string Year, string ClassID, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                 "@PageSize", intPageSize
                ,"@PageIndex", intPageIndex
                ,"@Year", Year
                ,"@ClassID", ClassID
            };
            return new DataConnect().callUSP("uspClassesStudents_GetStudentListByClassID", objKeyWords);

        }

        public DataTable GetStudentListByGrade(string sltGrade, string sltClassID, string strKeyWordYear)
        {
            object[] objKeyWords = new object[] {
                 "@Grade", sltGrade,
                 "@ClassID", sltClassID,
                 "@Year",strKeyWordYear
            };
            return new DataConnect().callUSP("uspClassesStudents_GetStudentListByGrade", objKeyWords);
        }
        public bool UpdateStudentToClass(List<ClassesStudentsDTO> lstStudents)
        {

            if (lstStudents != null && lstStudents.Count > 0)
            {
                for (int i = 0; i < lstStudents.Count; i++)
                {
                    object[] objKeyWords1 = new object[] {
                        "@ClassID", lstStudents[i].ClassID.Trim(),
                        "@StudentID", lstStudents[i].StudentID.Trim(),
                        "@Year", lstStudents[i].Year.Trim(),
                         };
                    bool rs = new DataConnect().uspExecuteNonQuery("uspClassesStudents_Insert", objKeyWords1);
                    if (rs == true)
                    {
                        object[] objKeyWords2 = new object[] {
                        "@StudentID", lstStudents[i].StudentID.Trim(),
                        "@Year", lstStudents[i].Year.Trim(),
                         };
                        DataTable dtb = new DataConnect().callUSP("uspScores_GetScoresByStudentID", objKeyWords2); ;

                        // Kiem Tra da ton trai trong score chua
                        if (dtb == null || dtb.Rows.Count == 0)
                        {
                            // INSERT INTO SCORE
                            object[] objKeyWords3 = new object[] {
                                "@StudentID", lstStudents[i].StudentID.Trim(),
                                "@Year", lstStudents[i].Year.Trim()
                            };
                            bool result = new DataConnect().uspExecuteNonQuery("uspScores_Insert", objKeyWords3);
                            if (!result)
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                for (int i = 0; i < lstStudents.Count; i++)
                {
                    object[] objKeyWords4 = new object[] {
                        "@StudentID", lstStudents[i].StudentID.Trim(),
                         };
                    bool rs = new DataConnect().uspExecuteNonQuery("uspStudents_UpdateClassStatus", objKeyWords4);
                    if (rs == false)
                    {
                        return false;
                    }
                }

                return true;
            }
            return false;
        }

        public bool DeleteStudentFromClass(string strSltYear, string strClassID, string strStudentID)
        {
          
            List<string> Lstusp_name = new List<string>() { "uspClassesStudents_DeleteStudentFromClass", "uspStudents_UpdateClassStatus_False" };
            List<object[]> LstobjKeyWords = new List<object[]>();

            //Tạo list keywords
            object[] objKeyWords1 = new object[] {
                        "@ClassID", strClassID.Trim(),
                        "@StudentID", strStudentID.Trim(),
                        "@Year", strSltYear.Trim(),
                         };
            LstobjKeyWords.Add(objKeyWords1);

            object[] objKeyWords2 = new object[] {
                    "@StudentID", strStudentID.Trim()
                };
            LstobjKeyWords.Add(objKeyWords2);

            return new DataConnect().exeUSP(Lstusp_name, LstobjKeyWords, false);
        }
    }
}
