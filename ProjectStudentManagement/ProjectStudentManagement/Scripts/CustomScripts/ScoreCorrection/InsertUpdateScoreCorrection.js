﻿///////////////////////////////////////////////////THÊM/////////////////////////////////////////////////
$(document).ready(function () {
    LoadDropdownlist();
    OnchangeDropdownlist('#ContentScore');
    if ($('#hdFormType').val() == "Insert") {
        //Khoi tao su kien sau khi insert
        $('#frmScoreCorrectionInsert').ajaxForm(function (data) {
            ajaxInsertSuccess(data);
        });
    } else {
        //Khoi tao su kien sau khi update
        $('#frmScoreCorrectionUpdate').ajaxForm(function (data) {
            ajaxUpdateSuccess(data);
        });
        InitAddScore();
        InitRemoveScore();
    }
});


// show data for modal score
function SearchScore() {
    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').val();
    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').val();
    param.strKeyWordSubject = $('#sltKeyWordSubject').val();
    param.strKeyWordStudentID = $('#txtKeyWordStudent').val();
    $.ajax({
        url: "/ScoreCorrection/SearchScore",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentModelScore').html(data.content);
                Paging(data.totalpages, data.totalrows, 1);
                InitAddScore();

                //Gán data để xóa có data load lại trang
                $('#txtKeyWord').attr('data-value', $('#txtKeyWord').val());
                $('#sltKeyWordYear').attr('data-value', $('#sltKeyWordYear').val());
                $('#sltKeyWordSemeter').attr('data-value', $('#sltKeyWordSemeter').val());
                $('#sltKeyWordSubject').attr('data-value', $('#sltKeyWordSubject').val());
                $('#txtKeyWordStudentID').attr('data-value', $('#txtKeyWordStudentID').val());
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
            
            hideLoading("btnSearch");
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            hideLoading("btnSearch");
        }
    });
}

function InsertScore() {
    SearchScore();
    $('#ModalScore').modal('toggle');
}

function SearchAjaxPaging() {
    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').attr('data-value');
    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').val();
    param.strKeyWordSubject = $('#sltKeyWordSubject').val();
    param.strKeyWordSubject = $('#sltKeyWordStudent').val();
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/ScoreCorrection/SearchScore",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentModelScore').html(data.content);
                InitAddScore();
                //Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()));
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}
// end show data for modal score
function InitAddScore() {
    $('span[data-name="btnAddScore"]').each(function () {
        $(this).click(function () {
            //Lấy thông tin
            var idscore = $(this).attr('data-idscore');
            var oral_old = $('#tr_' + idscore).find('td[data-name="oral_old"]').html();
            var fifteenminutes_old = $('#tr_' + idscore).find('td[data-name="fifteenminutes_old"]').html();
            var fortyfiveminutes_old = $('#tr_' + idscore).find('td[data-name="fortyfiveminutes_old"]').html();
            var Year = $('#tr_' + idscore).find('td[data-name="Year"]').html();
            var Semester = $('#tr_' + idscore).find('td[data-name="Semester"]').html();
            var SubjectID = $('#tr_' + idscore).find('td[data-name="SubjectID"]').html();
            var StudentID = $('#tr_' + idscore).find('td[data-name="StudentID"]').html();
            
            if (checkIDScoreMainTable(idscore) == true) {
                //Gen HTML để thể hiện ra bảng chính
                var strHTML = "";

                strHTML += "<tr id='tr_" + idscore + "'>";

                strHTML += "<td data-name='oral_old' style='vertical-align: middle'>" + oral_old + "</td>";
                strHTML += "<td data-name='fifteenminutes_old' style='vertical-align: middle'>" + fifteenminutes_old + "</td>";
                strHTML += "<td data-name='fortyfiveminutes_old' style='vertical-align: middle'>" + fortyfiveminutes_old + "</td>";

                strHTML += "<td>"
                    + "<input id='oral_new' data-name='oral_new' class='form-control'"
                    + " oninput='validity.valid||(value=value.slice(0, -1));' type='number' min='0' max='10' step='0.01' placeholder='0.00'>"
                    + "</td>";
                strHTML += "<td>" +
                    "<input id='fifteenminutes_new' data-name='fifteenminutes_new' class='form-control'"
                    + " oninput='validity.valid||(value=value.slice(0, -1));' type='number' min='0' max='10' step='0.01' placeholder='0.00'>"
                    +"</td>";
                strHTML += "<td>" +
                    "<input id='fortyfiveminutes_new' data-name='fortyfiveminutes_new' class='form-control'"
                    + " oninput='validity.valid||(value=value.slice(0, -1));' type='number' min='0' max='10' step='0.01' placeholder='0.00'>"
                    +"</td>";

                strHTML += "<td><span class='glyphicon glyphicon-remove table-icon' data-name='btnRemoveScore' data-idscore='" + idscore + "'></span></td>";
                strHTML += "</tr>";

                //Đưa tr vào bảng trên giao diện chính
                if ($('#ContentScore tr td').length == 1) { //Hiện không có nội dung
                    $('#ContentScore').empty();
                    $('#ContentScore').append(strHTML);

                    $('#sltKeyWordYear').val(Year);
                    $('#sltKeyWordSemeter').val(Semester);
                    $('#sltKeyWordSubject').val(SubjectID);
                    $('#txtKeyWordStudent').val(StudentID);

                    CloseInsertScore();
                } else {
                    $.notify("Chỉ được chọn 1 dữ liệu.", "warn");
                }
            } else {
                $.notify("Điểm đã tồn tại.", "warn");
            }
        });
    });
}

function checkIDScoreMainTable(idscore) {
    var check = true;
    $('#ContentScore tr').each(function () {
        var idscoretemp = $(this).find('td span[data-name="btnRemoveScore"]').attr('data-idscore');
        if (idscoretemp == idscore) {
            check = false;
        }
    });
    return check;
}

function InitRemoveScore() {
    $('#ContentScore tr').each(function () {
        $(this).find("td span[data-name='btnRemoveScore']").click(function () {
            $(this).parent().parent().remove();

            //Nếu không còn dòng dữ liệu nào
            if ($('#ContentScore tr').length == 0)
                $('#ContentScore').append('<tr><td colspan="100%" class="table-content-null">Hiện không có nội dung</td></tr>');
        });
    });
}

function CloseInsertScore() {
    InitRemoveScore();
    $('#ModalScore').modal('toggle');
}

function BackManage() {
    setTimeout(function () { window.location.href = "/quan-ly-phieu-hieu-chinh-diem/" }, 500);
}

function InsertScoreCorrection() {
    var arrScore = [];
    var idscore = null;
    var cnt = 0;
    showLoading("btnInsert");
    //Lấy điểm
    if ($('#ContentScore tr td').length > 1) {
        $('#ContentScore tr').each(function () {
            idscore = $(this).find('td span[data-name="btnRemoveScore"]').attr('data-idscore');

            var objScore0 = {
                ScoreType: 0,
                ScoreOld: $(this).find('td[data-name="oral_old"]').html(),
                //ScoreNew: $(this).find('td[data-name="oral_new"]').html()
                ScoreNew: $('#oral_new').val()
            };
            cnt += objScore0['ScoreNew'] != null && objScore0['ScoreNew'] != '';
            arrScore.push(objScore0);
            var objScore1 = {
                ScoreType: 1,
                ScoreOld: $(this).find('td[data-name="fifteenminutes_old"]').html(),
                //ScoreNew: $(this).find('td[data-name="fifteenminutes_new"]').html()
                ScoreNew: $('#fifteenminutes_new').val()
            };
            cnt += objScore1['ScoreNew'] != null && objScore1['ScoreNew'] != '';
            arrScore.push(objScore1);
            var objScore2 = {
                ScoreType: 2,
                ScoreOld: $(this).find('td[data-name="fortyfiveminutes_old"]').html(),
                //ScoreNew: $(this).find('td[data-name="fortyfiveminutes_new"]').html()
                ScoreNew: $('#fortyfiveminutes_new').val()
            };
            cnt += objScore2['ScoreNew'] != null && objScore2['ScoreNew'] != '';
            arrScore.push(objScore2);
        });
        if (cnt == 0) {
            hideLoading("btnInsert");
            $.notify("Vui lòng nhập ít nhất 1 điểm mới", "info");
            $("#oral_new").focus();
            return false;
        }

        $('#hdListScore').val(JSON.stringify(arrScore));
        $('#hdScoreID').val(idscore);
    }

    if (idscore == null) {
        hideLoading("btnInsert");
        $.notify("Vui lòng chọn điểm và nhập điểm mới", "info");
        $("#btnChoose").focus();
        return false;
    }

    $('#frmScoreCorrectionInsert').submit();
    return false;
}

//Ham thuc hien sau khi insert
function ajaxInsertSuccess(data) {
    hideLoading("btnInsert");

    if (data.iserror == false) {
        $.notify("Lập phiếu thành công.", "success");
        setTimeout(function () {
            window.location.href = "/quan-ly-phieu-hieu-chinh-diem";
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}

///////////////////////////////////////////////////CẬP NHẬT/////////////////////////////////////////////////
function UpdateScoreCorrection() {
    var arrScore = [];
    showLoading("btnUpdate");

    //Lấy danh sách quyền
    if ($('#ContentScore tr td').length > 1) {
        $('#ContentScore tr').each(function () {
            var idkey = $(this).find('td[data-name="idkey"]').html();

            var objScore = {
                IDKey: idkey
            };

            arrScore.push(objScore);
        });
        $('#hdListScore').val(JSON.stringify(arrScore));
    }
    debugger;
    $('#frmScoreCorrectionUpdate').submit();
    return false;
}

//Ham thuc hien sau khi update
function ajaxUpdateSuccess(data) {
    hideLoading("btnUpdate");

    if (data.iserror == false) {
        $.notify("Cập nhật nhóm người dùng thành công.", "success");
        setTimeout(function () {
            var strRoleID = $('#hdRoleID').val();
            setTimeout(function () { window.location.href = "/quan-ly-nhom-nguoi-dung/chi-tiet/" + strRoleID }, 500);
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}