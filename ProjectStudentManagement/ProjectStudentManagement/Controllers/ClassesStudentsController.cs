﻿using DAO;
using DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectStudentManagement.Controllers
{
    public class ClassesStudentsController : Controller
    {
        // GET: Classes
        public ActionResult Index()
        {
            //Kiểm tra xem người dùng có được xem(truy cập vào chức năng)
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLLH_V") == false)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra xem người dùng có được phép thêm
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLLH_I") == false)
            {
                ViewBag.InsertPermission = false;
            }
            else
            {
                ViewBag.InsertPermission = true;
            }
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
            DataTable dtb = new SystemSettingsDAO().SearchSystemSettings();
            if(dtb != null && dtb.Rows.Count != 0 )
            {
                ViewBag.intMaximumStudentNumber = dtb.Rows[0]["MaxinumStudentNumber"].ToString();
            }
            else
            {
                ViewBag.intMaximumStudentNumber = "40";
            }
            return View();
        }
        [HttpPost]
        public ActionResult GetClassList()
        {
            try
            {
                DataTable dtb = new ClassesDAO().GetClassList();
                string ClassList = JsonConvert.SerializeObject(dtb);
                return Json(new { iserror = false, classlistcontent = ClassList });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }
        [HttpPost]
        public ActionResult GetStudentsByClassID(string strKeyWordYear, string sltKeyWordClass, int intPageIndex = 0)
        {
            try
            {
                //Lấy cấu hình số dòng trên trang
                //int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTML = new StringBuilder();
                DataTable dtb = new ClassesStudentsDAO().GetStudentListByClassID(strKeyWordYear, sltKeyWordClass, intPageIndex, intVisiblePages);

                if (dtb != null && dtb.Rows.Count > 0)
                {
                    strHTML = GenHTMLStudents(dtb);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtb.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTML.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTML),
                    totalpages = intTotalPages,
                    totalrows = intTotalRows
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }
        public ActionResult GetStudentListByGrade(string sltGrade,string sltClassID,string strKeyWordYear)
        {
            string StudentNumber = "";
            int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
            //Gen HTML
            StringBuilder strHTML = new StringBuilder();
            DataTable dtb = new ClassesStudentsDAO().GetStudentListByGrade(sltGrade, sltClassID, strKeyWordYear);
            if (dtb != null && dtb.Rows.Count > 0)
            {
                for( int i = 0; i< dtb.Rows.Count; i++)
                {
                    StringBuilder strAddress = new StringBuilder();
                    DateTime dob = new DateTime();
                    string studentID = dtb.Rows[i]["StudentID"].ToString().Trim();
                    StudentNumber = dtb.Rows[i]["StudentNumber"].ToString().Trim();
                    DateTime.TryParse(dtb.Rows[i]["Birthday"].ToString(), out dob);
                    strHTML.AppendFormat("<tr id='tr_{0}'>", studentID);

                    strHTML.AppendFormat("<td class='col-xs-1'>{0}</td>", i + 1);
                    strHTML.AppendFormat("<td class='col-xs-2' data-name='idkey'>{0}</td>", studentID);
                    strHTML.AppendFormat("<td class='col-xs-3'>{0} {1}</td>", dtb.Rows[i]["LastName"], dtb.Rows[i]["FirstName"]);
                    strHTML.AppendFormat("<td class='col-xs-1' data-name='birthday' >{0}</td>", dob.ToString("yyyy"));
                    //Hiển thị giới tính
                    if (dtb.Rows[i]["Gender"].ToString() == "1")
                    {
                        strHTML.AppendFormat("<td class='col-xs-1'  data-name='gender'>{0}</td>", "Nam");
                    }
                    if (dtb.Rows[i]["Gender"].ToString() == "0")
                    {
                        strHTML.AppendFormat("<td class='col-xs-1'  data-name='gender'>{0}</td>", "Nữ");
                    }
                    if (String.IsNullOrEmpty(dtb.Rows[i]["Gender"].ToString().Trim()))
                    {
                        strHTML.AppendFormat("<td class='col-xs-1' data-name='gender'>{0}</td>","");
                    }
                    // Nối CHUỔI ĐỊA CHỈ
                    strAddress.Append(dtb.Rows[i]["Address"].ToString());
                    strAddress.Append(" ");
                    strAddress.Append(dtb.Rows[i]["District"].ToString());
                    strAddress.Append(" ");
                    strAddress.Append(dtb.Rows[i]["CityorProvince"].ToString());
                    strAddress.Append(" ");
                    strAddress.Append(dtb.Rows[i]["Country"].ToString());
                    strAddress.Append(" ");
                    strAddress.Append(dtb.Rows[i]["PostalCode"].ToString());
                    strHTML.AppendFormat("<td data-name='address'  class='col-xs-3' ><div class='truncate' title='{0}'>{0}</div></td>", strAddress);
                    strHTML.AppendFormat("<td class='col-xs-1' >");
                    strHTML.AppendFormat("<label class='switch'>");
                    strHTML.AppendFormat("<input data-name='btnAddStudent' type='checkbox' data-idkey='{0}'>", studentID);
                    strHTML.AppendFormat("<div class='slider round'></div></label>");
                    strHTML.AppendFormat("</td>");
                    strHTML.Append("</tr>");
            
                }
            }
            else
            {
                strHTML.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
            }

            return Json(new
            {
                iserror = false,
                content = Convert.ToString(strHTML),
                studentnumber = StudentNumber
            });
        }
        
        private StringBuilder GenHTMLStudents(DataTable dtb)
        {
            bool bPerDelete = true;
            //Kiểm tra xem người dùng có được phép thêm
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLLH_D") == false)
            {
                bPerDelete = false;
            }

            int intCountRow = dtb.Rows.Count;
            StringBuilder strbuilder = new StringBuilder();
            for (int i = 0; i < intCountRow; i++)
            {
                StringBuilder strAddress = new StringBuilder();
                string StudentID = Convert.ToString(dtb.Rows[i]["StudentID"]).Trim();
                DateTime dob = new DateTime();
                DateTime.TryParse(dtb.Rows[i]["Birthday"].ToString(), out dob);

                strbuilder.Append("<tr>");
                strbuilder.AppendFormat("<td data-name='idkey'>{0}</td>", dtb.Rows[i]["StudentID"]);
                strbuilder.AppendFormat("<td>{0}</td>", dtb.Rows[i]["LastName"]);
                strbuilder.AppendFormat("<td>{0}</td>", dtb.Rows[i]["FirstName"]);

                //Hiển thị giới tính
                if (dtb.Rows[i]["Gender"].ToString() == "1")
                {
                    strbuilder.AppendFormat("<td>{0}</td>", "Nam");
                }
                if (dtb.Rows[i]["Gender"].ToString() == "0")
                {
                    strbuilder.AppendFormat("<td>{0}</td>", "Nữ");
                }
                if (dtb.Rows[i]["Gender"].ToString().Trim() == "")
                {
                    strbuilder.AppendFormat("<td>{0}</td>", "");
                }

                strbuilder.AppendFormat("<td>{0}</td>", dob.ToString("yyyy"));
                // Nối CHUỔI ĐỊA CHỈ
                strAddress.Append(dtb.Rows[i]["Address"].ToString());
                strAddress.Append(" ");
                strAddress.Append(dtb.Rows[i]["District"].ToString());
                strAddress.Append(" ");
                strAddress.Append(dtb.Rows[i]["CityorProvince"].ToString());
                strAddress.Append(" ");
                strAddress.Append(dtb.Rows[i]["Country"].ToString());
                strAddress.Append(" ");
                strAddress.Append(dtb.Rows[i]["PostalCode"].ToString());

                strbuilder.AppendFormat("<td>{0}</td>", strAddress);
                strbuilder.AppendFormat("<td data-studentid='{0}'>", StudentID);
                strbuilder.AppendFormat("<span class='glyphicon glyphicon-pencil table-icon' data-name='Detail'></span>");
                if(bPerDelete)
                {
                    strbuilder.AppendFormat("<span class='glyphicon glyphicon-trash table-icon' data-name='btnRemoveStudent' data-idkey='{0}'></span>", StudentID);
                }
                strbuilder.AppendFormat("</td>");
                strbuilder.Append("</tr>");
            }
           
            return strbuilder;
        }
        [HttpPost]
        public ActionResult InsertStudentToClass(FormCollection frm)
        {
            try
            {
                
                List<ClassesStudentsDTO> lstStudents = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClassesStudentsDTO>>(frm["hdStudentsList"]);

                bool bolSuccess = new ClassesStudentsDAO().UpdateStudentToClass(lstStudents);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception ex)
            {
                return Json(new { iserror = true });
            }
        }
        [HttpPost]
        public ActionResult DeleteStudentFromClass(string strSltYear, string strClassID, string strStudentID)
        {
            try
            {
                string strMessageError = string.Empty;
                bool bolSuccess = new ClassesStudentsDAO().DeleteStudentFromClass(strSltYear, strClassID,strStudentID);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true, message = "" });
            }
        }
    }
}