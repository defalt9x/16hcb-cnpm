﻿using Microsoft.Owin;
using Owin;
using System.Configuration;

[assembly: OwinStartupAttribute(typeof(ProjectStudentManagement.Startup))]
namespace ProjectStudentManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            //Configuration myConfiguration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            //myConfiguration.AppSettings.Settings.Add("RowPerPage","15");
            //myConfiguration.Save();
            ConfigureAuth(app);

        }
    }
}
