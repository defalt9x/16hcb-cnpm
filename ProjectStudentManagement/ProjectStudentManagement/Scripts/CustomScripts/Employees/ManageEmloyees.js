﻿var IsLoadServer = false;

$(document).ready(function () {
    $("#menu-toggle").click(function () {
        $("#sidebar-hidden-div").toggle();
        if ($("#sidebar-hidden-div").css('display') == "none")
            $("#page-wrapper").prop('id', 'page-wrapper-disable');
        else
            $("#page-wrapper-disable").prop('id', 'page-wrapper');
    });
    //$("#menu-toggle2").click(function (e) {
    //    $("#sidebar-hidden-div").show();
    //    $("#page-wrapper-disable").prop('id', 'page-wrapper');
    //});
});

$("#btnSearch").click(function () {
    IsLoadServer = false;
    SearchEmployees();
});

function SearchEmployees() {
    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').val();

    $.ajax({
        url: "/Employees/SearchEmployees",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
            $("#btnSearch").button('loading');
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentEmployees').html(data.content);
                Paging(data.totalpages, data.totalrows, 1);
                InitProcessInTable();

                //Gán data để xóa có data load lại trang
                $('#txtKeyWord').attr('data-value', $('#txtKeyWord').val());
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

            $("#btnSearch").button('reset');
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            $("#btnSearch").button('reset');
        }
    });
}

function Paging(totalPages, totalRows, startpage) {
    if (totalRows > parseInt(visiblepages)) {
        $('#paging').remove();
        $('#divPagingIndex').html("<ul id='paging' class='pagination'></ul>");
        $('#paging').twbsPagination({
            startPage: startpage,
            totalPages: totalPages,
            visiblePages: visiblepages,
            onPageClick: function (event, page) {
                //Ghi nhận chỉ số trang hiện tại
                $('#hdCurrentPage').val(page - 1); //Vì store bắt đầu từ 0 nên -1

                //Load data ứng với chỉ số page từ server
                if (IsLoadServer == true)
                    SearchAjaxPaging();

                //Thể hiện chỉ số record đang hiển thị trên giao diện
                var visiblePages = parseInt(visiblepages);
                var ToIndextemp = page * visiblePages;
                var FromIndex = (page - 1) * visiblePages + 1;
                //Lấy chỉ số đến trang (Nếu không là trang cuối)
                if (ToIndextemp < totalRows)
                    ToIndex = ToIndextemp;
                else
                    ToIndex = totalRows;

                $('#page-from-index').text(FromIndex);
                $('#page-to-index').text(ToIndex);
                $('#total-record').text(totalRows);

                IsLoadServer = true;
            }
        });
        $('#divPaging').css('display', 'block');
    } else {
        $('#divPaging').css('display', 'none');
    }
}

function SearchAjaxPaging() {
    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').attr('data-value');
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/Employees/SearchEmployees",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentEmployees').html(data.content);
                //Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()));
                InitProcessInTable();
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

function InitProcessInTable() {
    $('#ContentEmployees tr').each(function () {
        $(this).find('td span[data-name="Detail"]').click(function () {
            var strEmployeeID = $(this).closest('td').attr('data-employeeid');

            setTimeout(function () { window.location.href = "/quan-ly-giao-vien/chi-tiet/" + strEmployeeID }, 500);
        });

        $(this).find('td span[data-name="Delete"]').click(function () {
            var strEmployeeID = $(this).closest('td').attr('data-employeeid');
            var strMessage = "Bạn có muốn xóa nhân viên " + strEmployeeID + "?"

            $('#divMessage').html(strMessage);
            $('#divMessage').attr('data-id', strEmployeeID);
            $('#ModalConfirmDelete').modal('toggle');
        });
    });
}

function InsertEmployees() {
    setTimeout(function () { window.location.href = "/quan-ly-giao-vien/them/" }, 500);
}

function DeleleEmployees() {
    $('#hdCurRowBeforeDelOnScreen').val($('#ContentEmployees tr').length);
    param = {};
    param.strEmployeeID = $('#divMessage').attr('data-id');
    
    $.ajax({
        url: "/Employees/DeleteEmployees",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                SearchEmployeesAfterDelete(parseInt($('#hdCurRowBeforeDelOnScreen').val()) - 1);
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

function SearchEmployeesAfterDelete(CurRowAfterDelOnScreen) {
    if (CurRowAfterDelOnScreen == 0)
        $('#hdCurrentPage').val(parseInt($('#hdCurrentPage').val()) - 1);
    else
        $('#hdCurrentPage').val(parseInt($('#hdCurrentPage').val()));

    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').attr('data-value');
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/Employees/SearchEmployees",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentEmployees').html(data.content);
                Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()) + 1);
                InitProcessInTable();
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}