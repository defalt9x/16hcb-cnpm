﻿$(document).ready(function () {
    LoadDropdownlist();
});

function SearchScoreCorrection() {
    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').val();
    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').val();
    param.strKeyWordSubject = $('#sltKeyWordSubject').val();
    param.strKeyWordStudentID = $('#txtKeyWordStudentID').val();
    $.ajax({
        url: "/ScoreCorrection/SearchScoreCorrection",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
            $("#btnSearch").button('loading');
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentScoreCorrection').html(data.content);
                Paging(data.totalpages, data.totalrows, 1);
                InitProcessInTable();

                //Gán data để xóa có data load lại trang
                $('#txtKeyWord').attr('data-value', $('#txtKeyWord').val());
                $('#sltKeyWordYear').attr('data-value', $('#sltKeyWordYear').val());
                $('#sltKeyWordSemeter').attr('data-value', $('#sltKeyWordSemeter').val());
                $('#sltKeyWordSubject').attr('data-value', $('#sltKeyWordSubject').val());
                $('#txtKeyWordStudentID').attr('data-value', $('#txtKeyWordStudentID').val());
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

            $("#btnSearch").button('reset');
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            $("#btnSearch").button('reset');
        }
    });
}

function SearchAjaxPaging() {
    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').attr('data-value');
    param.strKeyWordYear = $('#sltKeyWordYear').val();
    param.strKeyWordSemeter = $('#sltKeyWordSemeter').val();
    param.strKeyWordSubject = $('#sltKeyWordSubject').val();
    param.strKeyWordStudentID = $('#txtKeyWordStudentID').val();

    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/ScoreCorrection/SearchScoreCorrection",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentScoreCorrection').html(data.content);
                //Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()));
                InitProcessInTable();
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

function InitProcessInTable() {
    $('#ContentScoreCorrection tr').each(function () {
        $(this).find('td span[data-name="Detail"]').click(function () {
            var id = $(this).closest('td').attr('data-scorecorrectionid');

            setTimeout(function () { window.location.href = "/quan-ly-phieu-hieu-chinh-diem/chi-tiet/" + id }, 500);
        });

        //$(this).find('td span[data-name="Delete"]').click(function () {
        //    var strRoleID = $(this).closest('td').attr('data-roleid');
        //    var strMessage = "Bạn có muốn xóa nhóm người dùng " + strRoleID + "?"

        //    $('#divMessage').html(strMessage);
        //    $('#ModalConfirmDelete').modal('toggle');
        //    $('#divMessage').attr('data-id', strRoleID);
        //});
    });
}

function InsertScoreCorrection() {
    setTimeout(function () { window.location.href = "/quan-ly-phieu-hieu-chinh-diem/lap-phieu/" }, 500);
}
