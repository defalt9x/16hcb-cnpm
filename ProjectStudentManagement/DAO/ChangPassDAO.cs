﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ChangPassDAO
    {

        public bool ChangePass(string usename,string strnewpass)
        {

            object[] objKeyWords = new[] {
                "@USERNAME", usename,
                "@PWS", DataConnect.MD5Hash(strnewpass)
            };

            DataTable dtbPassChange = new DataConnect().callUSP("usp_changepass", objKeyWords);
            //Nếu tồn tại dòng dữ liệu chứng tỏ đăng nhập thành công
            if (dtbPassChange != null && dtbPassChange.Rows.Count != 0)
            {
                return true;
            }
            return false;
        }
    }
}
