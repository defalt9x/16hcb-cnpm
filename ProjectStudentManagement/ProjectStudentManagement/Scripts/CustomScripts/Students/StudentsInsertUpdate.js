﻿///////////////////////////////////////////////////THÊM/////////////////////////////////////////////////
$(document).ready(function () {
    if ($('#hdFormType').val() == "Insert") {
        //Khoi tao su kien sau khi insert
        $('#frmStudentsInsert').ajaxForm(function (data) {
            ajaxInsertSuccess(data);
        });
    } else {
        //Khoi tao su kien sau khi update
        $('#frmStudentsUpdate').ajaxForm(function (data) {
            ajaxUpdateSuccess(data);
        });

    }
    InitAddFamilyMember();
    InitRemoveFamilyMember();

    $("#txtFamilyName").focusout(function () {
        $("#grpFamilyName").removeClass("has-error");
    });
    $("#txtFamilyPhoneNumber").focusout(function () {
        $("#grpFamilyPhoneNumber").removeClass("has-error");
    });
    $("#txtFamilyEmailAddress").focusout(function () {
        $("#grpFamilyEmailAddress").removeClass("has-error");
    });
    $("#sltRelationship").focusout(function () {
        $("#grpRelationship").removeClass("has-error");
    });
});


function Paging(totalPages, totalRows) {
    if (totalRows > parseInt(visiblePages)) {
        $('#paging').twbsPagination({
            totalPages: totalPages,
            visiblePages: visiblePages,
            onPageClick: function (event, page) {
                var ToIndextemp = page * visiblePages;
                var visiblePages = visiblePages;
                var FromIndex = (page - 1) * visiblePages + 1;
                //Lấy chỉ số đến trang (Nếu không là trang cuối)
                if (ToIndextemp < totalPages)
                    ToIndex = ToIndextemp;
                else
                    ToIndex = totalPages

                $('#page-from-index').text(FromIndex);
                $('#page-to-index').text(ToIndex);
            }
        });
        $('#divPaging').css('display', 'block');
    } else {
        $('#divPaging').css('display', 'none');
    }
}
function BackManage() {
    setTimeout(function () { window.location.href = "/quan-ly-hoc-sinh" }, 500);
}

function InsertStudents() {
    if (checkvalidate() == true)
    {
        showLoading("btnInsert");
        var arr = [];
        //Lấy danh sách quyền
        if ($('#ContentTableList tr td').length > 1) {
            $('#ContentTableList tr').each(function () {
                var idkey = $(this).find('td[data-name="STT"]').html();
                var txtFamilyName = $(this).find('td[data-name="txtFamilyName"]').html();
                var sltRelationship = $(this).find('td[data-name="txtRelationshipvalue"]').html();
                var txtFamilyPhoneNumber = $(this).find('td[data-name="txtFamilyPhoneNumber"]').html();
                var txtFamilyEmailAddress = $(this).find('td[data-name="txtFamilyEmailAddress"]').html();

                var objName = {
                    MemberID: idkey,
                    Name: txtFamilyName,
                    Relationship: sltRelationship,
                    PhoneNumber: txtFamilyPhoneNumber,
                    EmailAddress: txtFamilyEmailAddress
                };

                arr.push(objName);
            });
            $('#hdListFamilyMember').val(JSON.stringify(arr));
        }
        $('#frmStudentsInsert').submit();
        return true;

    }
    return false;
}
function checkvalidate()
{
    var firstname = $("#txtFirstName").val().trim();
    var lastname = $("#txtLastName").val().trim();
    var birthday = $("#txtBirthday").val().trim();
    var address = $("#txtAddress").val().trim();
    var district = $("#txtDistrict").val().trim();
    var city = $("#txtCityProvince").val().trim();
    var country = $("#txtCountry").val().trim();
    var postalcode = $("#txtPostalCode").val().trim();
    var phonenumber = $("#txtPhoneNumber").val().trim();
    var emailaddress = $("#txtEmailAddress").val().trim();
    var grade = $("#txtGrade").val().trim();
    if(firstname.trim() == "" )
    {
        $.notify("Vui lòng nhập Họ", "error");
        $("#txtFirstName").focus();
        return false;
    }
    if (lastname == "") {
        $.notify("Vui lòng nhập Tên", "error");
        $("#txtLastName").focus();
        return false;
    }
    if ($('#hdFormType').val() == "Insert") {

        if (isBirthDayValid(birthday) == false) {
            $.notify("Tuổi học sinh phải từ "+minimumAged+" đến "+ maximumAged+".","error");
            $("#txtBirthday").focus();
            return false;
        }
    }

    if ($('#hdFormType').val() == "Update") {
        var graduationyear = $("#txtGraduationYear").val().trim();
        var currentyear = new Date().getFullYear();
        if (graduationyear != ""){
            if (isNaN(graduationyear) == true) {
                $.notify("Năm tốt nghiệp phải là số", "error");
                $("#txtGraduationYear").focus();
                return false;
            }
            if (graduationyear > currentyear || graduationyear < 1900) {
                $.notify("Năm tốt nghiệp phải nhỏ hơn năm hiện tại", "error");
                $("#txtGraduationYear").focus();
                return false;
            }
        }
        var yearOfAdmission = $("#txtYearOfAdmission").val().trim();
        if (yearOfAdmission == "") {
            $.notify("Vui lòng nhập năm Nhập học", "error");
            $("#txtYearOfAdmission").focus();
            return false;
        }
        if (isNaN(yearOfAdmission) == true) {
            $.notify("Năm nhập học phải là số", "error");
            $("#txtYearOfAdmission").focus();
            return false;
        }
        if (yearOfAdmission > currentyear || yearOfAdmission < 1900) {
            $.notify("Năm nhập học phải nhỏ hơn hoặc bằng năm hiện tại", "error");
            $("#txtYearOfAdmission").focus();
            return false;
        }

    }
    if (grade == "") {
        $.notify("Vui lòng nhập trình độ", "error");
        $("#txtGrade").focus();
        return false;
    }
    if (isNaN(grade) == true) {
        $.notify("Trình độ phải là số", "error");
        $("#txtGrade").focus();
        return false;
    }
    if (parseInt(grade) < 10 || parseInt(grade) > 12) {
        $.notify("Trình độ là từ 10 - 12", "error");
        $("#txtGrade").focus();
        return false;
    }
    if (address == "") {
        $.notify("Vui lòng nhập Địa chỉ", "error");
        $("#txtAddress").focus();
        return false;
    }
    if (district == "") {
        $.notify("Vui lòng nhập Quận huyện", "error");
        $("#txtDistrict").focus();
        return false;
    }
    if (city == "") {
        $.notify("Vui lòng nhập Tỉnh /Thành phố", "error");
        $("#txtCityProvince").focus();
        return false;
    }
    if (postalcode == "") {
        $.notify("Vui lòng nhập Mã bưu điện", "error");
        $("#txtPostalCode").focus();
        return false;
    }
    if (isNaN(postalcode) == true) {
        $.notify("Mã bưu điện phải là số", "error");
        $("#txtPostalCode").focus();
        return false;
    }
    if (phonenumber == "") {
        $.notify("Vui lòng nhập Số điện thoại", "error");
        $("#txtPhoneNumber").focus();
        return false;
    }
    if (isNaN(phonenumber) == true) {
        $.notify("Số điện thoại phải là số", "error");
        $("#txtPhoneNumber").focus();
        return false;
    }
    if (emailaddress == "") {
        $.notify("Vui lòng nhập Email", "error");
        $("#txtEmailAddress").focus();
        return false;
    }
    if (isEmail(emailaddress) == false) {
        $.notify("Email không đúng định dạng abc@domainName.com", "error");
        $("#txtFamilyEmailAddress").focus();
        return false;
    }
    var idkey = $("#ContentTableList").find("tr[data-name='trcontent']").length;
    if (idkey == 0)
    {
        $.notify("Vui lòng thêm thông tin nhân thân học sinh", "error");
        return false;
    }

    return true;

}
//Ham thuc hien sau khi insert
function ajaxInsertSuccess(data) {
    hideLoading("btnInsert");

    if (data.iserror == false) {
        $.notify("Thêm học sinh mới thành công.", "success");
        setTimeout(function () {
            window.location.href = "/quan-ly-hoc-sinh";
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}

///////////////////////////////////////////////////CẬP NHẬT/////////////////////////////////////////////////
function UpdateStudents() {
    if (checkvalidate() == true) {
        showLoading("btnUpdate");
        var arr = [];
        //Lấy danh sách quyền
        if ($('#ContentTableList tr td').length > 1) {
            $('#ContentTableList tr').each(function () {
                var idkey = $(this).find('td[data-name="STT"]').html();
                var txtFamilyName = $(this).find('td[data-name="txtFamilyName"]').html();
                var sltRelationship = $(this).find('td[data-name="txtRelationshipvalue"]').html();
                var txtFamilyPhoneNumber = $(this).find('td[data-name="txtFamilyPhoneNumber"]').html();
                var txtFamilyEmailAddress = $(this).find('td[data-name="txtFamilyEmailAddress"]').html();

                var objName = {
                    MemberID: idkey,
                    Name: txtFamilyName,
                    Relationship: sltRelationship,
                    PhoneNumber: txtFamilyPhoneNumber,
                    EmailAddress: txtFamilyEmailAddress
                };

                arr.push(objName);
            });
            $('#hdListFamilyMember').val(JSON.stringify(arr));
        }
        $('#frmStudentsUpdate').submit();
        return true;

    }
    return false;
}

//Ham thuc hien sau khi update
function ajaxUpdateSuccess(data) {
    hideLoading("btnUpdate");

    if (data.iserror == false) {
        $.notify("Cập nhật học sinh thành công.", "success");
        setTimeout(function () {
            var strStudentID = $('#txtStudentID').val();
            setTimeout(function () { window.location.href = "/quan-ly-hoc-sinh/chi-tiet/" + strStudentID }, 500);
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}
//Insert Family Member
function OpenModalInsertFamilyMember() {
    $('#ModalFamilyMember').modal('toggle');
}
function CloseModalInsertFamilyMember() {
    $("#txtFamilyName").val("");
    $("#sltRelationship").val(0);
    $("#txtFamilyPhoneNumber").val("");
    $("#txtFamilyEmailAddress").val("");
    $('#ModalFamilyMember').modal('toggle');
}
function InitAddFamilyMember() {
 
    $("#btnAddFamilyMember").click(function () {
            //Lấy thông tin
            var txtFamilyName = $("#txtFamilyName").val();
            var txtFamilyPhoneNumber = $("#txtFamilyPhoneNumber").val();
            var txtFamilyEmailAddress = $("#txtFamilyEmailAddress").val();
            var sltRelationship = $("#sltRelationship").val();
            var txtRelationship = $("#sltRelationship option:selected").text();
            var idkey = $("#ContentTableList").find("tr[data-name='trcontent']").length +1;
            if (txtFamilyName == "") {
                $.notify("Vui lòng nhập Họ Tên", "error");
                $("#txtFamilyName").focus();
                $("#grpFamilyName").addClass("has-error");
                return false;
            }
            if (sltRelationship == null) {
                $.notify("Vui lòng chọn Mối quan hệ", "error");
                $("#sltRelationship").focus();
                $("#grpRelationship").addClass("has-error");
                return false;
            }
            if (txtFamilyPhoneNumber == "") {
                $.notify("Vui lòng nhập Số Điện Thoại", "error");
                $("#txtFamilyPhoneNumber").focus();
                $("#grpFamilyPhoneNumber").addClass("has-error");
                return false;
            }
            if (isNaN(txtFamilyPhoneNumber) == true) {
                $.notify("Số điện thoại phải là số", "error");
                $("#txtFamilyPhoneNumber").focus();
                $("#grpFamilyPhoneNumber").addClass("has-error");
                return false;
            }
            if (txtFamilyEmailAddress == "") {
                $.notify("Vui lòng nhập địa chỉ Email", "error");
                $("#txtFamilyEmailAddress").focus();
                $("#grpFamilyEmailAddress").addClass("has-error");
                return false;
            }
            if (isEmail(txtFamilyEmailAddress) == false) {
                $.notify("Email không đúng định dạng abc@domainName.com", "error");
                $("#txtFamilyEmailAddress").focus();
                $("#grpFamilyEmailAddress").addClass("has-error");
                return false;
            }
            //Gen HTML để thể hiện ra bảng chính
            var strHTML= "";
            strHTML += "<tr data-name='trcontent'>";
            strHTML += "<td data-name='STT'>" + idkey + "</td>";
            strHTML += "<td data-name='txtFamilyName'>" + txtFamilyName + "</td>";
            strHTML += "<td data-name='txtFamilyPhoneNumber'>" + txtFamilyPhoneNumber + "</td>";
            strHTML += "<td data-name='txtFamilyEmailAddress'>" + txtFamilyEmailAddress + "</td>";
            strHTML += "<td data-name='txtRelationship'>" + txtRelationship + "</td>";
            strHTML += "<td data-name='txtRelationshipvalue' hidden>" + sltRelationship + "</td>";
            strHTML += "<td><span class='glyphicon glyphicon-remove table-icon' data-name='btnRemoveMember'></span></td>";

            strHTML += "</tr>";
            CloseModalInsertFamilyMember();
            //Đưa tr vào bảng trên giao diện chính
            if ($('#ContentTableList tr td').length == 1) { //Hiện không có nội dung
                $('#ContentTableList').empty();
                $('#ContentTableList').append(strHTML);
            } else {
                $('#ContentTableList').append(strHTML);
            }
            InitRemoveFamilyMember();

    });

}
function InitRemoveFamilyMember() {
    $('#ContentTableList tr').each(function () {
        $(this).find("td span[data-name='btnRemoveMember']").click(function () {
            $(this).parent().parent().remove();

            //Nếu không còn dòng dữ liệu nào
            if ($('#ContentTableList tr').length == 0)
                $('#ContentTableList').append('<tr><td colspan="12" class="table-content-null">Hiện không có nội dung</td></tr>');
        });
    });
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function isBirthDayValid(birhtday)
{

    var curenttime = new Date();
    var year2 = curenttime.getFullYear();
    var year1 = parseInt(birhtday.substring(6), 10)
    var sub = year2-year1;
    if(sub >= minimumAged && sub <=maximumAged)
    {
        return true;
    }
    return false;
}