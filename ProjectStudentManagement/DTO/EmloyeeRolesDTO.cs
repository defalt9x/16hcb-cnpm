﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class EmloyeeRolesDTO
    {
        public string RoleID { get; set; }

        public string RoleName { get; set; }

        public List<PermissionDTO> lstPermission { get; set; }
    }
}
