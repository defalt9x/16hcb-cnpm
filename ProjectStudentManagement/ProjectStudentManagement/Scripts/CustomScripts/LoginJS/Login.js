﻿$(document).ready(function () {
    //Khởi tạo sự kiện nút enter
    $('input').keypress(function (e) {
        var key = e.which;
        if (key == 13)
        {
            $('#btnlogin').click();
            return false;
        }
    });

    //Khởi tạo sự kiện cho nút đăng nhập
    $("#btnlogin").click(function () {
        var username = $("#inptUsername").val();
        var password = $("#inptPass").val();

        //Kiểm tra dữ liệu
        if (username == "") {
            $.notify("Vui lòng nhập Tên đăng nhập", "info");
            $("#inptUsername").focus();
            return false;
        }

        if (password == "") {
            $.notify("Vui lòng nhập Mật khẩu", "info");
            $("#inptPass").focus();
            return false;
        }

        if (password.length < 4) {
            $.notify("Mật khẩu phải lớn hơn 4 kí tự", "info");
            $("#inptPass").focus();
            return false;
        }

        var UserInfo = { Username: username, Password: password };
        $.ajax({
            url: "/Home/LoginCheck",
            type: "POST",
            data: '{user:' + JSON.stringify(UserInfo) + '}',
            contentType: 'application/json; charset=UTF-8',
            dataType: "json",
            beforeSend: function () {
                $("#btnlogin").button('loading');
            },
            success: function (data) {
                //setTimeout(function () {
                //    if (data.iserror == false) {
                //        if (data.chkUser == true) {
                //            window.location.href = "/Home/Index";
                //        }
                //        else {
                //            window.location.href = "/Home/Login";
                //        }
                //    } else {
                //        $("#btnlogin").button('reset');
                //    }
                //}, 8000);

                if (data.iserror == false) {
                    if (data.chkUser == true) {
                        window.location.href = "/Home/Index";
                    }
                    else {
                        $("#btnlogin").button('reset');
                        $("#inptPass").val("");
                        $.notify("Đăng nhập không thành công, xin kiểm tra lại tài khoản và mật khẩu!", "error");
                    }
                } else {
                    $("#btnlogin").button('reset');
                    $.notify("Lỗi #Login01, xin thông báo với quản trị viên!", "error");
                }
            },
            error: function (xhr, status, error) {
                //alert(xhr.responseText);
                $("#btnlogin").button('reset');
                $.notify("Lỗi #Login02, xin thông báo với quản trị viên!", "error");
            }
        });
    });
})