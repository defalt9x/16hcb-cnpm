﻿using DAO;
using DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectStudentManagement.Controllers
{
    public class ClassesController : Controller
    {
        #region Index
        //
        // GET: /Classes/
        public ActionResult Index()
        {
            //Kiểm tra xem người dùng có được xem(truy cập vào chức năng)
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLLH_V") == false)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra xem người dùng có được phép thêm
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLLH_I") == false)
            {
                ViewBag.InsertPermission = false;
            }

            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            return View();
        }

        [HttpPost]
        public ActionResult SearchClasses(string strKeyWord, int intPageIndex = 0)
        {
            try
            {
                //Lấy cấu hình số dòng trên trang
                //int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTMLClasses = new StringBuilder();

                DataTable dtbClasses = new ClassesDAO().SearchClasses(strKeyWord, intPageIndex, intVisiblePages);

                if (dtbClasses != null && dtbClasses.Rows.Count > 0)
                {
                    strHTMLClasses = GenHTMLClasses(dtbClasses);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtbClasses.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTMLClasses.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTMLClasses),
                    totalpages = intTotalPages,
                    totalrows = intTotalRows
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        private StringBuilder GenHTMLClasses(DataTable dtbPermission)
        {
            bool bPerEdit = true;
            //Kiểm tra người dùng được xóa
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLLH_U") == false)
            {
                bPerEdit = false;
            }
            bool bPerDelete = true;
            //Kiểm tra người dùng được xóa
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLLH_D") == false)
            {
                bPerDelete = false;
            }


            int intCountRow = dtbPermission.Rows.Count;
            StringBuilder strHTMLClasses = new StringBuilder();

            for (int i = 0; i < intCountRow; i++)
            {
                string strClassesID = Convert.ToString(dtbPermission.Rows[i]["ClassID"]).Trim();

                strHTMLClasses.Append("<tr>");
                strHTMLClasses.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i]["STT"]);
                strHTMLClasses.AppendFormat("<td class='ClassesIDMD'>{0}</td>", strClassesID);
                strHTMLClasses.AppendFormat("<td class='ClassesNameMD'>{0}</td>", dtbPermission.Rows[i]["ClassName"]);
                strHTMLClasses.AppendFormat("<td class='ClassesGradeMD'>{0}</td>", dtbPermission.Rows[i]["Grade"]);
                string sEdit = "";
                string sDelete = "";
                if (bPerEdit != false)
                {
                    sEdit += "<span class='glyphicon glyphicon-pencil table-icon' data-name='Detail'></span>";
                }

                if (bPerDelete != false)
                {
                    sDelete += "<span class='glyphicon glyphicon-trash table-icon' data-name='Delete'></span>";
                }
                strHTMLClasses.AppendFormat("<td data-id='{0}'>" + sEdit + sDelete + "</td>", strClassesID);

                strHTMLClasses.Append("</tr>");
            }

            return strHTMLClasses;
        }
        #endregion

        #region Delete
        [HttpPost]
        public JsonResult DeleteClasses(string strClassesID)
        {
            try
            {
                bool bolSuccess = new ClassesDAO().DeleteClasses(strClassesID);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region Insert
        public ActionResult ClassesInsert()
        {
            //Kiểm tra xem người dùng có được phép thêm
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLLH_I") == false)
            {
                return RedirectToAction("Login", "Home");
            }
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            return View();
        }

        [HttpPost]
        public JsonResult InsertClasses(FormCollection frm)
        {
            try
            {
                string strClassName = Convert.ToString(frm["tbxClassName"]);
                string strClassGrade = Convert.ToString(frm["sltGrade"]);
                ClassesDAO cDA = new ClassesDAO();
                bool bolSuccess = cDA.InsertClasses(strClassName, strClassGrade);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

       

        #region Update


        public ActionResult EditClassesName(string strClassesID, string strClassesName)
        {
            try
            {
                ClassesDAO cDAO = new ClassesDAO();
                bool bolSuccess = cDAO.EditClassesName(strClassesID, strClassesName);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region Helper
        [HttpPost]
        public ActionResult GetRole()
        {

            try
            {
                DataTable dtbQuery = new EmployeesDAO().GetRoleList();
                string sRoleList = JsonConvert.SerializeObject(dtbQuery);
                return Json(new { iserror = false, Rolelistcontent = sRoleList });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }
        #endregion
	}
}