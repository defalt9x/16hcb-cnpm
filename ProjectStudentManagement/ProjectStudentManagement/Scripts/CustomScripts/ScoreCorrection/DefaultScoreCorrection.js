﻿var IsLoadServer = false;

function Paging(totalPages, totalRows, startpage) {
    if (totalRows > parseInt(visiblepages)) {
        $('#paging').remove();
        $('#divPagingIndex').html("<ul id='paging' class='pagination'></ul>");
        $('#paging').twbsPagination({
            startPage: startpage,
            totalPages: totalPages,
            visiblePages: visiblepages,
            onPageClick: function (event, page) {
                //Ghi nhận chỉ số trang hiện tại
                $('#hdCurrentPage').val(page - 1); //Vì store bắt đầu từ 0 nên -1

                //Load data ứng với chỉ số page từ server
                if (IsLoadServer == true)
                    SearchAjaxPaging();

                //Thể hiện chỉ số record đang hiển thị trên giao diện
                var visiblePages = parseInt(visiblepages);
                var ToIndextemp = page * visiblePages;
                var FromIndex = (page - 1) * visiblePages + 1;
                //Lấy chỉ số đến trang (Nếu không là trang cuối)
                if (ToIndextemp < totalRows)
                    ToIndex = ToIndextemp;
                else
                    ToIndex = totalRows;

                $('#page-from-index').text(FromIndex);
                $('#page-to-index').text(ToIndex);
                $('#total-record').text(totalRows);

                IsLoadServer = true;
            }
        });
        $('#divPaging').css('display', 'block');
    } else {
        $('#divPaging').css('display', 'none');
    }
}

function BackManage() {
    setTimeout(function () { window.location.href = "/quan-ly-phieu-hieu-chinh-diem/" }, 500);
}

//function EditScoreCorrection() {
//    var strScoreCorrectionID = $('#hdScoreCorrectionID').val();

//    setTimeout(function () { window.location.href = "/quan-ly-phieu-hieu-chinh-diem/cap-nhat/" + strScoreCorrectionID }, 500);
//}


function LoadDropdownlist() {
    //Nạp năm vào selectdropdown
    $.ajax({
        url: "/Home/GetYearList",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            if (data.iserror == false) {
                var obj = JSON.parse(data.yearlistcontent);
                var objsize = Object.keys(obj).length;
                for (var i = 0; i < objsize; i++) {
                    $("#sltKeyWordYear").append("<option value='" + obj[i].Year + "'>Niên khóa: " + obj[i].Year + " </option>");
                }
            }
            else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });


    //Nạp môn học vào selectdropdown
    $.ajax({
        url: "/ScoreCorrection/GetSubjectList",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            if (data.iserror == false) {
                var obj = JSON.parse(data.subjectlistcontent);
                var objsize = Object.keys(obj).length;
                for (var i = 0; i < objsize; i++) {
                    $("#sltKeyWordSubject").append("<option value='" + obj[i].SubjectID + "'>Môn học: " + obj[i].SubjectName + " </option>");
                }
            }
            else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

function OnchangeDropdownlist(id) {
    //xóa điểm hiệu chỉnh khi có thay đổi
    $("#sltKeyWordYear").change(function () {
        $(id).html('<tr><td colspan="100%" class="table-content-null">Hiện không có nội dung</td></tr>');
    });
    $("#sltKeyWordSemeter").change(function () {
        $(id).html('<tr><td colspan="100%" class="table-content-null">Hiện không có nội dung</td></tr>');
    });
    $("#sltKeyWordSubject").change(function () {
        $(id).html('<tr><td colspan="100%" class="table-content-null">Hiện không có nội dung</td></tr>');
    });
    $("#sltKeyWordStudent").change(function () {
        $(id).html('<tr><td colspan="100%" class="table-content-null">Hiện không có nội dung</td></tr>');
    });
}


