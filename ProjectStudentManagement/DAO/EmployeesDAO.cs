﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class EmployeesDAO
    {

        public DataTable SearchEmployees(string strKeyWord, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWord", strKeyWord
            };

            DataTable dtbEmployees = new DataConnect().callUSP("uspGetEmloyees_SRH", objKeyWords);
            if (dtbEmployees != null && dtbEmployees.Rows.Count != 0)
            {
                return dtbEmployees;
            }

            return null;
        }

        public bool DeleteEmployees(string strEmployeeID)
        {
            object[] objKeyWords = new object[] {
                "@EmployeeID", strEmployeeID
            };

            DataTable dtbEmployees = new DataConnect().callUSP("uspDeleteEmployees", objKeyWords);
            if (dtbEmployees != null)
            {
                return true;
            }
            return false;
        }


        public DataTable GetRoleList()
        {
            DataTable dtbQuery = new DataConnect().callUSP("uspGetRoleList", null);
            if (dtbQuery != null && dtbQuery.Rows.Count != 0)
            {
                return dtbQuery;
            }

            return null;
        }

        public bool InsertEmployees(string strLastName, string strFirstName, string strAddress, string strDistrict, string strCity, string strSDT, string strEmail, string strRole, string strPass)
        {

            object[] objKeyWords = new[] {
                "@FirstName", strFirstName,
                "@LastName", strLastName,
                "@Address", strAddress,
                "@Distric", strDistrict,
                "@City", strCity,
                "@Phone", strSDT,
                "@Email", strEmail,
                "@Role", strRole,
                "@Pass", DataConnect.MD5Hash(strPass)
            };

            DataTable dtbQuery = new DataConnect().callUSP("uspInsertEmployee", objKeyWords);
            //Nếu tồn tại dòng dữ liệu chứng tỏ đăng nhập thành công
            if (dtbQuery != null && dtbQuery.Rows.Count != 0)
            {
                return true;
            }
            return false;
        }


        public bool UpdateEmployees(string strEmployeeID, string strLastName, string strFirstName, string strAddress, string strDistrict, string strCity, string strSDT, string strEmail, string strRole)
        {

            object[] objKeyWords = new[] {
                "@EmployeeID",strEmployeeID,
                "@FirstName", strFirstName,
                "@LastName", strLastName,
                "@Address", strAddress,
                "@Distric", strDistrict,
                "@City", strCity,
                "@Phone", strSDT,
                "@Email", strEmail,
                "@Role", strRole,
            };

            DataTable dtbQuery = new DataConnect().callUSP("uspUpdateEmployee", objKeyWords);
            //Nếu tồn tại dòng dữ liệu chứng tỏ đăng nhập thành công
            if (dtbQuery != null && dtbQuery.Rows.Count != 0)
            {
                return true;
            }
            return false;
        }

        public EmployeesDTO GetEmployeesByID(string strEmployeeID)
        {
            EmployeesDTO EDTO = new EmployeesDTO();
            object[] objKeyWords = new object[] {
                "@EmployeeID", strEmployeeID
            };
            DataTable dtbQuery = new DataConnect().callUSP("uspGetDetailEmployeesByID", objKeyWords);
            if (dtbQuery != null && dtbQuery.Rows.Count != 0)
            {
                EDTO.EmployeesID = (string)dtbQuery.Rows[0]["EmployeeID"];
                EDTO.strLastName = (string)dtbQuery.Rows[0]["LastName"];
                EDTO.strFirstName = (string)dtbQuery.Rows[0]["FirstName"];
                EDTO.strAddress = (string)dtbQuery.Rows[0]["Address"];
                EDTO.strDistrict = (string)dtbQuery.Rows[0]["District"];
                EDTO.strCity = (string)dtbQuery.Rows[0]["CityorProvince"];
                EDTO.strSDT = (string)dtbQuery.Rows[0]["PhoneNumber"];
                EDTO.strEmail = (string)dtbQuery.Rows[0]["EmailAddress"];
                EDTO.strRoleID = (string)dtbQuery.Rows[0]["RoleID"];
                EDTO.strRole = (string)dtbQuery.Rows[0]["RoleName"];
                return EDTO;
            }

            return null;
        }
    }
}
