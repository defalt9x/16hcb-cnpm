﻿var IsLoadServer = false;

$(document).ready(function () {
    $("#menu-toggle").click(function () {
        $("#sidebar-hidden-div").toggle();
        if ($("#sidebar-hidden-div").css('display') == "none")
            $("#page-wrapper").prop('id', 'page-wrapper-disable');
        else
            $("#page-wrapper-disable").prop('id', 'page-wrapper');
    });
    //$("#menu-toggle2").click(function (e) {
    //    $("#sidebar-hidden-div").show();
    //    $("#page-wrapper-disable").prop('id', 'page-wrapper');
    //});
});

function SearchClasses() {
    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').val();

    $.ajax({
        url: "/Classes/SearchClasses",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
            $("#btnSearch").button('loading');
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentClasses').html(data.content);
                Paging(data.totalpages, data.totalrows, 1);
                InitProcessInTable();

                //Gán data để xóa có data load lại trang
                $('#txtKeyWord').attr('data-value', $('#txtKeyWord').val());
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }

            $("#btnSearch").button('reset');
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            $("#btnSearch").button('reset');
        }
    });
}

function Paging(totalPages, totalRows, startpage) {
    if (totalRows > parseInt(visiblepages)) {
        $('#paging').remove();
        $('#divPagingIndex').html("<ul id='paging' class='pagination'></ul>");
        $('#paging').twbsPagination({
            startPage: startpage,
            totalPages: totalPages,
            visiblePages: visiblepages,
            onPageClick: function (event, page) {
                //Ghi nhận chỉ số trang hiện tại
                $('#hdCurrentPage').val(page - 1); //Vì store bắt đầu từ 0 nên -1

                //Load data ứng với chỉ số page từ server
                if (IsLoadServer == true)
                    SearchAjaxPaging();

                //Thể hiện chỉ số record đang hiển thị trên giao diện
                var visiblePages = parseInt(visiblepages);
                var ToIndextemp = page * visiblePages;
                var FromIndex = (page - 1) * visiblePages + 1;
                //Lấy chỉ số đến trang (Nếu không là trang cuối)
                if (ToIndextemp < totalRows)
                    ToIndex = ToIndextemp;
                else
                    ToIndex = totalRows;

                $('#page-from-index').text(FromIndex);
                $('#page-to-index').text(ToIndex);
                $('#total-record').text(totalRows);

                IsLoadServer = true;
            }
        });
        $('#divPaging').css('display', 'block');
    } else {
        $('#divPaging').css('display', 'none');
    }
}

function SearchAjaxPaging() {
    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').attr('data-value');
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/Classes/SearchClasses",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentClasses').html(data.content);
                //Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()));
                InitProcessInTable();
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

function InitProcessInTable() {
    $('#ContentClasses tr').each(function () {
        $(this).find('td span[data-name="Detail"]').click(function () {
            var strClassesID = $(this).closest('td').attr('data-id');
            var strNameID = $(this).closest('tr').find('.ClassesNameMD').html();
            var strGrade = $(this).closest('tr').find('.ClassesGradeMD').html();
            $('#txtClassesIDModal').val(strClassesID);
            $('#txtClassesGradeModal').val(strGrade);
            $('#txtClassesNameModal').val(strNameID);
            $('#ModalConfirmEdit').modal('toggle');
        });

        $(this).find('td span[data-name="Delete"]').click(function () {
            var strClassesID = $(this).closest('td').attr('data-id');
            var strNameID = $(this).closest('tr').find('.ClassesNameMD').html();
            var strMessage = "Bạn có muốn xóa lớp " + strNameID + "?"

            $('#divMessage').html(strMessage);
            $('#divMessage').attr('data-id', strClassesID);
            $('#ModalConfirmDelete').modal('toggle');
        });
    });
}

function InsertClasses() {
    setTimeout(function () { window.location.href = "/quan-ly-lop/them" }, 500);
}

function DeleleClasses() {
    $('#hdCurRowBeforeDelOnScreen').val($('#ContentClasses tr').length);
    param = {};
    param.strClassesID = $('#divMessage').attr('data-id');
    
    $.ajax({
        url: "/Classes/DeleteClasses",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $.notify("Xóa thành công.", "info");
                SearchEmployeesAfterDelete(parseInt($('#hdCurRowBeforeDelOnScreen').val()) - 1);
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

function togglemadalf() {
    $('#ModalConfirmEdit').modal('toggle');
}


function ChangeClassesName() {
    if ($("#txtClassesNameModal").val() == "") {
        $.notify("Vui lòng nhập tên lớp", "info");
        $("#txtClassesNameModal").focus();
        return false;
    }

    $('#hdCurRowBeforeDelOnScreen').val($('#ContentClasses tr').length);
    param = {};
    param.strClassesID = $('#txtClassesIDModal').val();
    param.strClassesName = $('#txtClassesNameModal').val();

    $.ajax({
        url: "/Classes/EditClassesName",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                togglemadalf();
                $.notify("Thay đổi thành công.", "info");
                SearchEmployeesAfterDelete(parseInt($('#hdCurRowBeforeDelOnScreen').val()) - 1);
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}

function SearchEmployeesAfterDelete(CurRowAfterDelOnScreen) {
    if (CurRowAfterDelOnScreen == 0)
        $('#hdCurrentPage').val(parseInt($('#hdCurrentPage').val()) - 1);
    else
        $('#hdCurrentPage').val(parseInt($('#hdCurrentPage').val()));

    //Nạp tham số
    var param = {};
    param.strKeyWord = $('#txtKeyWord').attr('data-value');
    param.intPageIndex = $('#hdCurrentPage').val();

    $.ajax({
        url: "/Classes/SearchClasses",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            if (data.iserror == false) {
                $('#ContentClasses').html(data.content);
                Paging(data.totalpages, data.totalrows, parseInt($('#hdCurrentPage').val()) + 1);
                InitProcessInTable();
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}