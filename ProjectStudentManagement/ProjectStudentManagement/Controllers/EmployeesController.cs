﻿using DAO;
using DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectStudentManagement.Controllers
{
    public class EmployeesController : Controller
    {
        #region Index
        //
        // GET: /Employees/
        public ActionResult Index()
        {
            //Kiểm tra xem người dùng có được xem(truy cập vào chức năng)
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLND_V") == false)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra xem người dùng có được phép thêm
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLND_I") == false)
            {
                ViewBag.InsertPermission = false;
            }

            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            return View();
        }

        [HttpPost]
        public ActionResult SearchEmployees(string strKeyWord, int intPageIndex = 0)
        {
            try
            {
                //Lấy cấu hình số dòng trên trang
                //int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTMLEmployees = new StringBuilder();

                DataTable dtbEmployees = new EmployeesDAO().SearchEmployees(strKeyWord, intPageIndex, intVisiblePages);

                if (dtbEmployees != null && dtbEmployees.Rows.Count > 0)
                {
                    strHTMLEmployees = GenHTMLEmployees(dtbEmployees);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtbEmployees.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTMLEmployees.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTMLEmployees),
                    totalpages = intTotalPages,
                    totalrows = intTotalRows
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        private StringBuilder GenHTMLEmployees(DataTable dtbPermission)
        {
            bool bPerEdit = true;
            bool bPerDelete = true;
            //Kiểm tra người dùng được xóa
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLND_D") == false)
            {
                bPerDelete = false;
            }


            int intCountRow = dtbPermission.Rows.Count;
            StringBuilder strHTMLEmployees = new StringBuilder();

            for (int i = 0; i < intCountRow; i++)
            {
                string strEmloyeeID = Convert.ToString(dtbPermission.Rows[i]["EmployeeID"]).Trim();

                strHTMLEmployees.Append("<tr>");
                strHTMLEmployees.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i]["STT"]);
                strHTMLEmployees.AppendFormat("<td>{0}</td>", strEmloyeeID);
                strHTMLEmployees.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i]["FullName"]);
                string sEdit = "";
                string sDelete = "";
                if (bPerEdit != false)
                {
                    sEdit += "<span class='glyphicon glyphicon-pencil table-icon' data-name='Detail'></span>";
                }

                if (bPerDelete != false)
                {
                    sDelete += "<span class='glyphicon glyphicon-trash table-icon' data-name='Delete'></span>";
                }
                strHTMLEmployees.AppendFormat("<td data-employeeid='{0}'>" + sEdit + sDelete + "</td>", strEmloyeeID);

                strHTMLEmployees.Append("</tr>");
            }

            return strHTMLEmployees;
        }
        #endregion

        #region Delete
        [HttpPost]
        public JsonResult DeleteEmployees(string strEmployeeID)
        {
            try
            {
                bool bolSuccess = new EmployeesDAO().DeleteEmployees(strEmployeeID);

                if (bolSuccess == false)
                    return Json(new { iserror = true});

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region Insert
        public ActionResult EmployeesInsert()
        {
            //Kiểm tra xem người dùng có được phép thêm
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLND_I") == false)
            {
                return RedirectToAction("Login", "Home");
            }
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            return View();
        }

        [HttpPost]
        public JsonResult InsertEmployees(FormCollection frm)
        {
            try
            {
                string strLastName = Convert.ToString(frm["tbxHo"]);
                string strFirstName = Convert.ToString(frm["tbxTen"]);
                string strAddress = Convert.ToString(frm["tbxAddress"]);
                string strDistrict = Convert.ToString(frm["tbxDistrict"]);
                string strCity = Convert.ToString(frm["tbxCity"]);
                string strSDT = Convert.ToString(frm["tbxSDT"]);
                string strEmail = Convert.ToString(frm["tbxEmail"]);
                string strRole = Convert.ToString(frm["sltKeyWordRole"]);
                string strPass = Convert.ToString(frm["tbxPass"]);

                bool bolSuccess = new EmployeesDAO().InsertEmployees(strLastName, strFirstName, strAddress, strDistrict, strCity, strSDT, strEmail, strRole, strPass);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region Detail
        public ActionResult EmployeesDetail(string strEmployeeID)
        {
            //Kiểm tra xem người dùng có được phép thêm
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLND_U") == false)
            {
                ViewBag.UpdatePermission = false;
            }
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            EmployeesDTO EDTO = new EmployeesDAO().GetEmployeesByID(strEmployeeID);
            return View(EDTO);
        }
        #endregion

        #region Update
        public ActionResult EmployeesUpdate(string strEmployeeID)
        {
            //EmployeesUpdate
            //Kiểm tra xem người dùng có được phép sửa
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLND_U") == false)
            {
                return RedirectToAction("Login", "Home");
            }
            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            EmployeesDTO EDTO = new EmployeesDAO().GetEmployeesByID(strEmployeeID);
            return View(EDTO);
        }


        public ActionResult UpdateEmployee(FormCollection frm)
        {
            try
            {
                string strEmployeeID = Convert.ToString(frm["tbxMGV"]);
                string strLastName = Convert.ToString(frm["tbxHo"]);
                string strFirstName = Convert.ToString(frm["tbxTen"]);
                string strAddress = Convert.ToString(frm["tbxAddress"]);
                string strDistrict = Convert.ToString(frm["tbxDistrict"]);
                string strCity = Convert.ToString(frm["tbxCity"]);
                string strSDT = Convert.ToString(frm["tbxSDT"]);
                string strEmail = Convert.ToString(frm["tbxEmail"]);
                string strRole = Convert.ToString(frm["sltKeyWordRole"]);

                bool bolSuccess = new EmployeesDAO().UpdateEmployees(strEmployeeID,strLastName, strFirstName, strAddress, strDistrict, strCity, strSDT, strEmail, strRole);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region Helper
        [HttpPost]
        public ActionResult GetRole()
        {

            try
            {
                DataTable dtbQuery = new EmployeesDAO().GetRoleList();
                string sRoleList = JsonConvert.SerializeObject(dtbQuery);
                return Json(new { iserror = false, Rolelistcontent = sRoleList });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }
        #endregion
    }
}