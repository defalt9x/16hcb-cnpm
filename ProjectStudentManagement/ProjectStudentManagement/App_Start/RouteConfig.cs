﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectStudentManagement
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region Báo cáo
            routes.MapRoute(
                name: "bao-cao-hoc-ky",
                url: "bao-cao-hoc-ky",
                defaults: new { controller = "Home", action = "Report" }
            );


            routes.MapRoute(
                name: "bao-cao-mon",
                url: "bao-cao-mon",
                defaults: new { controller = "Home", action = "Report2" }
            );
            #endregion

            #region Nhóm người dùng
            routes.MapRoute(
                name: "quan-ly-nhom-nguoi-dung",
                url: "quan-ly-nhom-nguoi-dung",
                defaults: new { controller = "EmloyeeRoles", action = "Index" }
            );

            routes.MapRoute(
                name: "quan-ly-nhom-nguoi-dung/them",
                url: "quan-ly-nhom-nguoi-dung/them",
                defaults: new { controller = "EmloyeeRoles", action = "EmloyeeRolesInsert" }
            );

            routes.MapRoute(
                name: "quan-ly-nhom-nguoi-dung/cap-nhat",
                url: "quan-ly-nhom-nguoi-dung/cap-nhat/{strRoleID}",
                defaults: new { controller = "EmloyeeRoles", action = "EmloyeeRolesUpdate", strRoleID = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "quan-ly-nhom-nguoi-dung/chi-tiet",
                url: "quan-ly-nhom-nguoi-dung/chi-tiet/{strRoleID}",
                defaults: new { controller = "EmloyeeRoles", action = "EmloyeeRolesDetail", strRoleID = UrlParameter.Optional }
            );
            #endregion

            #region Quản lý học sinh
            routes.MapRoute(
                name: "quan-ly-hoc-sinh",
                url: "quan-ly-hoc-sinh",
                defaults: new { controller = "Students", action = "Index" }
            );

            routes.MapRoute(
                name: "quan-ly-hoc-sinh/them",
                url: "quan-ly-hoc-sinh/them",
                defaults: new { controller = "Students", action = "StudentsInsert" }
            );

            routes.MapRoute(
                name: "quan-ly-hoc-sinh/cap-nhat",
                url: "quan-ly-hoc-sinh/cap-nhat/{strStudentID}",
                defaults: new { controller = "Students", action = "StudentsUpdate", strRoleID = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "quan-ly-hoc-sinh/chi-tiet",
                url: "quan-ly-hoc-sinh/chi-tiet/{strStudentID}",
                defaults: new { controller = "Students", action = "StudentsDetails", strRoleID = UrlParameter.Optional }
            );
            #endregion

            #region Thay đổi mật khẩu
            routes.MapRoute(
                name: "doi-mat-khau",
                url: "doi-mat-khau",
                defaults: new { controller = "Home", action = "ChangePass" }
            );

            #endregion

            #region Lớp học
           routes.MapRoute(
               name: "quan-ly-lop-hoc",
               url: "quan-ly-lop-hoc",
               defaults: new { controller = "ClassesStudents", action = "Index" }
           );

           routes.MapRoute(
              name: "quan-ly-lop",
              url: "quan-ly-lop",
              defaults: new { controller = "Classes", action = "Index" }
          );

           routes.MapRoute(
             name: "quan-ly-lop/them",
             url: "quan-ly-lop/them",
             defaults: new { controller = "Classes", action = "ClassesInsert" }
         );

            //routes.MapRoute(
            //    name: "quan-ly-hoc-sinh/them",
            //    url: "quan-ly-hoc-sinh/them",
            //    defaults: new { controller = "Students", action = "StudentsInsert" }
            //);

            //routes.MapRoute(
            //    name: "quan-ly-hoc-sinh/cap-nhat",
            //    url: "quan-ly-hoc-sinh/cap-nhat/{strStudentID}",
            //    defaults: new { controller = "Students", action = "StudentsUpdate", strRoleID = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "quan-ly-hoc-sinh/chi-tiet",
            //    url: "quan-ly-hoc-sinh/chi-tiet/{strStudentID}",
            //    defaults: new { controller = "Students", action = "StudentsDetails", strRoleID = UrlParameter.Optional }
            //);
            #endregion

            #region Quản lý điểm
            routes.MapRoute(
                name: "quan-ly-diem",
                url: "quan-ly-diem",
                defaults: new { controller = "Score", action = "Index" }
            );


            routes.MapRoute(
                name: "quan-ly-diem/sua-diem",
                url: "quan-ly-diem/sua-diem/{strStudentID}/{strYear}/{strSemeter}/{strSubject}",
                defaults: new { controller = "Score", action = "Detail", strStudentID = UrlParameter.Optional, strYear = UrlParameter.Optional, strSemeter = UrlParameter.Optional, strSubject = UrlParameter.Optional }
            );
         
            #endregion

            #region Phiếu hiệu chỉnh điểm
            routes.MapRoute(
                name: "quan-ly-phieu-hieu-chinh-diem",
                url: "quan-ly-phieu-hieu-chinh-diem",
                defaults: new { controller = "ScoreCorrection", action = "Index" }
            );
            routes.MapRoute(
                name: "quan-ly-phieu-hieu-chinh-diem/lap-phieu",
                url: "quan-ly-phieu-hieu-chinh-diem/lap-phieu",
                defaults: new { controller = "ScoreCorrection", action = "Create" }
            );
            routes.MapRoute(
                name: "quan-ly-phieu-hieu-chinh-diem/chi-tiet",
                url: "quan-ly-phieu-hieu-chinh-diem/chi-tiet/{id}",
                defaults: new { controller = "ScoreCorrection", action = "Details", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "quan-ly-phieu-hieu-chinh-diem/cap-nhat",
            //    url: "quan-ly-phieu-hieu-chinh-diem/cap-nhat/{strScoreCorrectionID}",
            //    defaults: new { controller = "ScoreCorrection", action = "Update", strRoleID = UrlParameter.Optional }
            //);

            #endregion

            #region Quản lý giáo viên
            routes.MapRoute(
                name: "quan-ly-giao-vien",
                url: "quan-ly-giao-vien",
                defaults: new { controller = "Employees", action = "Index" }
            );

            routes.MapRoute(
                name: "quan-ly-giao-vien/them",
                url: "quan-ly-giao-vien/them",
                defaults: new { controller = "Employees", action = "EmployeesInsert" }
            );

            routes.MapRoute(
                name: "quan-ly-giao-vien/cap-nhat",
                url: "quan-ly-giao-vien/cap-nhat/{strEmployeeID}",
                defaults: new { controller = "Employees", action = "EmployeesUpdate", strEmployeeID = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "quan-ly-giao-vien/chi-tiet",
                url: "quan-ly-giao-vien/chi-tiet/{strEmployeeID}",
                defaults: new { controller = "Employees", action = "EmployeesDetail", strEmployeeID = UrlParameter.Optional }
            );
            #endregion

            #region Quên mật khẩu
            routes.MapRoute(
                name: "quen-mat-khau",
                url: "quen-mat-khau",
                defaults: new { controller = "Home", action = "ResetPass" }
            );
            #endregion

            #region Cấu hình
            routes.MapRoute(
                name: "quan-ly-cau-hinh",
                url: "quan-ly-cau-hinh",
                defaults: new { controller = "SystemSettings", action = "Index" }
            );

            routes.MapRoute(
                name: "quan-ly-cau-hinh/them",
                url: "quan-ly-cau-hinh/them",
                defaults: new { controller = "SystemSettings", action = "SystemSettingsInsert" }
            );

            routes.MapRoute(
                name: "quan-ly-cau-hinh/cap-nhat",
                url: "quan-ly-cau-hinh/cap-nhat/{strSettingID}",
                defaults: new { controller = "SystemSettings", action = "SystemSettingsUpdate", strSettingID = UrlParameter.Optional }
            );
            #endregion

            #region Quản lý môn học
            routes.MapRoute(
                name: "quan-ly-mon-hoc",
                url: "quan-ly-mon-hoc",
                defaults: new { controller = "Subject", action = "Index" }
            );

            routes.MapRoute(
                name: "quan-ly-mon-hoc/them",
                url: "quan-ly-mon-hoc/them",
                defaults: new { controller = "Subject", action = "SubjectInsert" }
            );

            routes.MapRoute(
                name: "quan-ly-mon-hoc/cap-nhat",
                url: "quan-ly-mon-hoc/cap-nhat/{strSubjectID}",
                defaults: new { controller = "Subject", action = "SubjectUpdate", strSubjectID = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "quan-ly-mon-hoc/chi-tiet",
                url: "quan-ly-mon-hoc/chi-tiet/{strSubjectID}",
                defaults: new { controller = "Subject", action = "SubjectDetail", strSubjectID = UrlParameter.Optional }
            );
            #endregion

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}
