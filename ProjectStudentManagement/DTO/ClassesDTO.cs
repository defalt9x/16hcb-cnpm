﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ClassesDTO
    {
        public string ClassID { get; set; }
        public string ClassName { get; set; }
    }
}
