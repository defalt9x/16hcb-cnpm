﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ScoreCorrectionDAO
    {
        public List<ScoreCorrectionDetailDTO> GetDetail_ScoreCorrectionByID(string ScoreCorrectionID)
        {
            List<ScoreCorrectionDetailDTO> lst = new List<ScoreCorrectionDetailDTO>();
            DataTable dtbQuery2 = new DataConnect().callUSP(
                    "uspScoreCorrectionDetail_BySC_SEL",
                    new object[] { "@ScoreCorrectionID", ScoreCorrectionID });
            foreach (DataRow row in dtbQuery2.Rows)
            {
                ScoreCorrectionDetailDTO tmp = new ScoreCorrectionDetailDTO
                {
                    ScoreCorrectionDetailID = row["ScoreCorrectionDetailID"].ToString(),
                    ScoreCorrectionID = row["ScoreCorrectionID"].ToString(),
                    ScoreType = row["ScoreType"].ToString(),
                    ScoreOld = row["ScoreOld"].ToString(),
                    ScoreNew = row["ScoreNew"].ToString()

                };
                lst.Add(tmp);
            }
            return lst;
        }
        public DataTable SearchScoreCorrections(string strKeyWord, string strStudentID, string strKeyWordYear,
            string strKeyWordSemeter, string strKeyWordSubject, string strUserCreateID, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWord", strKeyWord,
	            "@StudentID", strStudentID,
	            "@Year", strKeyWordYear,
	            "@Semester", strKeyWordSemeter,
	            "@SubjectID", strKeyWordSubject,
                "@UserCreateID", strUserCreateID
            };

            DataTable dtbScoreCorrections = new DataConnect().callUSP("uspGetScoreCorrections_SRH", objKeyWords);
            if (dtbScoreCorrections != null && dtbScoreCorrections.Rows.Count != 0)
            {
                return dtbScoreCorrections;
            }

            return null;
        }

        public ScoreCorrectionDTO GetScoreCorrectionByID(string ScoreCorrectionID)
        {
            ScoreCorrectionDTO SCDTO = new ScoreCorrectionDTO();
            object[] objKeyWords = new object[] {
                "@ScoreCorrectionID", ScoreCorrectionID
            };
            DataTable dtbQuery = new DataConnect().callUSP("uspScoreCorrection_SEL", objKeyWords);

            if (dtbQuery != null && dtbQuery.Rows.Count != 0)
            {
                SCDTO.ScoreCorrectionID = dtbQuery.Rows[0]["ScoreCorrectionID"].ToString();
                SCDTO.ScoreID = dtbQuery.Rows[0]["ScoreID"].ToString();
                SCDTO.UserCreateID = dtbQuery.Rows[0]["UserCreateID"].ToString();
                SCDTO.IsAccepted = dtbQuery.Rows[0]["IsAccepted"].ToString();
                SCDTO.UserDecideID = dtbQuery.Rows[0]["UserDecideID"].ToString();
                string myTime = Convert.ToDateTime(dtbQuery.Rows[0]["InsertedDatetime"]).ToShortTimeString();
                DateTime myTime2 = Convert.ToDateTime(myTime);
                SCDTO.InsertedDatetime = myTime2;
                string myTime1 = Convert.ToDateTime(dtbQuery.Rows[0]["LastModified"]).ToShortTimeString();
                DateTime myTime21 = Convert.ToDateTime(myTime1);
                SCDTO.LastModified = myTime21;

                SCDTO.Year = dtbQuery.Rows[0]["Year"].ToString();
                SCDTO.Semester = dtbQuery.Rows[0]["Semester"].ToString();
                SCDTO.SubjectID = dtbQuery.Rows[0]["SubjectID"].ToString();
                SCDTO.StudentID = dtbQuery.Rows[0]["StudentID"].ToString();

                DataTable dtbQuery2 = new DataConnect().callUSP(
                    "uspScoreCorrectionDetail_BySC_SEL",
                    new object[] { "@ScoreCorrectionID", ScoreCorrectionID });
                SCDTO.lstScoreCorrectionDetail = new List<ScoreCorrectionDetailDTO>();
                foreach (DataRow row in dtbQuery2.Rows)
                {
                    ScoreCorrectionDetailDTO tmp = new ScoreCorrectionDetailDTO
                    {
                        ScoreCorrectionDetailID = row["ScoreCorrectionDetailID"].ToString(),
                        ScoreCorrectionID = row["ScoreCorrectionID"].ToString(),
                        ScoreType = row["ScoreType"].ToString(),
                        ScoreOld = row["ScoreOld"].ToString(),
                        ScoreNew = row["ScoreNew"].ToString(),
                    };
                    SCDTO.lstScoreCorrectionDetail.Add(tmp);
                }
                return SCDTO;
            }

            return null;
        }
        
        public bool SetAcceptScoreCorrection(string ScoreCorrectionID, string IsAccepted, string UserDecideID)
        {
            object[] objKeyWords = new[] {
                "@ScoreCorrectionID", ScoreCorrectionID,
                "@IsAccepted", IsAccepted,
                "@UserDecideID", UserDecideID,
            };

            DataTable dtbQuery = new DataConnect().callUSP("uspSetAcceptScoreCorrection", objKeyWords);
            if (dtbQuery != null && dtbQuery.Rows.Count != 0)
            {
                return true;
            }
            return false;
        }

        public DataTable SearchScores(string strKeyWord, string strKeyWordYear,
            string strKeyWordSemeter, string strKeyWordSubject, string strStudentID, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWord", strKeyWord,
	            "@StudentID", strStudentID,
	            "@Year", strKeyWordYear,
	            "@Semester", strKeyWordSemeter,
	            "@SubjectID", strKeyWordSubject
            };

            DataTable dtb = new DataConnect().callUSP("uspGetScoreForCorrections_SRH", objKeyWords);
            if (dtb != null && dtb.Rows.Count != 0)
            {
                return dtb;
            }

            return null;
        }

        public bool InsertScoreCorrection(string ScoreID, string UserCreateID, string IsAccepted, string UserDecideID,
            DateTime? InsertedDatetime, DateTime? LastModified, List<ScoreCorrectionDetailDTO> lstScoreCorrectionDetail)
        {

            List<string> Lstusp_name = new List<string>() { "uspScoreCorrection_ADD" };
            List<object[]> LstobjKeyWords = new List<object[]>();

            //Tạo list keywords
            object[] objKeyWords1 = new object[] {
                "@ScoreCorrectionID", null,
                "@ScoreID", ScoreID,
                "@UserCreateID", UserCreateID,
                "@IsAccepted", IsAccepted,
                "@UserDecideID", UserDecideID,
                "@InsertedDatetime", InsertedDatetime,
                "@LastModified", LastModified
            };
            LstobjKeyWords.Add(objKeyWords1);

            for (int i = 0; i < lstScoreCorrectionDetail.Count; i++)
            {
                if (lstScoreCorrectionDetail[i].ScoreNew != "")
                {
                    Lstusp_name.Add("uspScoreCorrectionDetail_ADD");
                    object[] objKeyWords2 = new object[] {
                        "@ScoreCorrectionDetailID", lstScoreCorrectionDetail[i].ScoreCorrectionDetailID,
                        "@ScoreCorrectionID", "INFINITY",
                        "@ScoreType", lstScoreCorrectionDetail[i].ScoreType,
                        "@ScoreOld", lstScoreCorrectionDetail[i].ScoreOld,
                        "@ScoreNew", lstScoreCorrectionDetail[i].ScoreNew
                    };

                    LstobjKeyWords.Add(objKeyWords2);
                }
            }

            return new DataConnect().exeUSP(Lstusp_name, LstobjKeyWords, true);
        }

    }
}
