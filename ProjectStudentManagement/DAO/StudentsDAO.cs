﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAO
{
    public class StudentsDAO
    {
        public DataTable GetStudentsList(string strKeyWord, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWord", strKeyWord
            };

            DataTable dtb = new DataConnect().callUSP("uspStudents_GetList", objKeyWords);
            if (dtb != null && dtb.Rows.Count != 0)
            {
                return dtb;
            }

            return null;
        }

        public StudentsDTO GetStudentByID(string strStudentID)
        {
            StudentsDTO objResult = new StudentsDTO();

            //Lấy Nhóm người dùng
            object[] objKeyWords = new object[] {
                "@StudentID", strStudentID
            };

            DataTable dtb = new DataConnect().callUSP("uspStudents_GetStudentByID", objKeyWords);
            DateTime birthday = new DateTime();
            Boolean isdeleted = new Boolean();
            if (dtb != null && dtb.Rows.Count > 0)
            {
                objResult.StudentID = Convert.ToString(dtb.Rows[0]["StudentID"]).Trim();
                objResult.FirstName = Convert.ToString(dtb.Rows[0]["FirstName"]).Trim();
                objResult.LastName = Convert.ToString(dtb.Rows[0]["LastName"]).Trim();
                DateTime.TryParse(dtb.Rows[0]["Birthday"].ToString(), out birthday);
                objResult.Birthday = birthday;
                objResult.Address = Convert.ToString(dtb.Rows[0]["Address"]).Trim();
                objResult.District = Convert.ToString(dtb.Rows[0]["District"]).Trim();
                objResult.CityProvince = Convert.ToString(dtb.Rows[0]["CityOrProvince"]).Trim();
                objResult.Country = Convert.ToString(dtb.Rows[0]["Country"]).Trim();
                objResult.PhoneNumber = Convert.ToString(dtb.Rows[0]["PhoneNumber"]).Trim();
                objResult.EmailAddress = Convert.ToString(dtb.Rows[0]["EmailAddress"]).Trim();
                objResult.GraduationYear = Convert.ToString(dtb.Rows[0]["GraduationYear"]).Trim();
                Boolean.TryParse(dtb.Rows[0]["IsDeleted"].ToString(), out isdeleted);
                objResult.Gender = Convert.ToString(dtb.Rows[0]["Gender"]).Trim();
                objResult.PostalCode = Convert.ToString(dtb.Rows[0]["PostalCode"]).Trim();
                objResult.Grade = Convert.ToString(dtb.Rows[0]["Grade"]).Trim();
                objResult.YearOfAdmission = Convert.ToString(dtb.Rows[0]["YearOfAdmission"]).Trim();
                objResult.IsDeleted = isdeleted;
            }

            return objResult;
        }
        public bool StudentsInsert(StudentsDTO studentDTO, List<FamilyMemberDTO> lstFamilyMember)
        {

            //Tạo list keywords
            object[] objKeyWords = new object[] {
                "@FirstName", studentDTO.FirstName,
                "@LastName", studentDTO.LastName,
                "@Address", studentDTO.Address,
                "@District", studentDTO.District,
                "@CityProvince", studentDTO.CityProvince,
                "@Country", studentDTO.Country,
                "@PostalCode", studentDTO.PostalCode,
                "@EmailAddress", studentDTO.EmailAddress,
                "@PhoneNumber", studentDTO.PhoneNumber,
                "@GraduationYear", studentDTO.GraduationYear,
                "@Birthday", studentDTO.Birthday,
                "@IsDeleted", false,
                "@Gender", studentDTO.Gender,
                "@Grade",studentDTO.Grade,
                "@YearOfAdmission",studentDTO.YearOfAdmission
            };
            // INSERT INTO STUDENTS
            DataTable dtbResult = new DataConnect().callUSP("uspStudents_Insert", objKeyWords);

            if (dtbResult.Rows.Count > 0)
            {
                // INSERT INTO FAMILY INFORMATION

                String studentID = dtbResult.Rows[0]["StudentID"].ToString();
                if (lstFamilyMember != null && lstFamilyMember.Count > 0)
                {
                    for (int i = 0; i < lstFamilyMember.Count; i++)
                    {
                        object[] objKeyWords2 = new object[] {
                        "@Name", lstFamilyMember[i].Name.Trim(),
                        "@PhoneNumber", lstFamilyMember[i].PhoneNumber,
                        "@EmailAddress", lstFamilyMember[i].EmailAddress,
                        "@Relationship", lstFamilyMember[i].Relationship,
                        "@StudentID", studentID
                        };
                        DataTable dtbResult2 = new DataConnect().callUSP("uspFamilyInfomation_Insert", objKeyWords2);
                        if (dtbResult2.Rows.Count <= 0)
                        {
                            return false;
                        }
                    }
                }
             
            }
            return true;
        }

        public List<FamilyMemberDTO> GetFamilyMemberListByID(string strStudentID)
        {
            List<FamilyMemberDTO> lstFamilyMember = new List<FamilyMemberDTO>();

            //Lấy Nhóm người dùng
            object[] objKeyWords = new object[] {
                "@StudentID", strStudentID
            };

            DataTable dtb = new DataConnect().callUSP("uspFamilyInformation_GetMemberByID", objKeyWords);
            if (dtb != null && dtb.Rows.Count > 0)
            {
                for (int i = 0; i < dtb.Rows.Count; i++)
                {
                    FamilyMemberDTO objResult = new FamilyMemberDTO();
                    objResult.MemberID = Convert.ToString(dtb.Rows[i]["MemberID"]).Trim();
                    objResult.Name = Convert.ToString(dtb.Rows[i]["Name"]).Trim();
                    objResult.PhoneNumber = Convert.ToString(dtb.Rows[i]["PhoneNumber"]).Trim();
                    objResult.EmailAddress = Convert.ToString(dtb.Rows[i]["EmailAddress"]).Trim();
                    objResult.Relationship = Convert.ToString(dtb.Rows[i]["Relationship"]).Trim();
                    objResult.StudentID = Convert.ToString(dtb.Rows[i]["StudentID"]).Trim();
                    lstFamilyMember.Add(objResult);
                }

            }
            return lstFamilyMember;
        }

        public bool StudentsUpdate(StudentsDTO studentDTO, List<FamilyMemberDTO> lstFamilyMember)
        {
            //Tạo list keywords
        
            object[] objKeyWords = new object[] {
                "@StudentID",studentDTO.StudentID,
                "@FirstName", studentDTO.FirstName,
                "@LastName", studentDTO.LastName,
                "@Address", studentDTO.Address,
                "@District", studentDTO.District,
                "@CityProvince", studentDTO.CityProvince,
                "@Country", studentDTO.Country,
                "@PostalCode", studentDTO.PostalCode,
                "@EmailAddress", studentDTO.EmailAddress,
                "@PhoneNumber", studentDTO.PhoneNumber,
                "@GraduationYear", studentDTO.GraduationYear,
                "@Birthday", studentDTO.Birthday,
                "@IsDeleted", false,
                "@Gender", studentDTO.Gender,
                "@Grade",studentDTO.Grade
            };
            bool result = new DataConnect().uspExecuteNonQuery("uspStudents_Update", objKeyWords);

            if (result)
            {
                object[] objKeyWords1 = new object[] {
                     "@StudentID",studentDTO.StudentID,
                };
                new DataConnect().callUSP("uspFamilyInfomation_Delete", objKeyWords1);

                if (lstFamilyMember != null && lstFamilyMember.Count > 0)
                {
                    for (int i = 0; i < lstFamilyMember.Count; i++)
                    {
                        object[] objKeyWords2 = new object[] {
                        "@Name", lstFamilyMember[i].Name,
                        "@PhoneNumber", lstFamilyMember[i].PhoneNumber,
                        "@EmailAddress", lstFamilyMember[i].EmailAddress,
                        "@Relationship", lstFamilyMember[i].Relationship,
                        "@StudentID", studentDTO.StudentID
                         };
                         new DataConnect().uspExecuteNonQuery("uspFamilyInfomation_Insert", objKeyWords2);                
                    }
                }
                return true;
            }
            else
            {
                return false;
           }
        }

      
        public bool StudentsDelete(String studentID)
        {
            //Tạo list keywords

            object[] objKeyWords = new object[] {
                "@StudentID",studentID,            
            };
            return new DataConnect().uspExecuteNonQuery("uspStudents_Delete", objKeyWords);          
        }
    }
}
