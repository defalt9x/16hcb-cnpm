﻿///////////////////////////////////////////////////THÊM/////////////////////////////////////////////////
$(document).ready(function () {

    $('#frmClassesInsert').ajaxForm(function (data) {
            ajaxInsertSuccess(data);
        });
 
});




function validationform()
{
    if ($("#tbxClassName").val() == "")
    {
        $.notify("Vui lòng nhập tên lớp", "info");
        $("#tbxClassName").focus();
        return false;
    }

  
    showLoading("btnXacNhan");
    return true;
}



function BackManage() {
    setTimeout(function () { window.location.href = "/quan-ly-lop/" }, 500);
}


//Ham thuc hien sau khi insert
function ajaxInsertSuccess(data) {
    //showLoading("btnInsert");
    hideLoading("btnXacNhan");

    if (data.iserror == false) {
        $.notify("Thêm lớp thành công.", "success");
        setTimeout(function () {
            window.location.href = "/quan-ly-lop";
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}

