﻿var IsLoadServer = false;

$(document).ready(function () {
    InitControl();
    $('#frmSystemSettingsUpdate').ajaxForm(function (data) {
        ajaxUpdateSuccess(data);
    });
});
function UpdateSystemSettings() {
    if (checkValidate() == true) {
        showLoading("btnUpdate");
        $('#frmSystemSettingsUpdate').submit();
    }
    return false;
}
function InitControl() {
    $("#txtMinimumAged").attr("disabled", "disabled");
    $("#txtMaximumAged").attr("disabled", "disabled");
    $("#txtMaximumStudentNumber").attr("disabled", "disabled");
    $("#txtStandardScore").attr("disabled", "disabled");
    $('#btnUpdate').hide();
}
function AllowEdit() {
    $("#txtMinimumAged").removeAttr("disabled");
    $("#txtMaximumAged").removeAttr("disabled");
    $("#txtMaximumStudentNumber").removeAttr("disabled");
    $("#txtStandardScore").removeAttr("disabled");
    $('#btnUpdate').show();
    $('#btnEdit').hide();

}
function RefreshPage() {
    setTimeout(function () {
        setTimeout(function () { window.location.href = "/quan-ly-cau-hinh/" });
    }, 500);
}
function BackManage() {
    setTimeout(function () {
        setTimeout(function () { window.location.href = "/" });
    }, 500);
}
//Ham thuc hien sau khi update
function ajaxUpdateSuccess(data) {
    hideLoading("btnUpdate");

    if (data.iserror == false) {
        $.notify("Cài đặt thành công.", "success");
        setTimeout(function () {
            setTimeout(function () { window.location.href = "/quan-ly-cau-hinh/"});
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}
function checkValidate()
{
    var txtMinimumAged = $("#txtMinimumAged").val().trim();
    var txtMaximumAged = $("#txtMaximumAged").val().trim();
    var txtMaximumStudentNumber = $("#txtMaximumStudentNumber").val().trim();
    var txtStandardScore = $("#txtStandardScore").val().trim();

    if(txtMinimumAged == "")
    {
        $.notify("Vui lòng nhập tuổi tối thiểu", "error");
        $("#txtMinimumAged").focus();
        return false;
    }
    if (isInteger(txtMinimumAged)==false) {
        $.notify("Số tuổi tối thiểu phải là số nguyên", "error");
        $("#txtMinimumAged").focus();
        return false;
    }
    if (parseInt(txtMinimumAged) < 1) {
        $.notify("Số tuổi tối thiểu phải lớn hơn 0", "error");
        $("#txtMinimumAged").focus();
        return false;
    }
    if (txtMaximumAged == "") {
        $.notify("Vui lòng nhập tuổi tối đa", "error");
        $("#txtMaximumAged").focus();
        return false;
    }
    if (isInteger(txtMaximumAged) == false) {
        $.notify("Số tuổi tối đa phải là số nguyên ", "error");
        $("#txtMaximumAged").focus();
        return false;
    }
    if (parseInt(txtMaximumAged) < 1 || parseInt(txtMaximumAged) > 99) {
        $.notify("Số tuổi tối đa phải lớn hơn 0 và nhỏ hơn 100", "error");
        $("#txtMaximumAged").focus();
        return false;
    }
    if (parseInt(txtMaximumAged) < parseInt(txtMinimumAged)) {
        $.notify("Số tuổi tối thiểu phải nhỏ hơn số tuổi tối đa", "error");
        $("#txtMaximumAged").focus();
        return false;
    }

    if (txtMaximumStudentNumber == "") {
        $.notify("Vui lòng nhập sĩ số tối đa", "error");
        $("#txtMaximumStudentNumber").focus();
        return false;
    }
    if (isInteger(txtMaximumStudentNumber) == false) {
        $.notify("Sĩ số tối đa phải là số nguyên", "error");
        $("#txtMaximumStudentNumber").focus();
        return false;
    }
    if (parseInt(txtMaximumStudentNumber) < 0 || parseInt(txtMaximumStudentNumber) > 500) {
        $.notify("Sĩ số tối đa phải là số nguyên từ 1 - 500", "error");
        $("#txtMaximumStudentNumber").focus();
        return false;
    }
    if (txtStandardScore == "") {
        $.notify("Vui lòng nhập điểm chuẩn", "error");
        $("#txtStandardScore").focus();
        return false;
    } 
    if (isNaN(txtStandardScore)) {
        $.notify("Điểm chuẩn phải là số thực", "error");
        $("#txtStandardScore").focus();
        return false;
    }
    if (parseFloat(txtStandardScore) <= 0 || parseFloat(txtStandardScore) > 10) {
        $.notify("Điểm chuẩn phải nằm trong khoảng từ 1 - 10", "error");
        $("#txtStandardScore").focus();
        return false;
    }
    return true;
}
function isInteger(x){
    if(x%1==0){
        return true;
    }
    return false;
}