﻿using DAO;
using DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectStudentManagement.Controllers
{
    public class ScoreCorrectionController : Controller
    {
        //
        // GET: /ScoreCorrection/
        public ActionResult Index()
        {
            //Kiểm tra xem người dùng có được xem(truy cập vào chức năng)
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLPD_V") == false)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra xem người dùng có được phép lập phiếu
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLPD_I") == false)
            {
                ViewBag.InsertPermission = false;
            }

            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
            return View();
        }

        [HttpPost]
        public ActionResult SearchScoreCorrection(string strKeyWord, string strKeyWordStudentID, string strKeyWordYear,
            string strKeyWordSemeter, string strKeyWordSubject, int intPageIndex = 0)
        {
            try
            {
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;
                string strUserCreateID = null;
                // nếu không có quyền chỉnh sửa thì chỉ lấy những phiếu do user hiện tại lập
                if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLPD_U") == false)
                {
                    strUserCreateID = (string)Session["Username"];
                }
                //Gen HTML
                StringBuilder strHTMLScoreCorrection = new StringBuilder();
                DataTable dtbScoreCorrection = new ScoreCorrectionDAO().SearchScoreCorrections(
                    strKeyWord, strKeyWordStudentID, strKeyWordYear, strKeyWordSemeter,
                    strKeyWordSubject, strUserCreateID, intPageIndex, intVisiblePages);

                if (dtbScoreCorrection != null && dtbScoreCorrection.Rows.Count > 0)
                {
                    strHTMLScoreCorrection = GenHTMLScoreCorrection(dtbScoreCorrection);
                    intTotalRows = Convert.ToInt32(dtbScoreCorrection.Rows[0]["TotalRows"]);
                    intTotalPages = intTotalRows / intVisiblePages;
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTMLScoreCorrection.Append("<tr><td colspan='100%' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTMLScoreCorrection),
                    totalpages = intTotalPages,
                    totalrows = intTotalRows
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        private StringBuilder GenHTMLScoreCorrection(DataTable dtb)
        {
            bool bPerEdit = true;
            bool bPerDelete = false;
            //Kiểm tra người dùng được xóa
            //if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK001D") == false)
            //{
            //    bPerDelete = false;
            //}

            int intCountRow = dtb.Rows.Count;
            StringBuilder strHTMLScoreCorrection = new StringBuilder();

            for (int i = 0; i < intCountRow; i++)
            {
                string strScoreCorrectionID = Convert.ToString(dtb.Rows[i]["ScoreCorrectionID"]).Trim();

                strHTMLScoreCorrection.Append("<tr>");
                strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", dtb.Rows[i]["STT"]);
                strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", dtb.Rows[i]["Year"]);
                strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", dtb.Rows[i]["Semester"]);
                strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", dtb.Rows[i]["SubjectID"]);
                strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", dtb.Rows[i]["UserCreateID"]);
                strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", dtb.Rows[i]["StudentID"]);

                List<ScoreCorrectionDetailDTO> lst = new ScoreCorrectionDAO().GetDetail_ScoreCorrectionByID(strScoreCorrectionID);
                for (int j = 0, k = 0; j < 3; j++)
                {
                    if (k < lst.Count && j == Convert.ToInt32(lst[k].ScoreType))
                    {
                        strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", lst[k].ScoreOld);
                        k++;
                    }
                    else
                    {
                        strHTMLScoreCorrection.AppendFormat("<td></td>");
                    }
                }
                for (int j = 0, k = 0; j < 3; j++)
                {
                    if (k < lst.Count && j == Convert.ToInt32(lst[k].ScoreType))
                    {
                        strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", lst[k].ScoreNew);
                        k++;
                    }
                    else
                    {
                        strHTMLScoreCorrection.AppendFormat("<td></td>");
                    }
                }
                if (dtb.Rows[i]["IsAccepted"].Equals(true))
                    strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", "Y");
                else if (dtb.Rows[i]["IsAccepted"].Equals(false))
                    strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", "N");
                else
                    strHTMLScoreCorrection.AppendFormat("<td>{0}</td>", "");
                string sEdit = "";
                string sDelete = "";
                if (bPerEdit != false)
                {
                    sEdit += "<span class='glyphicon glyphicon-pencil table-icon' data-name='Detail'></span>";
                }

                //if (bPerDelete != false)
                //{
                //    sDelete += "<span class='glyphicon glyphicon-trash table-icon' data-name='Delete'></span>";
                //}
                strHTMLScoreCorrection.AppendFormat("<td data-scorecorrectionid='{0}'>" + sEdit + sDelete + "</td>", strScoreCorrectionID);

                strHTMLScoreCorrection.Append("</tr>");
            }

            return strHTMLScoreCorrection;
        }


        #region detail
        //
        // GET: /ScoreCorrection/Details/5
        public ActionResult Details(string id)
        {
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLPD_V") == false)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra người dùng được xử lý phiếu
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLPD_U") == false)
            {
                ViewBag.UpdatePermission = false;
            }

            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            ScoreCorrectionDTO tmp = new ScoreCorrectionDAO().GetScoreCorrectionByID(id);
            return View(tmp);
        }

        [HttpPost]
        public JsonResult SetAcceptScoreCorrection(string strScoreCorrectionID, string isAccepted)
        {
            try
            {
                bool bolSuccess = new ScoreCorrectionDAO().SetAcceptScoreCorrection(
                    strScoreCorrectionID, isAccepted, (string)Session["Username"]);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region create
        //
        // GET: /ScoreCorrection/Create
        public ActionResult Create()
        {
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLPD_I") == false)
            {
                return RedirectToAction("Index", "ScoreCorrection");
            }

            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
            return View();
        }

        [HttpPost]
        public JsonResult InsertScoreCorrection(FormCollection frm)
        {
            try
            {
                List<ScoreCorrectionDetailDTO> lstSubjectExemptRequest =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<List<ScoreCorrectionDetailDTO>>(frm["hdListScore"]);
                string ScoreID = Convert.ToString(frm["hdScoreID"]);
                string UserCreateID = (string)Session["Username"];
                string IsAccepted = null;
                string UserDecideID = null;
                DateTime? InsertedDatetime = null;
                DateTime? LastModified = null;

                bool bolSuccess = new ScoreCorrectionDAO().InsertScoreCorrection(ScoreID, UserCreateID, IsAccepted, UserDecideID,
             InsertedDatetime, LastModified, lstSubjectExemptRequest);

                if (bolSuccess == false)
                    return Json(new { iserror = true });

                return Json(new { iserror = false });
            }
            catch (Exception)
            {
                return Json(new { iserror = true });
            }
        }
        #endregion

        #region score for modal in create
        [HttpPost]
        public ActionResult SearchScore(string strKeyWord, string strKeyWordYear,
            string strKeyWordSemeter, string strKeyWordSubject, string strKeyWordStudentID, int intPageIndex = 0)
        {
            try
            {
                //Lấy cấu hình số dòng trên trang
                //int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTML = new StringBuilder();
                DataTable dtb = new ScoreCorrectionDAO().SearchScores(strKeyWord, strKeyWordYear,
             strKeyWordSemeter, strKeyWordSubject, strKeyWordStudentID, intPageIndex, intVisiblePages);

                if (dtb != null && dtb.Rows.Count > 0)
                {
                    strHTML = GenHTMLScore(dtb);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtb.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTML.Append("<tr><td colspan='100%' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new
                {
                    iserror = false,
                    content = Convert.ToString(strHTML),
                    totalpages = intTotalPages,
                    totalrows = intTotalRows
                });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        private StringBuilder GenHTMLScore(DataTable dtb)
        {
            int intCountRow = dtb.Rows.Count;
            StringBuilder strHTML = new StringBuilder();

            for (int i = 0; i < intCountRow; i++)
            {
                string strScoreID = Convert.ToString(dtb.Rows[i]["ScoreID"]).Trim();

                strHTML.AppendFormat("<tr id='tr_{0}'>", strScoreID);
                strHTML.AppendFormat("<td data-name='STT'>{0}</td>", dtb.Rows[i]["STT"]);
                strHTML.AppendFormat("<td data-name='Year'>{0}</td>", dtb.Rows[i]["Year"]);
                strHTML.AppendFormat("<td data-name='Semester'>{0}</td>", dtb.Rows[i]["Semester"]);
                strHTML.AppendFormat("<td data-name='SubjectID'>{0}</td>", dtb.Rows[i]["SubjectID"]);
                strHTML.AppendFormat("<td data-name='StudentID'>{0}</td>", dtb.Rows[i]["StudentID"]);
                strHTML.AppendFormat("<td data-name='oral_old'>{0}</td>", dtb.Rows[i]["Score1"]);
                strHTML.AppendFormat("<td data-name='fifteenminutes_old'>{0}</td>", dtb.Rows[i]["Score2"]);
                strHTML.AppendFormat("<td data-name='fortyfiveminutes_old'>{0}</td>", dtb.Rows[i]["Score3"]);
                strHTML.AppendFormat("<td><span class='glyphicon glyphicon-plus table-icon' data-name='btnAddScore' data-idscore='{0}'></span></td>", strScoreID);

                strHTML.Append("</tr>");
            }

            return strHTML;
        }
        #endregion

        #region helper
        [HttpPost]
        public ActionResult GetSubjectList()
        {
            try
            {
                DataTable dtbQuery = new SubjectDAO().GetSubjectList();
                string sSubjectList = JsonConvert.SerializeObject(dtbQuery);
                return Json(new { iserror = false, subjectlistcontent = sSubjectList });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }
        #endregion
    }
}