﻿
$(document).ready(function () {
    //$('#frmSetAcceptScoreCorrection').ajaxForm(function (data) {
    //    ajaxUpdateSuccess(data);
    //});
});
///////////////////////////////////////////////////xử lý phiếu/////////////////////////////////////////////////
function SetAcceptScoreCorrection(strScoreCorrectionID, isAccepted) {
    param = {};
    param.strScoreCorrectionID = strScoreCorrectionID;
    param.isAccepted = isAccepted;
    $.ajax({
        url: "/ScoreCorrection/SetAcceptScoreCorrection",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(param),
        dataType: 'json',
        beforeSend: function () {
            if (isAccepted)
                showLoading("btnAccpet");
            else
                showLoading("btnNotAccpet");
        },
        success: function (data) {
            if (data.iserror == false) {
                $.notify("Xủ lý phiếu thành công.", "success");
                setTimeout(function () {
                    var id = $('#hdScoreCorrectionID').val();
                    setTimeout(function () { window.location.href = "/quan-ly-phieu-hieu-chinh-diem/chi-tiet/" + id }, 500);
                }, 500);
            }
            else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });

    if (isAccepted)
        hideLoading("btnAccpet");
    else
        hideLoading("btnNotAccpet");

}
