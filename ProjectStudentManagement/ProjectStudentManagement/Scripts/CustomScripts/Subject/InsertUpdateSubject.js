﻿///////////////////////////////////////////////////THÊM/////////////////////////////////////////////////
$(document).ready(function () {
    if ($('#hdFormType').val() == "Insert") {
        //Khoi tao su kien sau khi insert
        $('#frmSubjectInsert').ajaxForm(function (data) {
            ajaxInsertSuccess(data);
        });
    } else {
        //Khoi tao su kien sau khi update
        $('#frmSubjectUpdate').ajaxForm(function (data) {
            ajaxUpdateSuccess(data);
        });
    }

});

function BackManage() {
    setTimeout(function () { window.location.href = "/quan-ly-mon-hoc/" }, 500);
}

function InsertSubject() {
    if (checkValidate()) {

        showLoading("btnInsert");
        $('#frmSubjectInsert').submit();
    }
    return false;
}
function checkValidate() {
    var subjectname = $('#txtSubjectName').val().trim();
    if (subjectname == "") {
        $('#txtKeyWord').focus();
        $.notify("Vui lòng nhập tên môn học", "error");
        return false;
    }
    return true;
}
//Ham thuc hien sau khi insert
function ajaxInsertSuccess(data) {
    hideLoading("btnInsert");

    if (data.iserror == false) {
        $.notify("Thêm môn học thành công.", "success");
        setTimeout(function () {
            window.location.href = "/quan-ly-mon-hoc";
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}

///////////////////////////////////////////////////CẬP NHẬT/////////////////////////////////////////////////
function UpdateSubject() {
    showLoading("btnUpdate");
    $('#frmSubjectUpdate').submit();
    return false;
}

//Ham thuc hien sau khi update
function ajaxUpdateSuccess(data) {
    hideLoading("btnUpdate");

    if (data.iserror == false) {
        $.notify("Cập nhật môn học thành công.", "success");
        setTimeout(function () {
            var strSubjectID = $('#hdSubjectID').val();
            setTimeout(function () { window.location.href = "/quan-ly-mon-hoc/chi-tiet/" + strSubjectID }, 500);
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}