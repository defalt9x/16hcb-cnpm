﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PermissionDTO
    {
        public string IDKey { get; set; }
        public string KeyDescription { get; set; }
    }
}
