﻿///////////////////////////////////////////////////THÊM/////////////////////////////////////////////////
$(document).ready(function () {
    if ($('#hdFormType').val() == "Insert") {
        //Khoi tao su kien sau khi insert
        $('#frmSystemSettingsInsert').ajaxForm(function (data) {
            ajaxInsertSuccess(data);
        });
    } else {
        //Khoi tao su kien sau khi update
        $('#frmSystemSettingsUpdate').ajaxForm(function (data) {
            ajaxUpdateSuccess(data);
        });
    }

});

function BackManage() {
    setTimeout(function () { window.location.href = "/quan-ly-cau-hinh/" }, 500);
}

function InsertSystemSettings() {
    showLoading("btnInsert");
    $('#frmSystemSettingsInsert').submit();
    return false;
}

//Ham thuc hien sau khi insert
function ajaxInsertSuccess(data) {
    hideLoading("btnInsert");

    if (data.iserror == false) {
        $.notify("Thêm khai báo cấu hình thành công.", "success");
        setTimeout(function () {
            window.location.href = "/quan-ly-cau-hinh";
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}

///////////////////////////////////////////////////CẬP NHẬT/////////////////////////////////////////////////
function UpdateSystemSettings() {
    var arrPermission = [];
    showLoading("btnUpdate");
    $('#frmSystemSettingsUpdate').submit();
    return false;
}

//Ham thuc hien sau khi update
function ajaxUpdateSuccess(data) {
    hideLoading("btnUpdate");

    if (data.iserror == false) {
        $.notify("Cập nhật khai báo cấu hình thành công.", "success");
        setTimeout(function () {
            window.location.href = "/quan-ly-cau-hinh";
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}