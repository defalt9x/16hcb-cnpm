﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAO;
using System.Data;
using Newtonsoft.Json;
using System.Text;
using System.Configuration;

namespace ProjectStudentManagement.Controllers
{
    public class ScoreController : Controller
    {
        //
        // GET: /Score/
        public ActionResult Index()
        {
            if (Session["Username"] == null)
            {
                return RedirectToAction("Login", "Home");
            }

            //Kiểm tra người dùng được sửa
            ViewBag.EditPermission = true;
            if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLD_U") == false)
            {
                ViewBag.EditPermission = false;
            }

            //Lấy config số trang hiển thị trên thanh phân trang
            ViewBag.intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);

            return View();
        }


        [HttpPost]
        public ActionResult ScoreEdit(string strKeyWordStudent, string strKeyWordYear, string strKeyWordSemeter, string strKeyWordSubject, string strKeyWordScore1, string strKeyWordScore2, string strKeyWordScore3)
        {

            try
            {
                ScoresDAO SDA = new ScoresDAO();
                SDA.ScoreEdit(strKeyWordStudent, strKeyWordYear, strKeyWordSemeter, strKeyWordSubject, strKeyWordScore1, strKeyWordScore2, strKeyWordScore3);
                return Json(new { iserror = false });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }


        [HttpPost]
        public ActionResult GetSubjectList()
        {

            try
            {
                DataTable dtbQuery = new ScoresDAO().GetSubjectList();
                string sSubjectList = JsonConvert.SerializeObject(dtbQuery);
                return Json(new { iserror = false, subjectlistcontent = sSubjectList });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        [HttpPost]
        public ActionResult GetClassList(string strKeyWord)
        {

            try
            {
                DataTable dtbQuery = new ScoresDAO().GetClassList(strKeyWord);
                string sClassList = JsonConvert.SerializeObject(dtbQuery);
                return Json(new { iserror = false, Classlistcontent = sClassList });
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }


        [HttpPost]
        public ActionResult GetScore(string strKeyWordYear, string strKeyWordSemeter, string strKeyWordStudent, string strKeyWordClass, string strKeyWordSubject, int intPageIndex = 0)
        {

            try
            {

                bool bPerEdit = true;
                //Kiểm tra người dùng được sửa
                if (LoginDAO.CheckKeyPermission((string)Session["Username"], "PK_QLD_U") == false)
                {
                    bPerEdit = false;
                }
                if ((int.Parse(strKeyWordSemeter) == 1) & (DateTime.Now.Year > int.Parse(strKeyWordYear)))
                {
                    bPerEdit = false;
                }

                if ((int.Parse(strKeyWordSemeter) == 2) & ((DateTime.Now.Year != (int.Parse(strKeyWordYear) + 1)) | (DateTime.Now.Month > 6)))
                {
                    bPerEdit = false;
                }


                //Lấy cấu hình số dòng trên trang
                //int intPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["RowPerPage"]);
                //Lấy config số trang hiển thị trên thanh phân trang
                int intVisiblePages = Convert.ToInt32(ConfigurationManager.AppSettings["visiblePages"]);
                int intTotalPages = 0;
                int intTotalRows = 0;

                //Gen HTML
                StringBuilder strHTMLEmloyeeRoles = new StringBuilder();

                DataTable dtbQuery = new ScoresDAO().GetScore(strKeyWordYear, strKeyWordSemeter, strKeyWordStudent, strKeyWordClass, strKeyWordSubject, intPageIndex, intVisiblePages);

                if (dtbQuery != null && dtbQuery.Rows.Count > 0)
                {
                    strHTMLEmloyeeRoles = GenHTMLScore(dtbQuery, bPerEdit);
                    //Lấy tổng số lưởng dòng
                    intTotalRows = Convert.ToInt32(dtbQuery.Rows[0]["TotalRows"]);
                    //Lấy tổng số lượng trang
                    intTotalPages = intTotalRows / intVisiblePages;
                    //Trường hợp số phần tử không chia hết cho intVisiblePages
                    if (intTotalRows % intVisiblePages != 0)
                        intTotalPages = intTotalPages + 1;
                }
                else
                {
                    strHTMLEmloyeeRoles.Append("<tr><td colspan='12' class='table-content-null'>Hiện không có nội dung</td></tr>");
                }

                return Json(new { iserror = false, 
                                content = Convert.ToString(strHTMLEmloyeeRoles), 
                                totalpages = intTotalPages, 
                                totalrows = intTotalRows,
                                editpcolumn = bPerEdit});
            }
            catch
            {
                return Json(new { iserror = true });
            }
        }

        private StringBuilder GenHTMLScore(DataTable dtbPermission, bool bPerEdit)
        {

            


            int intCountRow = dtbPermission.Rows.Count;
            StringBuilder strHTMLScore = new StringBuilder();

            for (int i = 0; i < intCountRow; i++)
            {
                strHTMLScore.Append("<tr>");
                strHTMLScore.AppendFormat("<td class='StudentIDCell'>{0}</td>", dtbPermission.Rows[i]["StudentID"]);
                strHTMLScore.AppendFormat("<td class='FullNameCell'>{0}</td>", dtbPermission.Rows[i]["FullName"]);
                strHTMLScore.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i]["Year"]);
                strHTMLScore.AppendFormat("<td>{0}</td>", dtbPermission.Rows[i]["Semester"]);
                strHTMLScore.AppendFormat("<td class='Score1Cell'>{0}</td>", dtbPermission.Rows[i]["Score1"]);
                strHTMLScore.AppendFormat("<td class='Score2Cell'>{0}</td>", dtbPermission.Rows[i]["Score2"]);
                strHTMLScore.AppendFormat("<td class='Score3Cell'>{0}</td>", dtbPermission.Rows[i]["Score3"]);
                if (bPerEdit != false)
                {

                    string sEdit = "";
                    if (bPerEdit != false)
                    {
                        sEdit += "<span class='glyphicon glyphicon-pencil table-icon' data-name='Detail'></span>";
                    }

                    strHTMLScore.AppendFormat("<td class='btnTableEdit' data-studentid='{0}'>" + sEdit + "</td>", dtbPermission.Rows[i]["StudentID"]);
                }

                strHTMLScore.Append("</tr>");
            }

            return strHTMLScore;
        }
	}
}