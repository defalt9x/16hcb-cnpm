﻿///////////////////////////////////////////////////THÊM/////////////////////////////////////////////////
$(document).ready(function () {
    GetRole();
    if ($('#hdFormType').val() == "Insert") {
        //Khoi tao su kien sau khi insert
        $('#frmEmployeesInsert').ajaxForm(function (data) {
            ajaxInsertSuccess(data);
        });
    } else {
        //Khoi tao su kien sau khi update
        $('#frmEmployeesUpdate').ajaxForm(function (data) {
            ajaxUpdateSuccess(data);
        });
    }
    
});

function GetRole() {

    $.ajax({
        url: "/Employees/GetRole",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data) {
            if (data.iserror == false) {
                var obj = JSON.parse(data.Rolelistcontent);
                var objsize = Object.keys(obj).length;
                for (var i = 0; i < objsize; i++) {
                    $("#sltKeyWordRole").append("<option value='" + obj[i].RoleID + "'>" + obj[i].RoleName + " </option>");
                }
            } else {
                $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
            }
        },
        error: function (xhr, status, error) {
            $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
        }
    });
}


function validationform()
{
    if ($("#tbxHo").val() == "")
    {
        $.notify("Vui lòng nhập họ", "info");
        $("#tbxHo").focus();
        return false;
    }

    if ($("#tbxTen").val() == "") {
        $.notify("Vui lòng nhập tên", "info");
        $("#tbxTen").focus();
        return false;
    }

    if ($("#tbxSDT").val() == "") {
        $.notify("Vui lòng nhập số điện thoại", "info");
        $("#tbxSDT").focus();
        return false;
    }

    if ($("#tbxSDT").val().length != 11 & $("#tbxSDT").val().length != 7) {
        $.notify("số điện thoại cần có 7 số hay 11 số", "info");
        $("#tbxSDT").focus();
        return false;
    }

    if ($("#tbxEmail").val() == "") {
        $.notify("Vui lòng nhập email", "info");
        $("#tbxEmail").focus();
        return false;
    }

    if (!isEmail($("#tbxEmail").val())) {
        $.notify("Email không đúng định dạng", "info");
        $("#tbxEmail").focus();
        return false;
    }

    if ($("#sltKeyWordRole").val() == null) {
        $.notify("Vui lòng chọn quyền", "info");
        $("#sltKeyWordRole").focus();
        return false;
    }

    if ($("#tbxPass").val() == "") {
        $.notify("Vui lòng nhập mật khẩu", "info");
        $("#tbxPass").focus();
        return false;
    }

    if ($("#tbxPass").val().length < 5) {
        $.notify("Mật khẩu cần có hơn 4 ký tự", "info");
        $("#tbxPass").focus();
        return false;
    }
    showLoading("btnXacNhan");
    return true;
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


function BackManage() {
    setTimeout(function () { window.location.href = "/quan-ly-giao-vien/" }, 500);
}

function BackManageUpdate() {
    setTimeout(function () { window.location.href = "/quan-ly-giao-vien/chi-tiet/" + $("#hdEmployeeID").val() }, 500);
}

//Ham thuc hien sau khi insert
function ajaxInsertSuccess(data) {
    //showLoading("btnInsert");
    hideLoading("btnXacNhan");

    if (data.iserror == false) {
        $.notify("Thêm giáo viên thành công.", "success");
        setTimeout(function () {
            window.location.href = "/quan-ly-giao-vien";
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}

///////////////////////////////////////////////////CẬP NHẬT/////////////////////////////////////////////////


//Ham thuc hien sau khi update
function ajaxUpdateSuccess(data) {
    hideLoading("btnXacNhan");

    if (data.iserror == false) {
        $.notify("Cập nhật giáo viên thành công.", "success");
        setTimeout(function () {
            var strEmployeeID = $('#hdEmployeeID').val();
            setTimeout(function () { window.location.href = "/quan-ly-giao-vien/chi-tiet/" + strEmployeeID }, 500);
        }, 500);
    }
    else {
        $.notify("Đã có lỗi xảy ra! Vui lòng nhấn F5 để thử lại.", "error");
    }
}