﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ScoreCorrectionDetailDTO
    {
        public string ScoreCorrectionDetailID { get; set; }
        public string ScoreCorrectionID { get; set; }
        public string ScoreType { get; set; }
        public string ScoreOld { get; set; }
        public string ScoreNew { get; set; }

    }
}
