﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class SubjectDAO
    {
        public DataTable GetSubjectList()
        {
            DataTable dtb = new DataConnect().callUSP("uspGetSubjectList", null);
            if (dtb != null && dtb.Rows.Count != 0)
            {
                return dtb;
            }
            return null;
        }
        public DataTable SearchSubjects(string strKeyWord, int intPageIndex, int intPageSize)
        {
            object[] objKeyWords = new object[] {
                "@PageSize", intPageSize,
                "@PageIndex", intPageIndex,
                "@KeyWord", strKeyWord
            };

            DataTable dtbSubjects = new DataConnect().callUSP("uspGetSubjects_SRH", objKeyWords);
            if (dtbSubjects != null && dtbSubjects.Rows.Count != 0)
            {
                return dtbSubjects;
            }

            return null;
        }
        public bool InsertSubject(string strSubjectName)
        {
            object[] objKeyWords = new object[] {
                "@SubjectName", strSubjectName
            };

            return new DataConnect().uspExecuteNonQuery("uspSubjects_ADD", objKeyWords);
        }
        public SubjectDTO GetSubjectByID(string strSubjectID)
        {
            SubjectDTO objResult = new SubjectDTO();

            object[] objKeyWords = new object[] {
                "@SubjectID", strSubjectID
            };

            DataTable dtbSubject = new DataConnect().callUSP("uspSubjects_SEL", objKeyWords);

            if (dtbSubject != null && dtbSubject.Rows.Count > 0)
            {
                objResult.SubjectID = Convert.ToString(dtbSubject.Rows[0]["SubjectID"]).Trim();
                objResult.SubjectName = Convert.ToString(dtbSubject.Rows[0]["SubjectName"]).Trim();
            }

            return objResult;
        }
        public bool UpdateSubject(string strSubjectID, string strSubjectName)
        {
            object[] objKeyWords = new object[] {
                "@SubjectID", strSubjectID,
                "@SubjectName", strSubjectName
            };

            return new DataConnect().uspExecuteNonQuery("uspSubjects_UPD", objKeyWords);
        }
        public bool DeleteSubject(string strSubjectID, ref string strMessage)
        {
            //Kiểm tra nhóm người dùng có đang tồn tại người dùng nào không
            DataTable dtbScoreBySubject = new ScoresDAO().GetScoreBySubject(strSubjectID);
            if (dtbScoreBySubject != null && dtbScoreBySubject.Rows.Count > 0)
            {
                strMessage = "Tồn tại điểm thuộc môn học!";
            }
            else
            {
                object[] objKeyWords = new object[] {
                "@SubjectID", strSubjectID
                 };

                return new DataConnect().uspExecuteNonQuery("uspSubjects_DEL", objKeyWords);
            }

            return false;
        }
    }
}
