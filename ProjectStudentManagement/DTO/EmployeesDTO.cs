﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class EmployeesDTO
    {
        public string EmployeesID { get; set; }
        public string strLastName {get; set;}
        public string strFirstName { get; set; }
        public string strAddress { get; set; }
        public string strDistrict { get; set; }
        public string strCity { get; set; }
        public string strSDT { get; set; }
        public string strEmail { get; set; }
        public string strRole { get; set; }
        public string strRoleID { get; set; }
    }
}
