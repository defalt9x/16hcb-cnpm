﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ClassesStudentsDTO
    {
        public string ClassID { get; set; }
        public string ClassName { get; set; }
        public string StudentID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string CityProvince { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string GraduationYear { get; set; }
        public bool IsDeleted { get; set; }
        public string Gender { get; set; }
        public string PostalCode { get; set; }
        public string Grade { get; set; }
        public int NumberofStudents { get; set; }
        public string Year { get; set; }
        public List<FamilyMemberDTO> lstFamilyMember { get; set; }

    }
}
